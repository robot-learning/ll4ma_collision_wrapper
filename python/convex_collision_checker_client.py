# This is a wrapper over convex_collision_checker cpp library
import rospy
import numpy as np
from ll4ma_collision_wrapper.srv import *
from geometry_msgs.msg import Pose

class convex_collision_checker_client:
    def __init__(self,srv_name='collision_checker'):
        # Wait for service
        rospy.wait_for_service(srv_name)
        print "Found collision server"
        self.collision_checker=rospy.ServiceProxy(srv_name,collision_checker_msg,persistent=True)
        
        
    def get_signed_distance(self,r_pose):

        collision_inp=[]

        for i in range(len(r_pose)):
            l_pose=Pose()
            l_pose.position.x=r_pose[i][0]
            l_pose.position.y=r_pose[i][1]
            l_pose.position.z=r_pose[i][2]
            l_pose.orientation.x=r_pose[i][3]
            l_pose.orientation.y=r_pose[i][4]
            l_pose.orientation.z=r_pose[i][5]
            l_pose.orientation.w=r_pose[i][6]
            collision_inp.append(l_pose)
            #print collision_inp
        try:
            # Call collision checker:
            resp=self.collision_checker(collision_inp)
            #print resp
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e
            #exit()

        robot_data=[]
        for i in range(len(resp.link_data)):
            link_data=[]            
            for j in range(len(resp.link_data[i].c_data)):
                col_data=[resp.link_data[i].c_data[j].collision,np.array([float(resp.link_data[i].c_data[j].point[0]),float(resp.link_data[i].c_data[j].point[1]),float(resp.link_data[i].c_data[j].point[2])]),
                          np.array(resp.link_data[i].c_data[j].dir),resp.link_data[i].c_data[j].distance]
                link_data.append(col_data)
            robot_data.append(link_data)
        
        return robot_data

'''
def test_client():
    c_checker=convex_collision_checker()
    r_poses=[np.zeros(7) for i in range(7)]
    r_data=c_checker.get_signed_distance(r_poses)
    print r_data

#test_client()
'''
