#include "convex_collision_checker.h"
#include <ros/ros.h>
#include <ros/package.h>
#include <ctime>



int main(int argc, char** argv)
{
  
  // Initialize collision server with environment and robot:
  
  int DOF=8;// here its the number of links= n_joints+1
  string mesh_location=ros::package::getPath("urlg_robots_description");
  mesh_location.append("/meshes/lbr4/convex_hull/vertices/");
  collisionChecker c_check;
  // robot mesh file names:
  vector<string> r_files={"link_0.ply", "link_1.ply", "link_2.ply", "link_3.ply", "link_4.ply", "link_5.ply", "link_6.ply","link_7.ply"};

  for(int i=0;i<DOF;++i)
    {
      r_files[i].insert(0,mesh_location);
    }

  // This should from your data directory:
  string pcd_file="/home/bala/pcd_data/sim_table_tf.pcd";
  // Publish cloud
  c_check.initialize_robot_env_state(DOF,r_files,pcd_file);

  ros::init(argc,argv,"collision_server");
  ros::NodeHandle n;

  // Creating ros service:
  ros::ServiceServer service=n.advertiseService("collision_checker",&collisionChecker::check_collisions,&c_check);

  ROS_INFO("Running collision server");
  ros::spin();
  return 0;
}
