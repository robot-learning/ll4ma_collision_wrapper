# This is a wrapper over convex_collision_checker cpp library
import sys
from threading import Thread
import subprocess
from subprocess import PIPE
import numpy as np
from Queue import Queue, Empty
import time

ON_POSIX = 'posix' in sys.builtin_module_names




class convex_collision_checker:
    def __init__(self):
        # Initialize subprocess:
        cmd=['/bin/bash','rosrun','ll4ma_collision_wrapper','python_convex_collision_wrapper']
        self.collision_checker=subprocess.Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        self.q = Queue()
        self.q_data=Queue()
        t = Thread(target=self.enqueue_output, args=(self.collision_checker.stderr, self.q))
        t.daemon = True # thread dies with the program
        t.start()
        t_data=Thread(target=self.enqueue_output, args=(self.collision_checker.stdout, self.q_data))
        t_data.daemon= True
        t_data.start()
        # wait for intialization:
        self.check_process("**** Phew! Successfully initialized collision checker****")
        print "**** Phew! Successfully initialized collision checker****"

    def enqueue_output(self,out, queue):
        for line in iter(out.readline, b''):
            queue.put(line)
        out.close()

    def check_process(self,text,data_return=False):
        # This function reads the popen stream until the "text" is printed.
        datastream=""
        process_reached=False
        while(process_reached==False):
            try:  line = self.q.get_nowait() # or q.get(timeout=.1)
            except Empty:
                c=1 # since it cannot be empty
                #print('no output yet')
            else: # got line
                #print line
                datastream+=line
                if(text in line):
                    #print "Process computation succeeded"
                    process_reached=True
        #print datastream
        if(data_return):
            return datastream
        #return True

    def fetch_data(self,text):
        datastream=""
        process_reached=False
        while(process_reached==False):
            try:  line = self.q_data.get_nowait() # or q.get(timeout=.1)
            except Empty:
                c=1 # since it cannot be empty
                #print('no output yet')
            else: # got line
                #print line
                datastream+=line
                if(text in line):
                    #print "Process computation succeeded"
                    process_reached=True
        return datastream


        
    def get_signed_distance(self,r_pose):
        inp_data=''
        new_line='\n'
        for i in range(len(r_pose)):
            for j in range(7):
                inp_data+=str(r_pose[i][j])+new_line
                
        self.collision_checker.stdin.write(inp_data)
        self.check_process("signed_distance_done")
        data=self.fetch_data("signed_distance_done")

        return self.convert_datastream(data)
        

    def convert_datastream(self,data_stream):
        link_data=[]
        n_data=data_stream.split()
        robot_data=[]
        # Run a loop along all lines:
        i=2
        collision_data_length=8

        while i<(len(n_data)):
            if(n_data[i]=="robot_link:"):
         
                robot_data.append(link_data)
                link_data=[]
                i+=2
         
            elif(i<len(n_data)-collision_data_length):
                
                # collision data: 1 line
                collision=bool(n_data[i])

                # point data: 3 lines
                nearest_point=np.zeros(3)
                nearest_point[0]=float(n_data[i+1])
                nearest_point[1]=float(n_data[i+2])
                nearest_point[2]=float(n_data[i+3])

                # direction data: 3 lines
                sep_vec_dir=np.zeros(3)
                sep_vec_dir[0]=float(n_data[i+4])
                sep_vec_dir[1]=float(n_data[i+5])
                sep_vec_dir[2]=float(n_data[i+6])

                # Penetration depth: 1 line
                pen_depth=float(n_data[i+7])
                col_data=[collision,nearest_point,sep_vec_dir,pen_depth]
                link_data.append(col_data)
                #print col_data
                i+=collision_data_length
            else:
                robot_data.append(link_data)
                i+=1
        #print robot_data
        return robot_data
        
    def kill_process(self):
        self.collision_checker.kill()

def test_collision_checker():
    c_checker=convex_collision_checker()
    # Create robot link poses:
    r_poses=[np.zeros(7) for i in range(8)]

    sta_t = time.time()
    
    for i in range(2):
        print i
        r_poses[0][0]+=0.1
        r_data=c_checker.get_signed_distance(r_poses)
    elap=time.time()-sta_t
    print "Time to compute(seconds): "+str(elap)

    c_checker.kill_process()
    

#test_collision_checker()
