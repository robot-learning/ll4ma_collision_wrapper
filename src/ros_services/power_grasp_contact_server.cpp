#include "convex_collision_checker.h"
#include <ros/ros.h>
#include <ros/package.h>
#include <ctime>

// ros tf:
//#include <tf/transform_listener.h>



int main(int argc, char** argv)
{
  
  // Initialize collision server with environment and robot:
  
  int DOF=5;// here its the number of links= n_joints+1
  string mesh_location=ros::package::getPath("urlg_robots_description");
  mesh_location.append("/meshes/allegro/convex_hull/");
  collisionChecker c_check;
  // allegro tip files:
  vector<string> r_files={"palm_link.ply","index_tip.ply", "index_tip.ply", "index_tip.ply", "thumb_tip.ply"};

  for(int i=0;i<DOF;++i)
    {
      r_files[i].insert(0,mesh_location);
    }


  // initialize collision environment:
  c_check.initialize_robot_state(DOF,r_files);

  ros::init(argc,argv,"grasp_contact_server");
  ros::NodeHandle n;

  
  //ros::Subscriber sub = n.subscribe("/kinect/camera/depth/points", 1,&collisionChecker::pcl_callback,&c_check);
  //ros::ServiceServer env_service=n.advertiseService("collision_checker/update_env_cloud",&collisionChecker::update_env_cloud,&c_check);


  ros::Publisher pub=n.advertise<pcl::PointCloud<PointXYZRGB>>("/hacd/object",1);
  //tf::TransformListener tf_l;
  
  // add publisher to collision checker:
   c_check.pub_=pub;
   
  // add tf listener to collision checker:
   tf::TransformListener tf_l;
   c_check.tf_listen=&tf_l;
  
   // Read object pointcloud from file:
   ros::Rate loop_rate(1);
   ros::ServiceClient client=n.serviceClient<tabletop_obj_segmentation::SegmentGraspObject>("object_segmenter");
   for(int i=0;i<3;i++)
     {
       c_check.add_obj_cloud(client);
       loop_rate.sleep();
     }
   // Creating ros service:
     ros::ServiceServer service=n.advertiseService("grasp/contact_checker",&collisionChecker::check_collisions,&c_check);

   ROS_INFO("Running collision server");
  
   ros::spin();
   return 0;
}
