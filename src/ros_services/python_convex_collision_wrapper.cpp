#include "convex_collision_checker.h"
#include <ros/ros.h>
#include <ros/package.h>
#include <ctime>
int main(int argc, char** argv)
{
  //
  int DOF=8;// here its the number of links= n_joints+1
  string mesh_location=ros::package::getPath("urlg_robots_description");
  mesh_location.append("/meshes/lbr4/convex_hull/vertices/");
  collisionChecker c_check;
  // robot mesh file names:
  vector<string> r_files={"link_0.ply", "link_1.ply", "link_2.ply", "link_3.ply", "link_4.ply", "link_5.ply", "link_6.ply","link_7.ply"};

  for(int i=0;i<DOF;++i)
    {
      r_files[i].insert(0,mesh_location);
    }

  // This should from your data directory:
  string pcd_file="/home/bala/pcd_data/sim_table_tf.pcd";
  

  // Publish cloud
  //ros::init(argc,argv,"collision_node");
  //ros::NodeHandle nh;
  //c_check.create_publisher(nh);

  c_check.initialize_robot_env_state(DOF,r_files,pcd_file);
  //c_check.publish_env_objects(1,argc,argv);


  c_check.view_env_objects(3); // 2 is the robot, 0 is floor, 1 is table
  /*
  clock_t begin = clock();
  while(1)
    {
      //Get robot_poses from terminal:
      
      vector<double> link_pose;
      double input;
      vector<vector<double>> robot_link_poses;

      for (int i=0;i<DOF;++i)
	{
	  cerr<<"next robot pose:"<<endl;
	  link_pose.clear();
	  for (int j=0;j<7;++j)
	    {
	      cin>>input;
	
	      link_pose.push_back(input);
	    }
	  robot_link_poses.emplace_back(link_pose);
	}
      vector<vector<collision_data>> c_data;

      c_data=c_check.check_collisions(robot_link_poses);
      //  c_check.publish_robot_objects();
      // Print out collision data:
      for(int i=0;i<c_data.size();++i)
	{
	  cout<<"robot_link: "<<i<<endl;
	  for (int j=0;j<c_data[i].size();++j)
	    {
	      cout<<c_data[i][j].collision<<" "<<c_data[i][j].point<<" "<<c_data[i][j].dir<<" "<<c_data[i][j].distance<<endl;
	    }
	}
      cerr<<"signed_distance_done"<<endl;
      cout<<"signed_distance_done"<<endl;
      
    }
  */
}
