#include "convex_collision_checker.h"
#include <ros/ros.h>
#include <ros/package.h>
#include <ctime>

// ros tf:
//#include <tf/transform_listener.h>



int main(int argc, char** argv)
{
  
  // Initialize collision server with environment and robot:
  
  int DOF=8;// here its the number of links= n_joints+1

  int handDOF=16+1;
  string mesh_location=ros::package::getPath("urlg_robots_description");
  mesh_location.append("/meshes/lbr4/convex_hull/vertices/");

  // Add allegro hand files:
  string hand_mesh_location=ros::package::getPath("urlg_robots_description");
  hand_mesh_location.append("/meshes/allegro/convex_hull/");

  collisionChecker c_check;
  // robot mesh file names:
  vector<string> r_files={"link_0.ply", "link_1.ply", "link_2.ply", "link_3.ply", "link_4.ply", "link_5.ply", "link_6.ply","link_7.ply","palm_link.ply","index_link_1.ply","index_link_2.ply","index_tip.ply","index_link_1.ply","index_link_2.ply","index_tip.ply","index_link_1.ply","index_link_2.ply","index_tip.ply","thumb_link_2.ply","thumb_link_3.ply"};

  for(int i=0;i<DOF;++i)
    {
      r_files[i].insert(0,mesh_location);
    }
  
  for (int i=DOF;i<r_files.size();++i)
    {
       r_files[i].insert(0,hand_mesh_location);
    }


  // initialize collision environment:
  c_check.initialize_robot_state(r_files.size(),r_files);

  ros::init(argc,argv,"collision_server");
  ros::NodeHandle n;

  // Read pointcloud from sensor:

  
  ros::Subscriber sub = n.subscribe("/kinect/camera/depth/points", 1,&collisionChecker::pcl_callback,&c_check);
  ros::ServiceServer env_service=n.advertiseService("collision_checker/update_env_cloud",&collisionChecker::update_env_cloud,&c_check);

  // Creating ros service:
  ros::ServiceServer service=n.advertiseService("collision_checker",&collisionChecker::check_collisions,&c_check);

  ros::Publisher pub=n.advertise<pcl::PointCloud<PointXYZRGB>>("/hacd/env_objects",1);
  tf::TransformListener tf_l;
  
  // add publisher to collision checker:
  c_check.pub_=pub;
  // add tf listener to collision checker:
  c_check.tf_listen=&tf_l;
  
  ROS_INFO("Running collision server");
  
  ros::spin();
  return 0;

}
