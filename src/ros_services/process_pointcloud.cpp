#include <ll4ma_collision_wrapper/libs/pcl_add/pcl_eigen_conversion.h>
#include <ll4ma_collision_wrapper/ProcessPointCloud.h>
using namespace pcl_eigen;


ros::Publisher pub;
bool process_cld(ll4ma_collision_wrapper::ProcessPointCloud::Request &req, ll4ma_collision_wrapper::ProcessPointCloud::Response &res)
{
  // create new instance of pcl_eigen
  pclEigen pcl_eigen;
  
  PointCloud<PointXYZ>::Ptr e_cloud(new PointCloud<PointXYZ>);
  // Get environment cloud
  pcl::fromROSMsg(req.in_cloud,*e_cloud);
  // Transform pointcloud to robot base
  pcl_eigen.transform_pointcloud(e_cloud,req.transform_mat);

  // remove nans
  pcl_eigen.removenans(e_cloud);
  // voxel filter:
  pcl_eigen.downsample_cloud(e_cloud,0.02);
  // bound cloud:
  vector<float> min_ws_bound={-0.44,-1.2,-0.05};
  vector<float> max_ws_bound={0.8,-0.4,1.2};

  //vector<float> min_ws_bound={-1.2,-1.2,0.1};
  //vector<float> max_ws_bound={1.2,1.2,1.2};

  pcl_eigen.bound_cloud(e_cloud,min_ws_bound,max_ws_bound);
  
  // Run ransac to find table
  PointCloud<PointXYZ>::Ptr table_cloud(new PointCloud<PointXYZ>);
  PointCloud<PointXYZ>::Ptr outlier_cloud(new PointCloud<PointXYZ>);
  PointCloud<PointXYZ>::Ptr obj_cloud(new PointCloud<PointXYZ>);
  pcl_eigen.find_table(e_cloud,table_cloud,outlier_cloud);
  
  // Get object cloud(assuming one object in the environment
  pcl_eigen.find_cluster(outlier_cloud,obj_cloud);
  table_cloud->header.frame_id="base_link";
  obj_cloud->header.frame_id="base_link";
  e_cloud->header.frame_id="base_link";
  // Send table point cloud and obj point cloud as response

  pcl::toROSMsg(*e_cloud,res.env_cloud);
  pcl::toROSMsg(*obj_cloud,res.obj_cloud);


  // visualize pointcloud for debugging:
  pub.publish(e_cloud);
  

  return true;
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "pointcloud_process_node");
  ros::NodeHandle n;
  ros::ServiceServer process_srv=n.advertiseService("ll4ma_collision_wrapper/get_obj_env",process_cld);
  pub=n.advertise<pcl::PointCloud<PointXYZ>>("/pcl_processor/table",1);
  ros::spin();
}

