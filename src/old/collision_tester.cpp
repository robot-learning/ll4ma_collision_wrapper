// This function should read pointcloud, robot state from ros and compute collision cost, gradient.

#include<iostream>
// Importing libccd:
#include <ccd/ccd.h>
#include <ccd/quat.h>

// Eigen library for vectors and matrices:
#include <Eigen/Dense>
#include <Eigen/Geometry>

// Pcl library headers:
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/io/pcd_io.h>

#define FILE_OPEN_ERROR(fname) throw runtime_error( (boost::format("couldn't open %s")%fname).str() )

// Headers for convex decomposition:
#include "sphere_sampling.hpp"
#include "convexdecomp.hpp"


using namespace std;
using namespace Eigen;
using namespace pcl;





// Create an object template:
struct obj_t
{
  ccd_vec3_t pos;
  
  ccd_quat_t quat;
  vector<Vector3d> vertices;
};


obj_t loadMeshModel(vector<Vector3d> points,ccd_vec3_t pos,ccd_quat_t quat)
{
  vector<Vector3d> vertices;

  Vector3d centre;
  for(int i=0;i<points.size();++i)
    {
      centre+=points[i];
    }
  centre=centre/double(points.size());

  for(int i=0;i<points.size();++i)
    {
      vertices.emplace_back(centre+points[i]);
    }
  

  obj_t obj={.pos=pos,.quat=quat,.vertices=vertices};
  
  return obj;
}


void support_convex(const void *_obj, const ccd_vec3_t *dir_, ccd_vec3_t *vec)
{
  obj_t *obj=(obj_t *)_obj;
  
  ccd_vec3_t dir, p;
  ccd_real_t maxdot, dot;
  int i;

  //const auto& center = c->convex->center;
  ccd_quat_t qinv;
  
  ccdVec3Copy(&dir, dir_);
  ccdQuatInvert2(&qinv, &obj->quat);
  ccdQuatRotVec(&dir, &qinv);

  maxdot = -CCD_REAL_MAX;
 

  for(i = 0; i < obj->vertices.size(); ++i)
  {
    ccdVec3Set(&p, obj->vertices[i][0], obj->vertices[i][1] ,obj->vertices[i][2]);
    dot = ccdVec3Dot(&dir, &p);

    if(dot > maxdot)
    {
      ccdVec3Set(vec, obj->vertices[i][0], obj->vertices[i][1] ,obj->vertices[i][2]);
      maxdot = dot;
    }

  }

  // transform support vertex
  ccdQuatRotVec(vec, &obj->quat);
  ccdVec3Add(vec, &obj->pos);
}

void removenans(pcl::PCLPointCloud2& cloud, float fillval=0) {
  int npts = cloud.width * cloud.height;
  for (int i=0; i < npts; ++i) {
    float* ptdata = (float*)(cloud.data.data() + cloud.point_step * i);
    for (int j=0; j < 3; ++j) {
      if (!isfinite(ptdata[j])) ptdata[j] = fillval;
    }
  }
}

int main (int argc, char** argv)
{
  // Import pointcloud:
  string pcdfile="/home/bala/pcd_data/table_objects.pcd";
  pcl::PCLPointCloud2 cloud_blob;
  PointCloud<pcl::PointXYZ>::Ptr cloud (new typename pcl::PointCloud<pcl::PointXYZ>);

  if (pcl::io::loadPCDFile (pcdfile, cloud_blob) != 0) FILE_OPEN_ERROR(pcdfile);
  removenans(cloud_blob);
  pcl::fromPCLPointCloud2 (cloud_blob, *cloud);

  // Downsample cloud:
  float vsize=0.05f;
  pcl::VoxelGrid<PointXYZ > sor;
  sor.setInputCloud (cloud);
  sor.setLeafSize (vsize, vsize, vsize);
  sor.filter (*cloud);

  PointCloud<PointXYZ>::Ptr cloud_ds =cloud;

  // Perform convex decomposition:
  vector<vector<int> > label2inds;
  MatrixXf dirs = getSpherePoints(1);

  ConvexDecomp(cloud_ds, dirs, 0.06, &label2inds, NULL);
  cout<<" Got convex decomposition"<<endl;


  // Get two objects:

  typedef pair<int,int> IntPair;
  vector< IntPair > size_label;
  for (int i=0; i < label2inds.size(); ++i) {
    size_label.push_back(IntPair(-label2inds[i].size(), i));
  }

  std::sort(size_label.begin(), size_label.end());


  visualization::PCLVisualizer  viewer ("3d Viewer");
  viewer.setBackgroundColor (0,0,0);

  pcl::PointCloud<PointXYZRGB>::Ptr cloud_obj1(new PointCloud<PointXYZRGB>);
  vector<Vector3d> vertices_obj1;
  int label = size_label[0].second;
  
  float r=rand()%255, g = rand()%255, b = rand()%255;

  for (int j=0; j < label2inds[label].size(); ++j) {
    PointXYZ  oldpt = cloud_ds->points[label2inds[label][j]];
    PointXYZRGB newpt;
    newpt.x = oldpt.x;
    newpt.y = oldpt.y;
    newpt.z = oldpt.z;
    newpt.r = r;
    newpt.g = g;
    newpt.b = b;
    cloud_obj1->push_back(newpt);
    vertices_obj1.emplace_back(oldpt.x,oldpt.y,oldpt.z);
  }
  pcl::io::savePCDFileBinary("/home/bala/pcd_data/convex_table.pcd", *cloud_obj1);

  viewer.addPointCloud(cloud_obj1, (boost::format("cloud_1")).str());

  pcl::PointCloud<PointXYZRGB>::Ptr cloud_obj2(new PointCloud<PointXYZRGB>);
  vector<Vector3d> vertices_obj2;
  label = size_label[1].second;
  
  r=rand()%255, g = rand()%255, b = rand()%255;

  for (int j=0; j < label2inds[label].size(); ++j) {
    PointXYZ  oldpt = cloud_ds->points[label2inds[label][j]];
    PointXYZRGB newpt;
    newpt.x = oldpt.x;
    newpt.y = oldpt.y;
    newpt.z = oldpt.z;
    newpt.r = r;
    newpt.g = g;
    newpt.b = b;
    cloud_obj2->push_back(newpt);
    vertices_obj2.emplace_back(oldpt.x,oldpt.y,oldpt.z);
  }
    viewer.addPointCloud(cloud_obj2, (boost::format("cloud_2")).str());


  while (!viewer.wasStopped ())
  {
    viewer.spinOnce (100);
    boost::this_thread::sleep (boost::posix_time::microseconds (100000));
  }


  
  // Create object models:
  ccd_vec3_t pos;
  ccd_quat_t quat;
  pos={.v={0.0,0.0,0.0}};
  quat={.q={0.0,0.0,0.0,1.0}};
  obj_t obj1=loadMeshModel(vertices_obj1,pos,quat);
  pos={.v={0.,0.0,0.0}};
  quat={.q={0.0,0.0,0.0,1.0}};
  obj_t obj2=loadMeshModel(vertices_obj2,pos,quat);
  

  
  ccd_t ccd;
  CCD_INIT(&ccd); // initialize ccd_t struct
      
  // set up ccd_t struct
  ccd.support1       = support_convex; // support function for first object
  ccd.support2       = support_convex; // support function for second object
  ccd.max_iterations = 100;     // maximal number of iterations
  ccd.epa_tolerance  = 0.0001;  // maximal tolerance fro EPA part

  ccd_real_t depth;
  ccd_vec3_t dir, point_pos;
  
  int intersect = ccdGJKPenetration(&obj1, &obj2, &ccd, &depth, &dir, &point_pos);
  //int intersect = ccdGJKIntersect(&obj1, &obj2, &ccd);
  
  cout<<"Not in collision?: "<<bool(intersect*-1)<<endl;
  cout<<"Position of seperation vector: "<<*point_pos.v<<endl;
  cout<<"Seperation vector if not in collision: "<<*dir.v<<endl;
  cout<<"Penetration depth: "<<depth<<endl;

  return (0);
}
