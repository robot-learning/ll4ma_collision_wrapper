#include <ll4ma_collision_wrapper/scenes/rob_env.h>
#include <ros/ros.h>
#include <ros/package.h>
#include <ctime>
int main()
{
  //
  int DOF=8;
  string mesh_location=ros::package::getPath("urlg_robots_description");
  

  mesh_location.append("/meshes/lbr4/convex_hull/vertices/");
  collisionChecker c_check;
  // robot mesh file names:
  vector<string> r_files={"link_0.ply", "link_1.ply", "link_2.ply", "link_3.ply", "link_4.ply", "link_5.ply", "link_6.ply","link_7.ply"};

  for(int i=0;i<DOF;++i)
    {
      r_files[i].insert(0,mesh_location);
    }

  // This should from your data directory:
  string pcd_file="/home/bala/pcd_data/table.pcd";
  
  c_check.initialize_robot_env_state(DOF,r_files,pcd_file);


  clock_t begin = clock();

  for (int i=0;i<2;++i)  
    {
      
      
      // Get robot poses from manually:
      //vector<double> link_pose={0.0,0.0,0.0,0.0,0.0,0.0,1.0};
      vector<vector<double>> robot_link_poses;      
      robot_link_poses.emplace_back(vector<double>{1.0,0.0,0.0,0.0,0.0,0.0,1.0});
      robot_link_poses.emplace_back(vector<double>{1.0,0.0,0.11,0.0,0.0,0.0,1.0});	    
      robot_link_poses.emplace_back(vector<double>{1.0,0.0,0.31,0.0,0.0,0.0,1.0}); 
      robot_link_poses.emplace_back(vector<double>{1.0,0.0,0.502,0.0,0.0,0.0,1.0});
      robot_link_poses.emplace_back(vector<double>{1.0,0.0,0.702,0.0,0.0,0.0,1.0});
      robot_link_poses.emplace_back(vector<double>{1.0,0.0,0.902,0.0,0.0,0.0,1.0});
      robot_link_poses.emplace_back(vector<double>{1.0,0.0,1.092,0.0,0.0,0.0,1.0});
      robot_link_poses.emplace_back(vector<double>{1.0,0.0,1.17,0.0,0.0,0.0,1.0});
     
      vector<vector<collision_data>> c_data;
      c_data=c_check.check_collisions(robot_link_poses);

      // Print out collision data:
      for(int i=0;i<c_data.size();++i)
	{
	  //cout<<"robot_link: "<<i<<endl;
	  for (int j=0;j<c_data[i].size();++j)
	    {
	      //cout<<c_data[i][j].collision<<" "<<c_data[i][j].point<<" "<<c_data[i][j].dir<<" "<<c_data[i][j].distance<<endl;
	    }
	}
      //cerr<<"signed_distance_done"<<endl;
      //cout<<"signed_distance_done"<<endl;
      cerr<<i<<endl;
    }
    clock_t end = clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    cerr<<"Time taken: "<<elapsed_secs<<endl;
}
