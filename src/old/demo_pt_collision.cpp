// This program computes the signed distance between a mesh and a point
#include "collision_checker/convex_collision_checker.h"
#include <ros/ros.h>
#include <ros/package.h>
#include <ctime>
int main()
{
  //
  int DOF=1;

  string mesh_location = "/home/bala/catkin_ws/src/urlg_robots_description";//

  mesh_location.append("/meshes/lbr4/convex_hull/vertices/");
  collisionChecker c_check;
  // robot mesh file names:
  vector<string> r_files={"link_0.ply"};

  for(int i=0;i<DOF;++i)
    {
      r_files[i].insert(0,mesh_location);
    }
  
  
  c_check.initialize_robot_state(DOF,r_files);


  clock_t begin = clock();

  // Get robot poses from manually:
  //vector<double> link_pose={0.0,0.0,0.0,0.0,0.0,0.0,1.0};
  vector<vector<double>> robot_link_poses;      
  robot_link_poses.emplace_back(vector<double>{1.0,0.0,0.0,0.0,0.0,0.0,1.0});
  vector<int> link_idx={0};
  c_check.update_robot_state(robot_link_poses,link_idx);
  
  vector<collision_data> c_data;
  vector<vector<double>> pts;
  vector<double> pt={1.0,0.0,0.0};
  pts.push_back(pt);
  c_data=c_check.check_pt_collisions(pts,link_idx);
  cerr<<"collision data size: "<<c_data.size()<<endl;
  // Print out collision data:
  for(int i=0;i<c_data.size();++i)
  {
    cout<<"collision: "<<c_data[i].collision<<" pt: "<<c_data[i].point<<" direction: "<<c_data[i].dir<<" p_d: "<<c_data[i].distance<<endl;
  }
    clock_t end = clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    cerr<<"Time taken: "<<elapsed_secs<<endl;
}
