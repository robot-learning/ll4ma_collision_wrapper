// This function should read pointcloud, robot state from ros and compute collision cost, gradient.

#include<iostream>
// Importing libccd:
#include <ccd/ccd.h>
#include <ccd/quat.h>

// Eigen library for vectors and matrices:
#include <Eigen/Dense>
#include <Eigen/Geometry>

// Pcl library headers:
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#define FILE_OPEN_ERROR(fname) throw runtime_error( (boost::format("couldn't open %s")%fname).str() )

// Headers for convex decomposition:
#include "sphere_sampling.hpp"
#include "convexdecomp.hpp"

// Headers for visualization using osg:
//#include "../viz/osg_viewer.h"

using namespace std;
using namespace Eigen;
using namespace pcl;





// Create an object template:
struct obj_t
{
  ccd_vec3_t pos;
  
  ccd_quat_t quat;
  vector<Vector3d> vertices;
};


void centrePCL(pcl::PointCloud<PointXYZ>::Ptr cloud)
{
  Vector3d centre;
  for (int j=0; j < cloud->size(); ++j)
    {
    PointXYZ  oldpt = cloud->points[j];
    centre+=Vector3d(oldpt.x,oldpt.y,oldpt.z);
    }
  
  // Check for zero :
  for(int i=0;i<3;++i)
    {
      if(centre[i]!=0)
	{
	  centre[i]=centre[i]/double(cloud->size());
	}
    }
  
  //cout<<centre<<endl;
 for (int j=0; j < cloud->size(); ++j)
    {
    PointXYZ  oldpt = cloud->points[j];
    
    oldpt.x=oldpt.x-centre[0];
    oldpt.y=oldpt.y-centre[1];
    oldpt.z=oldpt.z-centre[2];
    cloud->points[j]=oldpt;
    }
  
}
/*
vector<Vector3d> loadPCLtoVector(pcl::PointCloud<PointXYZRGB>::Ptr cloud)
{

  vector<Vector3d> vertices; 
				 
  for (int j=0; j < cloud->size(); ++j)
    {
    PointXYZ  oldpt = cloud->points[j];
    PointXYZRGB newpt;
    newpt.x = oldpt.x;
    newpt.y = oldpt.y;
    newpt.z = oldpt.z;
    newpt.r = r;
    newpt.g = g;
    newpt.b = b;
    vertices.emplace_back(oldpt.x,oldpt.y,oldpt.z);
    }

  // Move vertices points to centre:
  Vector3d centre;
  for(int i=0;i<points.size();++i)
    {
      centre+=points[i];
    }
  
  centre=centre/double(points.size());
  cout<<centre<<endl;

  return vertices;
}
*/
  


obj_t loadMeshModel(vector<Vector3d> points,ccd_vec3_t pos,ccd_quat_t quat)
{
  vector<Vector3d> vertices;

  Vector3d centre;
  for(int i=0;i<points.size();++i)
    {
      centre+=points[i];
    }
  
  centre=centre/double(points.size());
  
  for(int i=0;i<points.size();++i)
    {
      vertices.emplace_back(points[i]-centre);
    }
  

  obj_t obj={.pos=pos,.quat=quat,.vertices=vertices};
  
  return obj;
}


void support_convex(const void *_obj, const ccd_vec3_t *dir_, ccd_vec3_t *vec)
{
  obj_t *obj=(obj_t *)_obj;
  
  ccd_vec3_t dir, p;
  ccd_real_t maxdot, dot;
  int i;

  //const auto& center = c->convex->center;
  ccd_quat_t qinv;
  
  ccdVec3Copy(&dir, dir_);
  ccdQuatInvert2(&qinv, &obj->quat);
  ccdQuatRotVec(&dir, &qinv);

  maxdot = -CCD_REAL_MAX;
 

  for(i = 0; i < obj->vertices.size(); ++i)
  {
    ccdVec3Set(&p, obj->vertices[i][0], obj->vertices[i][1] ,obj->vertices[i][2]);
    dot = ccdVec3Dot(&dir, &p);

    if(dot > maxdot)
    {
      ccdVec3Set(vec, obj->vertices[i][0], obj->vertices[i][1] ,obj->vertices[i][2]);
      maxdot = dot;
    }

  }

  // transform support vertex
  ccdQuatRotVec(vec, &obj->quat);
  ccdVec3Add(vec, &obj->pos);
}

void removenans(pcl::PCLPointCloud2& cloud, float fillval=0) {
  int npts = cloud.width * cloud.height;
  for (int i=0; i < npts; ++i) {
    float* ptdata = (float*)(cloud.data.data() + cloud.point_step * i);
    for (int j=0; j < 3; ++j) {
      if (!isfinite(ptdata[j])) ptdata[j] = fillval;
    }
  }
}

int main (int argc, char** argv)
{
  // Import pointcloud:
  string pcdfile="/home/bala/pcd_data/convex_table.pcd";
  pcl::PCLPointCloud2 cloud_table_p2;
  pcl::PCLPointCloud2 cloud_robot_p2;
  PointCloud<pcl::PointXYZ>::Ptr cloud_table (new typename pcl::PointCloud<pcl::PointXYZ>);
  PointCloud<pcl::PointXYZ>::Ptr cloud_robot (new typename pcl::PointCloud<pcl::PointXYZ>);
  if (pcl::io::loadPLYFile ("/home/bala/link4_convex.ply", cloud_robot_p2) != 0) FILE_OPEN_ERROR("/home/bala/link4.ply");
  if (pcl::io::loadPCDFile (pcdfile, cloud_table_p2) != 0) FILE_OPEN_ERROR(pcdfile);
  removenans(cloud_table_p2);
  removenans(cloud_robot_p2);
    
  pcl::fromPCLPointCloud2 (cloud_table_p2, *cloud_table);
  pcl::fromPCLPointCloud2 (cloud_robot_p2, *cloud_robot);
  //visualization::PCLVisualizer  viewer ("3d Viewer");
  //viewer.setBackgroundColor (0,0,0);

  pcl::PointCloud<PointXYZRGB>::Ptr cloud_obj1(new PointCloud<PointXYZRGB>);
  vector<Vector3d> vertices_obj1;  
  float r=rand()%255, g = rand()%255, b = rand()%255;
  centrePCL(cloud_table);
  for (int j=0; j < cloud_table->size(); ++j) {
    PointXYZ  oldpt = cloud_table->points[j];
    PointXYZRGB newpt;
    newpt.x = oldpt.x;
    newpt.y = oldpt.y;
    newpt.z = oldpt.z;
    newpt.r = r;
    newpt.g = g;
    newpt.b = b;
    cloud_obj1->push_back(newpt);
    vertices_obj1.emplace_back(oldpt.x,oldpt.y,oldpt.z);
  }  
  //viewer.addPointCloud(cloud_obj1, (boost::format("cloud_1")).str());

  pcl::PointCloud<PointXYZRGB>::Ptr cloud_obj2(new PointCloud<PointXYZRGB>);

  vector<Vector3d> vertices_obj2;// This is the robot

  centrePCL(cloud_robot);
  r=rand()%255, g = rand()%255, b = rand()%255;
  
  for (int j=0; j < cloud_robot->size(); ++j) {
    PointXYZ  oldpt = cloud_robot->points[j];
    PointXYZRGB newpt;
    newpt.x = oldpt.x;
    newpt.y = oldpt.y;
    newpt.z = oldpt.z;
    newpt.r = r;
    newpt.g = g;
    newpt.b = b;
    cloud_obj2->push_back(newpt);
    vertices_obj2.emplace_back(oldpt.x,oldpt.y,oldpt.z);
  }

  //viewer.addPointCloud(cloud_obj2, (boost::format("cloud_2")).str());

  
  
  

  
  // Create object models:
  ccd_vec3_t pos;
  ccd_quat_t quat;
  pos={.v={0.0,0.0,0.0}};
  quat={.q={0.0,0.0,0.0,1.0}};
  obj_t obj1=loadMeshModel(vertices_obj1,pos,quat);
  pos={.v={0.,0.0,0.0}};
  quat={.q={0.0,0.0,0.0,1.0}};
  obj_t obj2=loadMeshModel(vertices_obj2,pos,quat);
  

  
  ccd_t ccd;
  CCD_INIT(&ccd); // initialize ccd_t struct
      
  // set up ccd_t struct
  ccd.support1       = support_convex; // support function for first object
  ccd.support2       = support_convex; // support function for second object
  ccd.max_iterations = 1000;     // maximal number of iterations
  ccd.epa_tolerance  = 0.00001;  // maximal tolerance fro EPA part

  ccd_real_t depth;
  ccd_vec3_t dir, point_pos;
  
  int intersect = ccdGJKPenetration(&obj1, &obj2, &ccd, &depth, &dir, &point_pos);
  //int intersect = ccdGJKIntersect(&obj1, &obj2, &ccd);
  bool collision=false;
  if(intersect==0)
    {
      collision=true;
    }
  cout<<"Collision?: "<<collision<<' '<<intersect<<endl;
  cout<<"Position of seperation vector: "<<point_pos.v[0]<<' '<<point_pos.v[1]<<' '<<point_pos.v[2]<<endl;
  cout<<"Seperation vector: "<<dir.v[0]<<' '<<dir.v[1]<<' '<<dir.v[2]<<endl;
  cout<<"Penetration depth: "<<depth<<endl;

  // Add closest point to pcl
  // Add new point cloud to viz:
  r=rand()%255, g = rand()%255, b = rand()%255;  
  PointXYZRGB newpt;
  newpt.x = point_pos.v[0];
  newpt.y = point_pos.v[1];
  newpt.z = point_pos.v[2];
  newpt.r = r;
  newpt.g = g;
  newpt.b = b;
  cloud_obj2->push_back(newpt);
 


  
  // Move robot away from collision:
  // Get the vector from the robot mesh origin to the closest point:
  Vector3d robot_point,new_robot_pose;
  Vector3d point;
  for( int i=0;i<3;++i)
    {
      point[i]=dir.v[i]-point_pos.v[i];
      robot_point[i]=point_pos.v[i]-0;// TODO: Change to match robot link pose from TF
    }

  // Move the robot by -depth to get away from collision:
  new_robot_pose=robot_point+depth*point;

  // Add new point cloud to viz:
  r=rand()%255, g = rand()%255, b = rand()%255;  
  for (int j=0; j < cloud_robot->size(); ++j) {
    PointXYZ  oldpt = cloud_robot->points[j];
    PointXYZRGB newpt;
    newpt.x = oldpt.x+new_robot_pose[0];
    newpt.y = oldpt.y+new_robot_pose[1];
    newpt.z = oldpt.z+new_robot_pose[2];
    newpt.r = r;
    newpt.g = g;
    newpt.b = b;
    cloud_obj2->push_back(newpt);
    //vertices_obj1.emplace_back(oldpt.x,oldpt.y,oldpt.z);
  }

  //viewer.addPointCloud(cloud_obj2, (boost::format("cloud_3")).str());

  /*
  while (false)
  {
    viewer.spinOnce (100);
    boost::this_thread::sleep (boost::posix_time::microseconds (100000));
  }
  */


  /*
    TODO: Work on visualization using openscenegraph
  // Create osg viewer and test:
  worldViz osg_world;
  
  // Initialize world:
  osg_world.init_world();

  // Add convex shape:
  osg_world.add_convex_shape(vertices_obj1);
  osg_world.update_viewer();
  osg_world.run_viewer();
  */
  return (0);
}
