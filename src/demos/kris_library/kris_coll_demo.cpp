#include <ll4ma_collision_wrapper/libs/kris_library/collision_checker.h>
#include <tf/transform_broadcaster.h>
#include <visualization_msgs/Marker.h>
using namespace Meshing;
using namespace Geometry;

tf::Transform get_transform(vector<double> pose)
{
  tf::Transform transform;
  transform.setOrigin( tf::Vector3(pose[0],pose[1],pose[2]) );
  tf::Quaternion q(pose[3],pose[4],pose[5],pose[6]);
  //tf::Quaternion q(0,0,0,1);
  transform.setRotation(q);
  //cerr<<transform.getBasis().pitch<<endl;
  return transform;
}
int main(int argc,char** argv)
{
  ros::init(argc,argv,"kris_coll_demo");
  ros::NodeHandle n;
  ros::Rate rate(10);

  char* mesh_location="/home/bala/object_models/mustard_bottle/google_16k/nontextured.stl";
  TriMesh* obj1=new TriMesh();
  kris_ccheck::collisionChecker ccheck;
  ccheck.loadModel(mesh_location, obj1);

  TriMesh* obj2=new TriMesh();
  
  char* mesh_location_2="/home/bala/catkin_ws/src/ll4ma_collision_wrapper/data/in_hand_table.stl";
  ccheck.loadModel(mesh_location_2, obj2);

  char* pcd_location="/home/bala/catkin_ws/src/ll4ma_collision_wrapper/data/table.pcd";
  Eigen::Matrix4d T_;
  T_.setIdentity();
  CollisionMesh* table_obj=new CollisionMesh();
  
  table_obj=ccheck.loadPCDFile(pcd_location,T_);
  // create collision meshes:
  Eigen::Matrix4d T;
  T.setIdentity();
  CollisionMesh* coll_1=new CollisionMesh();
  coll_1=ccheck.createCollObj(obj1,T);
  
  CollisionMesh* coll_2=new CollisionMesh();
  coll_2=ccheck.createCollObj(obj2,T);

  // update poses:
  vector<double> obj1_pose={0.3,0.2,0.3,0,0,0,1};
  vector<double> obj2_pose={0.08,0.0,0.1,0,0,0,1};
  ccheck.updatePose(coll_1,obj1_pose);
  ccheck.updatePose(coll_2,obj2_pose);

  coll_data res;

  ccheck.signedDistance(coll_1,coll_2,res,true);
  cerr<<res.collision<<endl;
  // nearest points:
  cerr<<res.p1.transpose()<<endl;
  cerr<<res.p2.transpose()<<endl;
  vector<double> point={0,0,0,0,0,0,1};
  cerr<<res.distance<<endl;
  cerr<<(res.p2-res.p1).norm()<<endl;

  // visualize objects in rviz:
  // publish tf frames for objects and closest points:

  // publish markers
  ros::Publisher p1_pub = n.advertise<visualization_msgs::Marker>("obj1/point", 1);
  ros::Publisher p2_pub = n.advertise<visualization_msgs::Marker>("obj2/point", 1);
  ros::Publisher obj1_pub = n.advertise<visualization_msgs::Marker>("obj1/mesh", 1);
  ros::Publisher obj2_pub = n.advertise<visualization_msgs::Marker>("obj2/mesh", 1);

  uint32_t shape = visualization_msgs::Marker::SPHERE;

  
  
  // markers:
  visualization_msgs::Marker marker;
  marker.header.frame_id = "/obj1/point";
  marker.ns = "basic_shapes";
  marker.id = 0;
  
  // Set the marker type.  Initially this is CUBE, and cycles between that and SPHERE, ARROW, and CYLINDER
  marker.type = shape;
  
  // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
  marker.action = visualization_msgs::Marker::ADD;
  
  // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
  marker.pose.position.x = 0;
  marker.pose.position.y = 0;
  marker.pose.position.z = 0;
  marker.pose.orientation.x = 0.0;
  marker.pose.orientation.y = 0.0;
  marker.pose.orientation.z = 0.0;
  marker.pose.orientation.w = 1.0;
  
  // Set the scale of the marker -- 1x1x1 here means 1m on a side
  marker.scale.x = 0.01;
  marker.scale.y = 0.01;
  marker.scale.z = 0.01;

    // Set the color -- be sure to set alpha to something non-zero!
  marker.color.r = 1.0f;
  marker.color.g = 0.0f;
  marker.color.b = 0.0f;
  marker.color.a = 1.0;

  marker.lifetime = ros::Duration();

  visualization_msgs::Marker p2_marker=marker;
  p2_marker.header.frame_id = "obj2_point";
  visualization_msgs::Marker p1_marker=marker;
  p1_marker.header.frame_id = "obj1_point";

  // mesh marker:
  visualization_msgs::Marker obj1_marker=marker;
  obj1_marker.header.frame_id = "obj1";
  obj1_marker.scale.x=1.0;
  obj1_marker.scale.y=1.0;
  obj1_marker.scale.z=1.0;
  obj1_marker.color.r=0.0f;
  obj1_marker.color.g=1.0f;
  obj1_marker.color.a=0.8;
  uint32_t mesh_shape= visualization_msgs::Marker::MESH_RESOURCE;
  obj1_marker.type=mesh_shape;
  obj1_marker.mesh_resource="file:///home/bala/object_models/mustard_bottle/google_16k/nontextured.stl";
  visualization_msgs::Marker obj2_marker=obj1_marker;
  obj2_marker.header.frame_id="obj2";
  obj2_marker.color.g=0.8f;
  obj2_marker.color.r=0.8f;
  obj2_marker.color.b=0.8f;
  obj2_marker.mesh_resource="file:///home/bala/catkin_ws/src/ll4ma_collision_wrapper/data/in_hand_table.stl";

  static tf::TransformBroadcaster br;
  
  while(ros::ok())
  {
    tf::Transform transform=get_transform(obj1_pose);

    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world", "obj1"));    
    
    transform=get_transform(obj2_pose);
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world", "obj2"));

    point[0]=res.p1[0];
    point[1]=res.p1[1];
    point[2]=res.p1[2];
    transform=get_transform(point);
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world", "obj1_point"));

    point[0]=res.p2[0];
    point[1]=res.p2[1];
    point[2]=res.p2[2];
    transform=get_transform(point);
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world", "obj2_point"));

    // markers:
    p1_pub.publish(p1_marker);
    p2_pub.publish(p2_marker);
    obj1_pub.publish(obj1_marker);
    obj2_pub.publish(obj2_marker);
    
    ros::spinOnce();
    rate.sleep();
  }
  

  // visualize closest points

  
}

