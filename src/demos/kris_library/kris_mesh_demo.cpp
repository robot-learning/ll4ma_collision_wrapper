#include <ros/ros.h>
#include<KrisLibrary/meshing/Meshing.h>
#include<KrisLibrary/meshing/IO.h>

#include<KrisLibrary/geometry/AnyGeometry.h>
using namespace std;
int main(int argc, char** argv)
{
  // load obj1
  char* mesh_location="/home/bala/object_models/mustard_bottle/google_16k/nontextured.stl";
    
  Meshing::TriMesh obj_1;
  Meshing::LoadAssimp(mesh_location,obj_1);

  // load obj2
  char* mesh_location_2="/home/bala/object_models/mustard_bottle/google_16k/nontextured.stl";
    
  Meshing::TriMesh obj_2;
  Meshing::LoadAssimp(mesh_location_2,obj_2);

  // load the meshes as collision meshes

  Geometry::CollisionMesh obj1(obj_1);
  Geometry::CollisionMesh obj2(obj_2);
  Math3D::RigidTransform T;
  Math3D::Vector3 trans(0.001,0.0,0.);
  obj1.GetTransform(T);
  T.setTranslation(trans);
  obj1.UpdateTransform(T);
  
  // update pose:

  // check collision
  Geometry::CollisionMeshQuery coll_obj(obj1,obj2);
  std::cerr<<"Collision: "<<coll_obj.Collide()<<std::endl;

  Math3D::Vector3 p1,p2,d1;

  cerr<<"distance: "<<coll_obj.Distance(0.0001,0.0001)<<endl;
  coll_obj.ClosestPoints(p1,p2);
  cerr<<"Closest points: "<<p1<<" "<<p2<<endl;

  cerr<<"penetration depth: "<<coll_obj.PenetrationDepth()<<endl;

  coll_obj.PenetrationPoints(p1,p2,d1);
  
  cerr<<"Penetration points: "<<p1<<" "<<p2<<" "<<d1<<endl;
  
}
