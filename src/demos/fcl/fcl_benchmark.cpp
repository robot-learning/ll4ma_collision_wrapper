#include <ll4ma_collision_wrapper/libs/fcl/collision_checker.h>
#include <array>
#include <fstream>
#include <iostream>
#include <ros/ros.h>
#include <ros/package.h>
#include <ctime>
#include <chrono>

using namespace fcl;
using namespace fcl_ccheck;
typedef double S;


int main(int argc, char* argv[])
{

  using milli = std::chrono::milliseconds;
  auto start = std::chrono::high_resolution_clock::now();
  auto finish = std::chrono::high_resolution_clock::now();
  double elapsed_secs=std::chrono::duration_cast<milli>(finish - start).count();
  double ms=1000.0;
  
  collisionChecker c_check;
  start = std::chrono::high_resolution_clock::now();
  // This should from your data directory:
  string pcd_file=ros::package::getPath("ll4ma_collision_wrapper");
  pcd_file.append("/data/table.pcd");
  ROS_INFO_STREAM("Loading pcd file from location " << pcd_file);

  PointCloud<PointXYZ>::Ptr env_cloud(new PointCloud<PointXYZ>);

  
  // load pointcloud from file
  c_check._pcl_eigen.loadPCD(pcd_file,env_cloud);

  float resolution = 0.1f;
  
  // convert to normal octree:
  octomap::OcTree* tree = new octomap::OcTree(resolution);
  // load pointcloud to octree:
  c_check._pcl_eigen.loadPCLOctree(env_cloud,tree);


  // create collision objects from octree boxes:
  std::vector<CollisionObject<S>*> boxes=c_check.loadOctreeCollObj(tree);
  
  finish = std::chrono::high_resolution_clock::now();
  cerr<<"Time to load PointCloud to collision boxes(octree): "<<std::chrono::duration_cast<milli>(finish - start).count()/ms<<endl;
  // check with primitive shape:
  using CollisionGeometryPtr_t = std::shared_ptr<fcl::CollisionGeometry<S>>;

  CollisionGeometryPtr_t boxGeometry (new fcl::Box<S> (0.4, 0.5, 0.5));
  fcl::Transform3<S> tf2 = fcl::Transform3<S>::Identity();
  
  fcl::CollisionObject<S>* box = new CollisionObject<S>(boxGeometry, tf2);
  
  

  vector<double> obj_position={0.0,0.0,0.0};

  //TODO:Broadphase collision checking
  //DynamicAABBTreeCollisionManager<S>* manager = new DynamicAABBTreeCollisionManager<S>();
  //manager->registerObjects(boxes);
  //manager->setup();
  //manager->octree_as_geometry_collide = true;
  //manager->octree_as_geometry_distance = true;

  start = std::chrono::high_resolution_clock::now();
  
  int i=0;
    
  while(i<1000)
  {
    Vector3d dir(obj_position[0],obj_position[1],obj_position[2]);
    box->setTranslation(dir);


    // check collision between box and octomap env:
    DistanceResultd res;
    c_check.signedDistance(box,boxes,res);
    
    // add offset to position:
    obj_position[0]=0.0;
    obj_position[2]+=0.001;
    i++;
  }

  finish = std::chrono::high_resolution_clock::now();
  cerr<<"Time to compute signed distance(1000 calls): "<<std::chrono::duration_cast<milli>(finish - start).count()/ms<<endl;
  
  // read collision shapes from urdf:
  // read urdf from parameter server:
  ros::init(argc, argv, "fcl_demo");
  
  ros::NodeHandle nh;

  std::string robot_desc_string;
  nh.param("robot_description", robot_desc_string, std::string());
  vector<string> link_names={"lbr4_0_link", "lbr4_1_link", "lbr4_2_link", "lbr4_3_link", "lbr4_4_link", "lbr4_5_link", "lbr4_6_link"};
  vector<CollisionObjectd*> c_objs;
  vector<Transform3d> l_offsets;
  c_check.readLinkShapes(robot_desc_string,link_names,c_objs,l_offsets);
  
  return 0;

}
