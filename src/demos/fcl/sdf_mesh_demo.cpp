#include <ll4ma_collision_wrapper/libs/fcl/collision_checker.h>
#include <array>
#include <fstream>
#include <iostream>


using namespace fcl;
using namespace fcl_ccheck;

int main(int argc, char* argv[])
{
  
  
  collisionChecker c_check;
  
  std::vector<fcl::Vector3<double>> vertices;
  std::vector<fcl::Triangle> triangles;
  typedef fcl::BVHModel<fcl::OBBRSS<double>> Model;
  Model *model = new Model();  
  c_check.loadModelPLY("/home/bala/object_models/pringles/google_16k/nontextured.ply",model);

  Model *model_b = new Model();  
  c_check.loadModelPLY("/home/bala/object_models/pringles/google_16k/nontextured.ply",model_b);

  // Create collision objects:
  Eigen::Matrix4d T;
  T.setIdentity();
  CollisionObjectd* c1=c_check.createCollObj(model,T);
  T(0,3)=0.01;
  //T(1,3)=0.01;
  //T(2,3)=0.1;

  CollisionObjectd* c2=c_check.createCollObj(model_b,T);
  
  // Check for collision:
  CollisionResult<double> result;
  c_check.checkCollision(c1,c2,result);
  cerr<<result.isCollision()<<endl;
  
  DistanceResult<double> result_d;
  // perform distance test
  c_check.signedDistance(c1, c2, result_d);
  cerr<<result_d.min_distance<<endl;

  // move obj_1:
  Vector3d dir=c1->getTranslation()-result_d.min_distance*(result_d.nearest_points[1]);
  c1->setTranslation(dir);
  cerr<<dir.transpose()<<endl;
  
  c_check.checkCollision(c1,c2,result);
  cerr<<result.isCollision()<<endl;
  
  c_check.signedDistance(c1, c2, result_d);
  cerr<<result_d.min_distance<<endl;

  
  dir=c1->getTranslation()-result_d.min_distance*(result_d.nearest_points[1]);
  c1->setTranslation(dir);
  cerr<<dir.transpose()<<endl;
  c_check.checkCollision(c1,c2,result);
  cerr<<result.isCollision()<<endl;
  
  c_check.signedDistance(c1, c2, result_d);
  cerr<<result_d.min_distance<<endl;

  /*
  dir=c1->getTranslation()-result_d.min_distance*(result_d.nearest_points[0]-  result_d.nearest_points[1]);
  c1->setTranslation(dir);
  c_check.checkCollision(c1,c2,result);
  cerr<<result.isCollision()<<endl;
  */

  // visualize the two meshes:

  // visualize the desired translation from the collision 
  return 0;

}


// Load meshes

// build signed distance fields

// return collisions

// return signed distance


