// This demo shows how to load sdf for a robot model
#include <ros/package.h>
#include <urdf/model.h>
#include <srdfdom/model.h>
//#include <moveit_core/robot_model.h>

#include <string>
#include <fstream>
#include <streambuf>

#include <moveit/robot_model/robot_model.h>
#include <moveit/collision_distance_field/collision_robot_distance_field.h>
#include <moveit/collision_distance_field/collision_robot_hybrid.h>

int main (int argc, char** argv)
{
  // load robot model urdf from string
  // TODO: load from param:
  std::string pkg_path=ros::package::getPath("ll4ma_collision_wrapper");

  std::string urdf_file=pkg_path;
  std::string srdf_file=pkg_path;
  urdf_file.append("/data/lbr4_kdl.urdf");
  
    
  std::ifstream t(urdf_file);
  std::string urdf_string_((std::istreambuf_iterator<char>(t)),
                 std::istreambuf_iterator<char>());
  

  // load urdf from string:
  urdf::Model model;
  if (!model.initString(urdf_string_))
  {
    ROS_ERROR("Failed to parse urdf file");
  }

  urdf::ModelInterfaceSharedPtr ur_model(new urdf::ModelInterface(model));
  // load robot model from srdf
  srdf::ModelSharedPtr s_model(new srdf::Model());
  srdf_file.append("/config/lbr4.srdf");
  std::ifstream t_(srdf_file);
  std::string srdf_string_((std::istreambuf_iterator<char>(t_)),
                 std::istreambuf_iterator<char>());

    
  if(!s_model->initString(model,srdf_string_))
  {
     ROS_ERROR("Failed to parse srdf file");
  }


  // build moveit robot model:
  moveit::core::RobotModelPtr rob_model(new moveit::core::RobotModel(ur_model,s_model));
  // build sdf
  collision_detection::CollisionRobotDistanceField rob_sdf(rob_model);
  //collision_detection::CollisionRobotHybrid rob_hy(crob_model);
  
  return 0;
  
}
