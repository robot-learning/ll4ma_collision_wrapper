#include <ll4ma_collision_wrapper/libs/fcl/collision_checker.h>
#include <array>
#include <fstream>
#include <iostream>
#include <ros/ros.h>
#include <ros/package.h>
#include <ctime>


#include <octomap/octomap.h>
#include <octomap_msgs/Octomap.h>
#include <octomap/ColorOcTree.h>
#include <octomap_msgs/conversions.h>


#include <interactive_markers/interactive_marker_server.h>

void processFeedback(const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback )
{
  cerr<<feedback->pose.position.x<<endl;
}


using namespace fcl;
using namespace fcl_ccheck;
typedef double S;
template <typename S>

struct CollisionData
{
  CollisionData()
  {
    done = false;
  }

  /// @brief Collision request
  CollisionRequest<S> request;

  /// @brief Collision result
  CollisionResult<S> result;

  /// @brief Whether the collision iteration can stop
  bool done;
};

template <typename S>
struct DistanceData
{
  DistanceData()
  {
    done = false;
  }

  /// @brief Distance request
  DistanceRequest<S> request;

  /// @brief Distance result
  DistanceResult<S> result;

  /// @brief Whether the distance iteration can stop
  bool done;

};



bool defaultDistanceFunction(CollisionObject<S>* o1, CollisionObject<S>* o2, void* cdata_, S& dist)
{
  auto* cdata = static_cast<DistanceData<S>*>(cdata_);
  const DistanceRequest<S>& request = cdata->request;
  DistanceResult<S>& result = cdata->result;

  if(cdata->done) { dist = result.min_distance; return true; }

  distance(o1, o2, request, result);

  dist = result.min_distance;

  if(dist <= 0) return true; // in collision or in touch

  return cdata->done;
}


template <typename S>
bool defaultCollisionFunction(CollisionObject<S>* o1, CollisionObject<S>* o2, void* cdata_)
{

  cerr<<"calling collision"<<endl;
  auto* cdata = static_cast<CollisionData<S>*>(cdata_);
  const auto& request = cdata->request;
  auto& result = cdata->result;

  if(cdata->done) return true;

  collide(o1, o2, request, result);

  if(!request.enable_cost && (result.isCollision()) && (result.numContacts() >= request.num_max_contacts))
    cdata->done = true;

  return cdata->done;
}

template <typename S>
bool collisionFunction(CollisionObject<S>* o1, vector<CollisionObject<S>*> o2)
{
  
  for(int i=0;i<o2.size();++i)
  {
    //auto* cdata = static_cast<DistanceData<S>*>(cdata_);

    //CollisionData<S> cdata;
    CollisionRequest<S> request(1000,true);
    CollisionResult<S> result;
    
    
    collide(o1, o2[i], request, result);
    
    //cerr<<result.isCollision()<<endl;
    if(result.isCollision())
    {
      //cerr<<"collision"<<endl;
      return 1;
    }

  }
  
  return 0;

}

bool distanceFunction(CollisionObject<S>* o1, vector<CollisionObject<S>*> o2, S& dist)
{
  S mdist=1000.0;
  for(int i=0;i<o2.size();++i)
  {
    DistanceRequest<S> request;
    DistanceResult<S> result;
    request.enable_signed_distance=true;
    request.enable_nearest_points=true;
    request.gjk_solver_type=GST_LIBCCD;
    //cerr<<o1->getObjectType()<<" "<<o2[i]->getObjectType()<<endl;
    //cerr<<o1->getNodeType()<<" "<<o2[i]->getNodeType()<<endl;
    
    distance(o1, o2[i], request, result);
    dist=result.min_distance;
    if(dist<0.00)
    {
      return 1;
    }
    if(dist<mdist)
    {
      mdist=dist;
    }
    else
    {
      dist=mdist;
    }
  }
  return 0;
}

template <typename S>
bool defaultCollisionFunction(CollisionObject<S>* o1, vector<CollisionObject<S>*> o2, void* cdata_)
{
  for(int i=0;i<o2.size();++i)
  {
    auto* cdata = static_cast<CollisionData<S>*>(cdata_);
    const auto& request = cdata->request;
    auto& result = cdata->result;
    
    collide(o1, o2[i], request, result);
    if(result.isCollision())
    {
      cerr<<"collision"<<endl;
      return cdata->done;
    }
    //if(!request.enable_cost && (result.isCollision()) && (result.numContacts() >= request.num_max_contacts))
    //  cdata->done = true;

  return cdata->done;
  }
}

int main(int argc, char* argv[])
{
  ros::init(argc, argv, "fcl_demo");
  //ros::Node("fcl_demo");
  
  ros::NodeHandle nh;

  collisionChecker c_check;

  // This should from your data directory:
  string pcd_file=ros::package::getPath("ll4ma_collision_wrapper");
  pcd_file.append("/data/table.pcd");
  ROS_INFO_STREAM("Loading pcd file from location " << pcd_file);

  PointCloud<PointXYZ>::Ptr env_cloud(new PointCloud<PointXYZ>);

  
  //start = std::chrono::high_resolution_clock::now();
  // load pointcloud from file
  c_check._pcl_eigen.loadPCD(pcd_file,env_cloud);

  float resolution = 0.1f;
  
  // convert to normal octree:
  octomap::OcTree* tree = new octomap::OcTree(resolution);
  // load pointcloud to octree:
  c_check._pcl_eigen.loadPCLOctree(env_cloud,tree);


  // create collision objects from octree boxes:
  std::vector<CollisionObject<S>*> boxes=c_check.loadOctreeCollObj(tree);
  
  // visualize octomap:
  visualization_msgs::MarkerArray occupiedNodesVis;
  c_check._pcl_eigen.get_marker_octree(tree,occupiedNodesVis);
  ros::Publisher m_pub=nh.advertise<visualization_msgs::MarkerArray>("collision_env", 0);
  ros::Rate loop_r(500);

  
  // check with primitive shape:
  using CollisionGeometryPtr_t = std::shared_ptr<fcl::CollisionGeometry<S>>;

  CollisionGeometryPtr_t boxGeometry (new fcl::Box<S> (0.4, 0.5, 0.5));
  fcl::Transform3<S> tf2 = fcl::Transform3<S>::Identity();
  
  fcl::CollisionObject<S>* box = new CollisionObject<S>(boxGeometry, tf2);
  ros::Publisher pub=nh.advertise<pcl::PointCloud<PointXYZ>>("/collision_checker/env",1);
  
  

  vector<double> obj_position={0.0,0.0,0.0};
  ros::Publisher vis_pub = nh.advertise<visualization_msgs::Marker>( "visualization_marker", 0 );
  // visualize object mesh:
  visualization_msgs::Marker marker;
  marker.header.frame_id = "map";
  marker.header.stamp = ros::Time();
  marker.ns = "my_namespace";
  marker.id = 0;
  //marker.type = visualization_msgs::Marker::MESH_RESOURCE;
  //marker.mesh_resource = "file:///home/bala/object_models/mustard_bottle/google_16k/nontextured.stl";

  marker.type = visualization_msgs::Marker::CUBE;

  marker.action = visualization_msgs::Marker::ADD;
  marker.pose.position.x = obj_position[0];
  marker.pose.position.y =  obj_position[1];
  marker.pose.position.z =  obj_position[2];
  marker.pose.orientation.x = 0.0;
  marker.pose.orientation.y = 0.0;
  marker.pose.orientation.z = 0.0;
  marker.pose.orientation.w = 1.0;
  marker.scale.x = 0.4;
  marker.scale.y = 0.5;
  marker.scale.z = 0.5;
  marker.color.a = 1.0; // Don't forget to set the alpha!
  marker.color.r = 0.0;
  marker.color.g = 1.0;
  marker.color.b = 0.0;

  //TODO:Broadphase collision checking
  //DynamicAABBTreeCollisionManager<S>* manager = new DynamicAABBTreeCollisionManager<S>();
  //manager->registerObjects(boxes);
  //manager->setup();
  //manager->octree_as_geometry_collide = true;
  //manager->octree_as_geometry_distance = true;

  

  
  while(ros::ok())
  {
    env_cloud->header.frame_id="map";
    pub.publish(env_cloud);
    m_pub.publish(occupiedNodesVis);


    CollisionData<S> cdata;
    DistanceData<S> cdata3;
    cdata3.request.enable_signed_distance=true;
    cdata3.request.enable_nearest_points=true;

    // update marker position
    marker.pose.position.x = obj_position[0];
    marker.pose.position.y =  obj_position[1];
    marker.pose.position.z =  obj_position[2];
    marker.header.stamp = ros::Time();

    Vector3d dir(obj_position[0],obj_position[1],obj_position[2]);


    box->setTranslation(dir);
    
    S d;

    //S c=distanceFunction(box,boxes,d);

    // check collision between box and octomap env:
    DistanceResultd res;
    c_check.signedDistance(box,boxes,res);
    d=res.min_distance;
    if(d>0.0)
    {
      marker.color.r = 0.0;
      marker.color.g = 1.0;
      marker.color.b = 0.0;
    }
    else
    {
      marker.color.r = 1.0;
      marker.color.g = 0.0;
      marker.color.b = 0.0;
    }
    
    vis_pub.publish( marker );

    
    // add offset to position:
    obj_position[0]=0.0;
    //obj_position[1]-=0.001;
    obj_position[2]+=0.001;
    //delete manager;
    loop_r.sleep();
  }

  
  
  // print out distance data:
  return 0;

}
