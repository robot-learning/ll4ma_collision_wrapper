#include <ll4ma_collision_wrapper/libs/geodesic/HeatGeodesic.h>
#include <ll4ma_collision_wrapper/libs/kris_library/collision_checker.h>

// given a map and two regions in the map, find the shortest geodesic distance on the map between the two regions
#include <ros/ros.h>
#include <ros/package.h>
#include<colorized_mesh_display/ColorizedMeshStamped.h>
#include <visualization_msgs/MarkerArray.h>

#include <KrisLibrary/meshing/Meshing.h>
#include <KrisLibrary/meshing/IO.h>
#include <KrisLibrary/math3d/rotation.h>
//#include <KrisLibrary/

using namespace std;
using namespace colorized_mesh_display;

visualization_msgs::MarkerArray get_marker(vector<Math3D::Vector3> pts, Eigen::Vector3d rgb,double scale=0.002)
{
  visualization_msgs::MarkerArray m_arr;
  
  // reset markerarray:
  uint32_t shape = visualization_msgs::Marker::SPHERE;
  // create markers:
  visualization_msgs::Marker marker;
  marker.header.frame_id = "world";
  marker.ns = "basic_shapes";
  marker.id = 0;
  
  // Set the marker type.  Initially this is CUBE, and cycles between that and SPHERE, ARROW, and CYLINDER
  marker.type = shape;
  
  // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
  marker.action = visualization_msgs::Marker::ADD;
  
  // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
  marker.pose.orientation.x = 0.0;
  marker.pose.orientation.y = 0.0;
  marker.pose.orientation.z = 0.0;
  marker.pose.orientation.w = 1.0;
  
  // Set the scale of the marker -- 1x1x1 here means 1m on a side
  marker.scale.x = scale;
  marker.scale.y = scale;
  marker.scale.z = scale;
  
  // Set the color -- be sure to set alpha to something non-zero!
  marker.color.r = rgb[0];
  marker.color.g = rgb[1];
  marker.color.b = rgb[2];
  marker.color.a = 1.0;
    
  marker.lifetime = ros::Duration();
    
  for(int i=0;i<pts.size();++i)
  {

    marker.pose.position.x =pts[i][0];
    marker.pose.position.y = pts[i][1];
    marker.pose.position.z =pts[i][2];
    
    m_arr.markers.emplace_back(marker);
    marker.id+=1;
    
  }
      

  return m_arr;
}
ColorizedMesh get_viz_mesh(Meshing::TriMesh mesh, vector<double> heatmap=vector<double>(),Eigen::Vector3d rgb_max=Eigen::Vector3d(1,0.2,0.0))
{

  // get color
  Eigen::Vector3d rgb_min(0.0,0.0,0.0);
  Eigen::Vector3d rgb_mid(1.0,0.0,0.0);
    
  //Eigen::Vector3d rgb_max(1.0,0.2,0.0);
  //Eigen::Map<Eigen::VectorXd> heat(&heatmap[0],heatmap.size());

  // only visualize vertices, face, color
  ColorizedMesh out;
  for(int i=0;i<mesh.verts.size();++i)
  {
    geometry_msgs::Point32 vertex;
    vertex.x=mesh.verts[i][0];
    vertex.y=mesh.verts[i][1];
    vertex.z=mesh.verts[i][2];
    out.vertices.push_back(vertex);
    // assume no normal info
    geometry_msgs::Vector3 normal;
    normal.x=0;
    normal.y=0;
    normal.z=1;
    out.vertex_normals.push_back(normal);
    // add vertex color:
    std_msgs::ColorRGBA color;
    Eigen::Vector3d rgb;
    if(heatmap.size()>0)
    {
      rgb=rgb_min+(rgb_max-rgb_min)*heatmap[i];
    }
    else
    {
      rgb=rgb_max;

    }
    color.r=rgb[0];
    color.g=rgb[1];
    color.b=rgb[2];
    color.a=1.0;
    out.vertex_colors.push_back(color);
    
    
    
  }
  for(int i=0;i<mesh.tris.size();++i)
  {
    shape_msgs::MeshTriangle triangle;
    triangle.vertex_indices={{mesh.tris[i][0],mesh.tris[i][1],mesh.tris[i][2]}};
    out.triangles.push_back(std::move(triangle));
  }
  return out;
}


// given two trimeshes, return intersecting vertices:
vector<int> intersecting_verts(Meshing::TriMesh map, Meshing::TriMesh region,double tol=1e-6)
{
  
  Geometry::CollisionMesh obj1(map);
  Geometry::CollisionMesh obj2(region);

  Geometry::CollisionMeshQuery coll_obj(obj1,obj2);
  std::cerr<<"Found points within tolerance: "<<coll_obj.WithinDistanceAll(tol)<<std::endl;

  vector<Math3D::Vector3> obj_pts,f_pts;

  // find points within distance threshold
  
  coll_obj.TolerancePoints(obj_pts,f_pts);

  // add collision intersection points with closest triangle
  
  // find vertices to collision points on object
  vector<int> obj_vids;//(obj_pts.size());
  for(int i=0;i<obj_pts.size();++i)
  {
    double min_d=1000;
    int idx=0;
    for(int j=0;j<map.verts.size();++j)
    {
      double dist=(obj_pts[i]-map.verts[j]).norm();
      if(dist<min_d)
      {
        min_d=dist;
        idx=j;
      }
    }
    //if(min_d<1e-3)
    {
      obj_vids.push_back(idx);
    }
  }

  // sort and remove duplicates:
  vector<int> sort_vids;
  int temp;

  for(int i=obj_vids.size()-1;i>=0;--i)
  {
    for(int j=0;j<=i;++j)
    {      
      if(obj_vids[j-1]>obj_vids[j])
      {
        temp=obj_vids[j-1];
        obj_vids[j-1]=obj_vids[j];
        obj_vids[j]=temp;
      }
    }
  }
  sort_vids.emplace_back(obj_vids[0]);
  for(int i=0;i<obj_vids.size();++i)
  {
    if(obj_vids[i]!=sort_vids[sort_vids.size()-1])
    {
      sort_vids.emplace_back(obj_vids[i]);
    }
  }
  return sort_vids;
  
}
double computeGeodesic(Meshing::TriMesh &obj_mesh, vector<int> &contact_vids, vector<int> &des_vids,int &min_fvid, int &min_dvid)
{

  min_fvid=0;
  // compute euclidean distance:
  //int min_fvid=0;
  //int min_dvid=0;
  double min_dist=1000;
  
  
  geodesic::HeatGeodesic mesh(obj_mesh);
  vector<double> shortest_distance_arr(contact_vids.size(),0.0);
  vector<int> dvid_arr(contact_vids.size(),0.0);
  for(int i=0;i<contact_vids.size();++i)
  {

    Eigen::VectorXd geo_dist=mesh.computeGeodesics(contact_vids[i]);
    /*
    vector<double> heatmap;
    for(int i=0;i<mesh.vertices.size();++i)
    {
      heatmap.push_back(mesh.vertices[i].phi);
    }
    */
    //o_mesh.mesh=get_viz_mesh(obj_mesh,heatmap);

    // find shortest distance to the desired mesh region
    for(int j=0;j<des_vids.size();++j)
    {
      double dist=geo_dist[des_vids[j]];
      if(dist<min_dist)
      {
        min_dist=dist;
        dvid_arr[i]=j;
      }
    }
    // store in array
    shortest_distance_arr[i]=min_dist;
    min_dist=1000;
  }

  // find the farthest vertex
  min_dist=0.0;
  for(int i=0;i<shortest_distance_arr.size();++i)
  {
    if(shortest_distance_arr[i]>min_dist)
    {
      min_dist=shortest_distance_arr[i];
      min_fvid=i;
      min_dvid=dvid_arr[i];
    }
  }
  return min_dist;
}

double computeEuclidean(Meshing::TriMesh &obj_mesh, vector<int> &contact_vids, vector<int> &des_vids,int &min_fvid, int &min_dvid)
{

  min_fvid=0;
  // compute euclidean distance:
  //int min_fvid=0;
  //int min_dvid=0;
  double min_dist=1000;
  
  
  geodesic::HeatGeodesic mesh(obj_mesh);
  vector<double> shortest_distance_arr(contact_vids.size(),0.0);
  vector<int> dvid_arr(contact_vids.size(),0.0);
  for(int i=0;i<contact_vids.size();++i)
  {

    Eigen::VectorXd geo_dist=mesh.computeEuclidean(contact_vids[i]);
    /*
    vector<double> heatmap;
    for(int i=0;i<mesh.vertices.size();++i)
    {
      heatmap.push_back(mesh.vertices[i].phi);
    }
    */
    //o_mesh.mesh=get_viz_mesh(obj_mesh,heatmap);

    // find shortest distance to the desired mesh region
    for(int j=0;j<des_vids.size();++j)
    {
      double dist=geo_dist[des_vids[j]];
      if(dist<min_dist)
      {
        min_dist=dist;
        dvid_arr[i]=j;
      }
    }
    // store in array
    shortest_distance_arr[i]=min_dist;
    min_dist=1000;
  }

  // find the farthest vertex
  min_dist=0.0;
  for(int i=0;i<shortest_distance_arr.size();++i)
  {
    if(shortest_distance_arr[i]>min_dist)
    {
      min_dist=shortest_distance_arr[i];
      min_fvid=i;
      min_dvid=dvid_arr[i];
    }
  }
  return min_dist;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "geodesic_test_node");
  ros::NodeHandle nh;

  // load as trimeshes
  std::string file_name = "/data/block_u_mesh_s.obj";
  std::string finger_name = "/data/finger.stl";
  std::string des_name = "/data/block_u_des.stl";

  std::string path = ros::package::getPath("ll4ma_collision_wrapper");
  path.append(file_name);

  // read trimesh:
  Meshing::TriMesh obj_1;
  Meshing::LoadAssimp(path.c_str(),obj_1);

  path = ros::package::getPath("ll4ma_collision_wrapper");
  

  path.append(finger_name);

  // read trimesh:
  Meshing::TriMesh finger_;
  Meshing::LoadAssimp(path.c_str(),finger_);

  Math3D::RigidTransform T;
  Math3D::Vector3 trans(0.04,0.022,0.035);
  Math3D::Vector3 rpy(0.0,1.6,2.2);
  T.setIdentity();
  
  Math3D::SetMatrixRotationZYX(T.R,rpy);
  //finger_.GetTransform(T);
  T.setTranslation(trans);
  finger_.Transform(T);


  path = ros::package::getPath("ll4ma_collision_wrapper");
  

  path.append(des_name);
  Meshing::TriMesh des_reg;
  Meshing::LoadAssimp(path.c_str(),des_reg);

  // transform finger

  // transform mesh

  // visualize meshes
  ros::Publisher o_pub = nh.advertise<colorized_mesh_display::ColorizedMeshStamped>("obj_mesh", 1, true);

  ros::Publisher f_pub = nh.advertise<colorized_mesh_display::ColorizedMeshStamped>("finger_mesh", 1, true);
  ros::Publisher d_pub = nh.advertise<colorized_mesh_display::ColorizedMeshStamped>("des_mesh", 1, true);

  ros::Publisher o_mpub = nh.advertise<visualization_msgs::MarkerArray>("obj_pt", 1);
  ros::Publisher d_mpub = nh.advertise<visualization_msgs::MarkerArray>("des_pt", 1);
  ros::Publisher f_mpub = nh.advertise<visualization_msgs::MarkerArray>("f_pt", 1);

  
  colorized_mesh_display::ColorizedMeshStamped o_mesh,f_mesh,d_mesh;
  o_mesh.mesh=get_viz_mesh(obj_1,vector<double>(),Eigen::Vector3d(0.4,0.4,0.4));
  f_mesh.mesh=get_viz_mesh(finger_,vector<double>(),Eigen::Vector3d(0.0,0.5,0.0));
  d_mesh.mesh=get_viz_mesh(des_reg,vector<double>(),Eigen::Vector3d(0.0,0.0,1.0));

  o_mesh.header.frame_id="world";
  f_mesh.header.frame_id="world";
  d_mesh.header.frame_id="world";

  vector<int> finger_intersecting_vids=intersecting_verts(obj_1,finger_,1e-4);
  
  vector<int> desired_intersecting_vids=intersecting_verts(obj_1,des_reg,1e-3);

  
  
  geodesic::HeatGeodesic mesh(obj_1);

  int min_fvid=0;
  
  int min_dvid=0;
  double min_dist=computeGeodesic(obj_1,finger_intersecting_vids,desired_intersecting_vids,min_fvid,min_dvid);
  //double min_dist=computeEuclidean(obj_1,finger_intersecting_vids,desired_intersecting_vids,min_fvid,min_dvid);

  min_fvid=10;
  
  /*
  for(int i=0;i<finger_intersecting_vids.size();++i)
  {
    
    Eigen::VectorXd geo_dist=mesh.computeGeodesics(finger_intersecting_vids[i]);
    //Eigen::VectorXd geo_dist=mesh.computeEuclidean(finger_intersecting_vids[i]);
    
    for(int j=0;j<desired_intersecting_vids.size();++j)
    {
      double dist=geo_dist[desired_intersecting_vids[j]];
      //cerr<<dist<<endl;
      if(dist>min_dist)
      {
        min_dist=dist;
        min_fvid=i;
        min_dvid=desired_intersecting_vids[j];
      }
    }
    //cerr<<min_dist<<endl;
  }
  */
  cerr<<min_dist<<endl;
  // get best heatmap:
  Eigen::VectorXd geo_dist=mesh.computeGeodesics(finger_intersecting_vids[min_fvid]);
  //Eigen::VectorXd geo_dist=mesh.computeEuclidean(finger_intersecting_vids[min_fvid]);

    
  // get heatmap:
  vector<double> heatmap;
  for(int i=0;i<mesh.vertices.size();++i)
  {
    heatmap.push_back(mesh.vertices[i].phi);
  }
    
    
  vector<Math3D::Vector3> f_heat_source;
  f_heat_source.emplace_back(obj_1.verts[finger_intersecting_vids[min_fvid]]);
  visualization_msgs::MarkerArray f_heat=get_marker(f_heat_source,Eigen::Vector3d(1,1,0),0.008);

  f_heat_source[0]=obj_1.verts[desired_intersecting_vids[min_dvid]];
  // visualize points
  visualization_msgs::MarkerArray f_min=get_marker(f_heat_source,Eigen::Vector3d(0,1,1),0.008);

  f_min.markers[0].id=1;
  //f_heat.markers.emplace_back(f_min.markers[0]);
  vector<Math3D::Vector3> obj_pts;
  for(int i=0;i<finger_intersecting_vids.size();++i)
  {
    obj_pts.emplace_back(obj_1.verts[finger_intersecting_vids[i]]);
  }

  vector<Math3D::Vector3> des_pts;
  //des_pts.emplace_back(obj_1.verts[min_dvid]);
  
  for(int i=0;i<desired_intersecting_vids.size();++i)
  {
    //cerr<<desired_intersecting_vids[i]<<" "<<i<<endl;
    //cerr<<desired_intersecting_vids[i]<<" "<<obj_1.verts[desired_intersecting_vids[i]][1]<<endl;
    des_pts.emplace_back(obj_1.verts[desired_intersecting_vids[i]]);
  }
  //des_pts.emplace_back(obj_1.verts[desired_intersecting_vids[0]]);

  visualization_msgs::MarkerArray obj_m=get_marker(obj_pts,Eigen::Vector3d(0,1,0));
  //cerr<<des_pts.size()<<endl;
  visualization_msgs::MarkerArray d_m=get_marker(des_pts,Eigen::Vector3d(0,0,1));
  o_mesh.mesh=get_viz_mesh(obj_1,heatmap);

  
  // visualize created mesh
  ros::Rate rate(1);
  int i=0;
  while(ros::ok())
  {
    o_mesh.header.stamp = ros::Time::now();
    f_mesh.header.stamp = ros::Time::now();
    d_mesh.header.stamp = ros::Time::now();
    //f_m.header.stamp = ros::Time::now();
    //obj_m.header.stamp = ros::Time::now();
    if(i<1)
    {
    o_pub.publish(o_mesh);
    }
    i++;
    f_pub.publish(f_mesh);
    d_pub.publish(d_mesh);

    f_mpub.publish(f_heat);
    o_mpub.publish(obj_m);
    d_mpub.publish(d_m);
    rate.sleep();
  }
  

}
// euc: 0.0620001
// geo: 0.175185s
