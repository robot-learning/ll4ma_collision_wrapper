#include <ll4ma_collision_wrapper/libs/geodesic/HeatGeodesic.h>
#include <ros/ros.h>

#include <ros/package.h>
#include<KrisLibrary/meshing/IO.h>
#include<colorized_mesh_display/ColorizedMeshStamped.h>
#include <visualization_msgs/Marker.h>

using namespace std;
using namespace colorized_mesh_display;
ColorizedMesh get_viz_mesh(Meshing::TriMesh mesh, vector<double> heatmap)
{

  // get color
  Eigen::Vector3d rgb_min(0.0,0.0,0.0);
  Eigen::Vector3d rgb_mid(1.0,0.0,0.0);
    
  Eigen::Vector3d rgb_max(1.0,0.2,0.0);
  //Eigen::Map<Eigen::VectorXd> heat(&heatmap[0],heatmap.size());

  // only visualize vertices, face, color
  ColorizedMesh out;
  for(int i=0;i<mesh.verts.size();++i)
  {
    geometry_msgs::Point32 vertex;
    vertex.x=mesh.verts[i][0];
    vertex.y=mesh.verts[i][1];
    vertex.z=mesh.verts[i][2];
    out.vertices.push_back(vertex);
    // assume no normal info
    geometry_msgs::Vector3 normal;
    normal.x=0;
    normal.y=0;
    normal.z=1;
    out.vertex_normals.push_back(normal);
    // add vertex color:
    std_msgs::ColorRGBA color;
    Eigen::Vector3d rgb=rgb_min+(rgb_max-rgb_min)*heatmap[i];
    color.r=rgb[0];
    color.g=rgb[1];
    color.b=rgb[2];
    color.a=1.0;
    out.vertex_colors.push_back(color);
    
    
    
  }
  for(int i=0;i<mesh.tris.size();++i)
  {
    shape_msgs::MeshTriangle triangle;
    triangle.vertex_indices={{mesh.tris[i][0],mesh.tris[i][1],mesh.tris[i][2]}};
    out.triangles.push_back(std::move(triangle));
  }
  return out;
}

int main(int argc,char** argv)
{
  ros::init(argc, argv, "geodesic_test_node");
  ros::NodeHandle nh;

  // load
  std::string file_name = "/data/block_u_mesh_s.obj";
  std::string path = ros::package::getPath("ll4ma_collision_wrapper");
  path.append(file_name);

  // read trimesh:
  Meshing::TriMesh obj_1;
  Meshing::LoadAssimp(path.c_str(),obj_1);
  geodesic::HeatGeodesic mesh(obj_1);
  //cerr<<mesh.vertices.size()<<endl;
  //cerr<<mesh.halfEdges.size()<<endl;
  int v_idx=15;
  Eigen::Vector3d pt(obj_1.verts[v_idx][0],obj_1.verts[v_idx][1],obj_1.verts[v_idx][2]);
  //mesh.read(path);
  mesh.computeGeodesics(v_idx);
  //mesh.computeEuclidean(v_idx);
  
  // get heatmap:
  vector<double> heatmap;
  for(int i=0;i<mesh.vertices.size();++i)
  {
    heatmap.push_back(mesh.vertices[i].phi);
  }

  // visualize mesh in rviz
  
  colorized_mesh_display::ColorizedMeshStamped colorized_mesh;
  colorized_mesh.mesh=get_viz_mesh(obj_1,heatmap);

  colorized_mesh.header.frame_id = "world";
  colorized_mesh.header.stamp = ros::Time::now();
  ros::Publisher pub = nh.advertise<colorized_mesh_display::ColorizedMeshStamped>("geodesic_mesh", 1, true);
  ros::Publisher p1_pub = nh.advertise<visualization_msgs::Marker>("heat_point", 1);
  uint32_t shape = visualization_msgs::Marker::SPHERE;

  
  
  // markers:
  visualization_msgs::Marker marker;
  marker.header.frame_id = "world";
  marker.ns = "basic_shapes";
  marker.id = 0;
  
  // Set the marker type.  Initially this is CUBE, and cycles between that and SPHERE, ARROW, and CYLINDER
  marker.type = shape;
  
  // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
  marker.action = visualization_msgs::Marker::ADD;
  
  // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
  marker.pose.position.x = pt[0];
  marker.pose.position.y = pt[1];
  marker.pose.position.z = pt[2];
  marker.pose.orientation.x = 0.0;
  marker.pose.orientation.y = 0.0;
  marker.pose.orientation.z = 0.0;
  marker.pose.orientation.w = 1.0;
  
  // Set the scale of the marker -- 1x1x1 here means 1m on a side
  marker.scale.x = 1.5;
  marker.scale.y = 1.5;
  marker.scale.z = 1.5;

    // Set the color -- be sure to set alpha to something non-zero!
  marker.color.r = 0.0f;
  marker.color.g = 1.0f;
  marker.color.b = 0.0f;
  marker.color.a = 1.0;

  marker.lifetime = ros::Duration();


  ros::Rate rate(1);
  while(ros::ok())
  {
    colorized_mesh.header.stamp = ros::Time::now();
    p1_pub.publish(marker);
    
    pub.publish(colorized_mesh);
    rate.sleep();
  }
  return 0;
}
