#include <ll4ma_collision_wrapper/scene/kris_library/manipulation.h>
namespace kris_ccheck
{
using namespace Meshing;
using namespace Geometry;
// constructor:
graspEnv::graspEnv()
{
}

void graspEnv::load_robot_links(vector<string> &arm_link_names,vector<string> &hand_link_names,string &urdf_txt)
{
  n_links_=arm_link_names.size();
  n_hand_links_=hand_link_names.size();
  readLinkShapes(urdf_txt,arm_link_names,arm_link_objs_,arm_link_offsets_);
  readLinkShapes(urdf_txt,hand_link_names,hand_link_objs_,hand_link_offsets_);

}

void graspEnv::update_arm_state(vector<vector<double>> &link_poses)
{
  //vector<Transform3d> l_poses;
  //getTransform3d(link_poses,l_poses);
  //addOffsets(arm_link_offsets_,link_poses,l_poses);
  updatePose(arm_link_objs_,link_poses);
}

void graspEnv::update_hand_state(vector<vector<double>> &link_poses)
{
  //vector<Transform3d> l_poses;
  //getTransform3d(link_poses,l_poses);

  //addOffsets(hand_link_offsets_,link_poses,l_poses);
  updatePose(hand_link_objs_,link_poses);
}
void graspEnv::update_robot_state(vector<vector<double>> &arm_link_poses,vector<vector<double>> &hand_link_poses)
{
  update_arm_state(arm_link_poses);
  update_hand_state(hand_link_poses);
}

void graspEnv::update_robot_state(vector<vector<double>> &arm_hand_link_poses)
{
  vector<vector<double>> arm_poses;
  arm_poses.assign(arm_hand_link_poses.begin(),arm_hand_link_poses.begin()+n_links_);

  vector<vector<double>> hand_poses;
  hand_poses.assign(arm_hand_link_poses.begin()+n_links_,arm_hand_link_poses.begin()+n_links_+n_hand_links_);

  update_arm_state(arm_poses);
  update_hand_state(hand_poses);
}

void graspEnv::add_env_meshes(vector<string> &f_names, vector<Eigen::Matrix4d> &T)
{
  env_objs_.resize(f_names.size()); // TODO: allow for appending new environment meshes.
  for(int i=0; i<f_names.size(); ++i)
  {
    env_objs_[i]=loadMeshShape(f_names[i],T[i]);
  }
}


void graspEnv::update_env_poses(vector<vector<double>> &poses, vector<int> &idx)
{
  for(int i=0;i<idx.size();++i)
  {
    updatePose(env_objs_[idx[i]],poses[i]);
  }
}

void graspEnv::add_obj_mesh(const string &f_name, Eigen::Matrix4d &T)
{
  grasp_obj_=loadMeshShape(f_name,T);
  // also save the trimesh model for surface normal queries:
  //Trimesh* o_mesh=new Trimesh();
  obj_mesh_= new TriMesh;
  loadModel(f_name,obj_mesh_);
  Math3D::RigidTransform T_mat(Math3D::Matrix4(Math3D::Vector4(T.col(0)[0],T.col(0)[1],T.col(0)[2],T.col(0)[3]),
                                               Math3D::Vector4(T.col(1)[0],T.col(1)[1],T.col(1)[2],T.col(1)[3]),
                                               Math3D::Vector4(T.col(2)[0],T.col(2)[1],T.col(2)[2],T.col(2)[3]),
                                               Math3D::Vector4(T.col(3)[0],T.col(3)[1],T.col(3)[2],T.col(3)[3])));

  obj_mesh_->Transform(T_mat);
  
  
}

void graspEnv::update_obj_pose(vector<double> &pose)
{
  updatePose(grasp_obj_,pose);
}

void graspEnv::add_reach_mesh(vector<string> &f_names,Eigen::Matrix4d &T)
{
  reach_objs_.resize(f_names.size()); 
  for(int i=0; i<f_names.size(); ++i)
  {
   reach_objs_[i]=loadMeshShape(f_names[i],T);
  }
}

void graspEnv::update_reach_pose(vector<double> &pose)
{
  for(int i=0;i<reach_objs_.size();++i)
  {
    updatePose(reach_objs_[i],pose);
  }
}

void graspEnv::reach_obj_ccheck(vector<coll_data> &c_data)
{
  c_data.resize(reach_objs_.size());
  for(int i=0;i<reach_objs_.size();++i)
  {
    signedDistance(reach_objs_[i],grasp_obj_,c_data[i],false);
  }
}
void graspEnv::reach_obj_ccheck(coll_data &c_data,int idx)
{
  signedDistance(reach_objs_[idx],grasp_obj_,c_data,false);
}

void graspEnv::rob_env_ccheck(vector<coll_data> &c_data)
{
  //cerr<<n_links_+n_hand_links_<<endl;
  // assuming robot state is already updated:
  c_data.resize(n_links_+n_hand_links_);
  for (int i=0;i<n_links_;++i)
  {
    signedDistance(arm_link_objs_[i],env_objs_,c_data[i]);
  }
  for (int i=0;i<n_hand_links_;++i)
  {
    signedDistance(hand_link_objs_[i],env_objs_,c_data[i+n_links_]);
  } 
}
void graspEnv::arm_env_ccheck(vector<vector<double>> &l_poses, vector<coll_data> &c_data)
{
  // update robot link poses:
  //update_arm_state(l_poses);
  
  c_data.resize(n_links_);

  // TODO: setup multi threading.
  for (int i=0;i<n_links_;++i)
  {
    signedDistance(arm_link_objs_[i],env_objs_,c_data[i]);
  }
}

void graspEnv::hand_env_ccheck(vector<vector<double>> &l_poses, vector<coll_data> &c_data)
{
  // update robot link poses:
  //update_hand_state(l_poses);
  
  c_data.resize(n_hand_links_);

  for (int i=0;i<n_hand_links_;++i)
  {
    signedDistance(hand_link_objs_[i],env_objs_,c_data[i]);
  }
}

void graspEnv::arm_object_ccheck(vector<vector<double>> &l_poses, vector<coll_data> &c_data)
{
  c_data.resize(n_links_);

  // TODO: setup multi threading.
  for (int i=0;i<n_links_;++i)
  {
    signedDistance(arm_link_objs_[i],grasp_obj_,c_data[i]);
  }
  
}
void graspEnv::hand_object_ccheck(vector<vector<double>> &l_poses, vector<coll_data> &c_data)
{
  c_data.resize(n_hand_links_);
  for (int i=0;i<n_hand_links_;++i)
  {
    signedDistance(hand_link_objs_[i],grasp_obj_,c_data[i]);
  }
}

void graspEnv::arm_object_ccheck(vector<vector<double>> &l_poses, vector<coll_data> &c_data, vector<int> l_idx)
{
  c_data.resize(l_idx.size());
  for (int i=0;i<l_idx.size();++i)
  {
    signedDistance(arm_link_objs_[l_idx[i]],grasp_obj_,c_data[i]);
  }
  
}

void graspEnv::rob_object_ccheck(vector<coll_data> &c_data, vector<int> l_idx,bool collision)
{
  c_data.resize(l_idx.size());
  for (int i=0;i<l_idx.size();++i)
  {
    if(l_idx[i]<n_links_)
    {
      signedDistance(arm_link_objs_[l_idx[i]],grasp_obj_,c_data[i],collision);
    }
    else
    {
      signedDistance(hand_link_objs_[l_idx[i]-n_links_],grasp_obj_,c_data[i],collision);
    }
  }
   
}
void graspEnv::obj_env_ccheck(coll_data &c_data)
{
  //checkCollision(grasp_obj_,env_objs_,c_data);
  signedDistance(grasp_obj_,env_objs_,c_data,false);
}

void graspEnv::obj_surface_normal(vector<double> &cpt, vector<double> &s_pt, vector<double> &s_normal)
{
  // get index of closest triangle
  assert(cpt.size()==3);
  Math3D::Vector3 pt(cpt[0],cpt[1],cpt[2]);
  Math3D::Vector3 s_pt_;
  int tri_idx=obj_mesh_->ClosestPoint(pt,s_pt_);
  s_pt.resize(3);
  s_pt[0]=s_pt_[0];
  s_pt[1]=s_pt_[1];
  s_pt[2]=s_pt_[2];

  // get surface normal

  Math3D::Vector3 s_n = obj_mesh_->TriangleNormal(tri_idx);
  s_normal.resize(3);
  s_normal[0]=s_n[0];
  s_normal[1]=s_n[1];
  s_normal[2]=s_n[2];
  
}
}
