#include <ll4ma_collision_wrapper/scene/kris_ccd/manipulation.h>

using namespace Meshing;
using namespace Geometry;

namespace kris_ccd_ccheck
{

void manipulationEnv::load_robot_links(vector<string> &arm_link_names, vector<string> &hand_link_names, string &urdf_text)
{
  n_links_=arm_link_names.size();
  n_hand_links_=hand_link_names.size();
  readLinkShapes(urdf_text,arm_link_names,arm_link_objs_,ccd_arm_link_objs_);
  readLinkShapes(urdf_text,hand_link_names,hand_link_objs_,ccd_hand_link_objs_);  
}

void manipulationEnv::load_robot_links(vector<string> &hand_link_names, string &urdf_text)
{
  n_links_=hand_link_names.size();
  n_hand_links_=n_links_;
  readLinkShapes(urdf_text,hand_link_names,hand_link_objs_,ccd_hand_link_objs_);  
}


/*
void manipulationEnv::load_robot_contact_links(vector<string> &robot_contact_link_names, string &urdf_text)
{
  readLinkShapes(urdf_text,robot_contact_link_names,contact_link_objs_,ccd_contact_link_objs_);
}
*/


void manipulationEnv::update_arm_state(vector<vector<double>> &link_poses)
{
  updatePose(arm_link_objs_,link_poses);
  updatePose(ccd_arm_link_objs_,link_poses);
}

void manipulationEnv::update_hand_state(vector<vector<double>> &link_poses)
{
  updatePose(hand_link_objs_,link_poses);
  updatePose(ccd_hand_link_objs_,link_poses);
}
void manipulationEnv::update_robot_state(vector<vector<double>> &arm_hand_link_poses)
{
  vector<vector<double>> arm_poses;
  arm_poses.assign(arm_hand_link_poses.begin(),arm_hand_link_poses.begin()+n_links_);

  vector<vector<double>> hand_poses;
  hand_poses.assign(arm_hand_link_poses.begin()+n_links_,arm_hand_link_poses.begin()+n_links_+n_hand_links_);

  update_arm_state(arm_poses);
  update_hand_state(hand_poses);
}

void manipulationEnv::update_env_meshes(vector<string> &f_names, vector<Eigen::Matrix4d> &T,vector<bool> cvx_idx)
{
  env_objs_.resize(f_names.size()); // TODO: allow for appending new environment meshes.
  ccd_env_objs_.resize(f_names.size());
  for(int i=0; i<f_names.size(); ++i)
  {
    if(cvx_idx.size()==f_names.size())
    {
      if(cvx_idx[i])
      {
        ccd_env_objs_[i].resize(1);
        loadCvxMeshShape(f_names[i],T[i],ccd_env_objs_[i][0]);
      }
      else
      {
        loadMeshShape(f_names[i],T[i],ccd_env_objs_[i]);
      }
    }
    else
    {
      loadMeshShape(f_names[i],T[i],ccd_env_objs_[i]);
    }
      env_objs_[i]=loadMeshShape(f_names[i],T[i]);
      cerr<<"****Environment convex objects: "<<ccd_env_objs_[i].size()<<endl;
  }
}


void manipulationEnv::update_env_poses(vector<vector<double>> &poses, vector<int> &idx)
{
  for(int i=0;i<idx.size();++i)
  {
    updatePose(env_objs_[idx[i]],poses[i]);
    updatePose(ccd_env_objs_[idx[i]],poses[i]);
  }
}

void manipulationEnv::update_obj_mesh(const string &f_name, Eigen::Matrix4d &T)
{
  cerr<<"Adding object mesh"<<endl;
  grasp_obj_=loadMeshShape(f_name,T);
  loadMeshShape(f_name,T,ccd_grasp_obj_);
  cerr<<"Number of cconvex regions in object: "<< ccd_grasp_obj_.size()<<endl;
  
  // also save the trimesh model for surface normal queries:

  obj_mesh_= new TriMesh;
  world_obj_mesh_= new TriMesh;
  loadModel(f_name,obj_mesh_);

  *world_obj_mesh_=*obj_mesh_;
  Math3D::RigidTransform T_mat(Math3D::Matrix4(Math3D::Vector4(T.col(0)[0],T.col(0)[1],T.col(0)[2],T.col(0)[3]),
                                               Math3D::Vector4(T.col(1)[0],T.col(1)[1],T.col(1)[2],T.col(1)[3]),
                                               Math3D::Vector4(T.col(2)[0],T.col(2)[1],T.col(2)[2],T.col(2)[3]),
                                               Math3D::Vector4(T.col(3)[0],T.col(3)[1],T.col(3)[2],T.col(3)[3])));

  world_obj_mesh_->Transform(T_mat);
  
  
}

void manipulationEnv::update_obj_pose(vector<double> &pose)
{
  
  //cerr<<"***UPDATING OBJECT POSE"<<endl;
  updatePose(grasp_obj_,pose);
  updatePose(ccd_grasp_obj_,pose);
  // also update trimesh model pose for surface normal queries
  // copy verts from orginal mesh
  world_obj_mesh_->verts=obj_mesh_->verts;
  world_obj_mesh_->tris=obj_mesh_->tris;
  
  updatePose(world_obj_mesh_,pose);
  Math3D::RigidTransform d;
  Math3D::Matrix4 v;
  grasp_obj_->GetTransform(d);
  d.get(v);
  //cerr<<v<<endl;
}

void manipulationEnv::update_reach_mesh(vector<string> &f_names,Eigen::Matrix4d &T)
{
  reach_objs_.resize(f_names.size());
  ccd_reach_objs_.resize(f_names.size()); 

  for(int i=0; i<f_names.size(); ++i)
  {
   reach_objs_[i]=loadMeshShape(f_names[i],T);
   loadCvxMeshShape(f_names[i],T,ccd_reach_objs_[i]);
  }
}

void manipulationEnv::update_reach_pose(vector<double> &pose)
{
  for(int i=0;i<reach_objs_.size();++i)
  {
    updatePose(reach_objs_[i],pose);
    updatePose(ccd_reach_objs_[i],pose);
  }
}

void manipulationEnv::reach_obj_ccheck(vector<coll_data> &c_data)
{
  c_data.resize(reach_objs_.size());
  for(int i=0;i<reach_objs_.size();++i)
  {
    signedDistance(reach_objs_[i],grasp_obj_,ccd_reach_objs_[i],ccd_grasp_obj_,c_data[i],true);
  }
}
void manipulationEnv::reach_obj_ccheck(coll_data &c_data,int idx)
{
  
  signedDistance(reach_objs_[idx],grasp_obj_,ccd_reach_objs_[idx],ccd_grasp_obj_,c_data,true);
}

/*
void manipulationEnv::rob_obj_contact(coll_data &c_data, const int &idx)
{
  signedDistance(contact_link_objs_[idx],
                 grasp_obj_,ccd_contact_link_objs_[idx],
                 ccd_grasp_obj_,c_data,true);
}
*/
void manipulationEnv::rob_env_ccheck(vector<coll_data> &c_data,const bool &compute_non_colliding)
{
  //cerr<<n_links_+n_hand_links_<<endl;
  // assuming robot state is already updated:
  c_data.resize(n_links_+n_hand_links_);
  for (int i=0;i<n_links_;++i)
  {
    signedDistance(arm_link_objs_[i],env_objs_,ccd_arm_link_objs_[i],ccd_env_objs_,c_data[i],compute_non_colliding);
  }
  for (int i=0;i<n_hand_links_;++i)
  {
    signedDistance(hand_link_objs_[i],env_objs_,ccd_hand_link_objs_[i],ccd_env_objs_,c_data[i+n_links_],compute_non_colliding);
  } 
}

void manipulationEnv::hand_hand_ccheck(vector<coll_data> &c_data,const bool &compute_non_colliding)
{
  c_data.resize(n_hand_links_);
  // start from 1 because 0 is palm link -- ignore that one
  for (int i=1;i<n_hand_links_;++i)
  {
    for (int j=1;j<n_hand_links_;++j){
      // neighbor link -> non-colliding
      // this treats tips and link_0 of next finger as non-colliding
      if (j == i-1 or j == i or j == i+1){
	      continue;
      }

      signedDistance(hand_link_objs_[i],hand_link_objs_[j],ccd_hand_link_objs_[i],ccd_hand_link_objs_[j],c_data[i],compute_non_colliding);

      // if there is collision, no need to check further for that link because 
      // one link can be in collision with only one another link
      if (c_data[i].collision)
      {
	      break;
      }
    }
  }
}


void manipulationEnv::arm_env_ccheck(vector<vector<double>> &l_poses, vector<coll_data> &c_data)
{
  // update robot link poses:
  //update_arm_state(l_poses);
  
  c_data.resize(n_links_);

  // TODO: setup multi threading.
  for (int i=0;i<n_links_;++i)
  {
    signedDistance(arm_link_objs_[i],env_objs_,ccd_arm_link_objs_[i],ccd_env_objs_,c_data[i]);
  }
}

void manipulationEnv::hand_env_ccheck(vector<vector<double>> &l_poses, vector<coll_data> &c_data)
{
  // update robot link poses:
  //update_hand_state(l_poses);
  
  c_data.resize(n_hand_links_);

  for (int i=0;i<n_hand_links_;++i)
  {
    signedDistance(hand_link_objs_[i],env_objs_,ccd_hand_link_objs_[i],ccd_env_objs_,c_data[i+n_links_]);

  }
}

void manipulationEnv::arm_object_ccheck(vector<vector<double>> &l_poses, vector<coll_data> &c_data)
{
  c_data.resize(n_links_);

  // TODO: setup multi threading.
  for (int i=0;i<n_links_;++i)
  {
    signedDistance(arm_link_objs_[i],grasp_obj_,ccd_arm_link_objs_[i],ccd_grasp_obj_,c_data[i]);
  }
  
}
void manipulationEnv::hand_object_ccheck(vector<vector<double>> &l_poses, vector<coll_data> &c_data, bool compute_non_colliding)
{
  c_data.resize(n_hand_links_);
  for (int i=0;i<n_hand_links_;++i)
  {
    signedDistance(hand_link_objs_[i],grasp_obj_,ccd_hand_link_objs_[i],ccd_grasp_obj_,c_data[i],compute_non_colliding);
  }
}
void manipulationEnv::hand_object_ccheck( vector<coll_data> &c_data, bool compute_non_colliding)
{
  c_data.resize(n_hand_links_);
  for (int i=0;i<n_hand_links_;++i)
  {
    signedDistance(hand_link_objs_[i],grasp_obj_,ccd_hand_link_objs_[i],ccd_grasp_obj_,c_data[i],compute_non_colliding);
  }
}

void manipulationEnv::hand_object_ccheck( vector<coll_data> &c_data, const vector<int> &l_idx,bool compute_non_colliding)
{
  c_data.resize(l_idx.size());
  int i=0;
  for (int j=0;j<l_idx.size();++j)
  {
    i = l_idx[j];
    signedDistance(hand_link_objs_[i],grasp_obj_,
                   ccd_hand_link_objs_[i],ccd_grasp_obj_,c_data[j],compute_non_colliding);
  }
}

void manipulationEnv::hand_object_ccheck( coll_data &c_data, const int &l_idx,bool compute_non_colliding)
{
  signedDistance(hand_link_objs_[l_idx],grasp_obj_,
                   ccd_hand_link_objs_[l_idx],ccd_grasp_obj_,c_data,compute_non_colliding);
}


void manipulationEnv::arm_object_ccheck(vector<vector<double>> &l_poses, vector<coll_data> &c_data, vector<int> l_idx)
{
  c_data.resize(l_idx.size());
  for (int i=0;i<l_idx.size();++i)
  {
    signedDistance(arm_link_objs_[l_idx[i]],grasp_obj_,ccd_arm_link_objs_[l_idx[i]],ccd_grasp_obj_,c_data[i]);
  }
  
}

void manipulationEnv::rob_object_ccheck(vector<coll_data> &c_data, vector<int> l_idx,bool compute_non_colliding)
{
  c_data.resize(n_links_+n_hand_links_);

  //c_data.resize(l_idx.size());
  for (int i=0;i<l_idx.size();++i)
  {
    if(l_idx[i]<n_links_)
    {
      signedDistance(arm_link_objs_[l_idx[i]],grasp_obj_,ccd_arm_link_objs_[l_idx[i]],ccd_grasp_obj_,c_data[l_idx[i]],compute_non_colliding);
    }
    else
    {
      signedDistance(hand_link_objs_[l_idx[i]-n_links_], grasp_obj_,ccd_hand_link_objs_[l_idx[i]-n_links_],ccd_grasp_obj_,c_data[l_idx[i]],compute_non_colliding);
    }
  }
   
}

void manipulationEnv::obj_env_ccheck(coll_data &c_data)
{
  signedDistance(grasp_obj_,env_objs_,ccd_grasp_obj_,ccd_env_objs_,c_data);
}

void manipulationEnv::obj_surface_normal(vector<double> &cpt, vector<double> &s_pt, vector<double> &s_normal)
{
  // get index of closest triangle
  Math3D::Vector3 pt(cpt[0],cpt[1],cpt[2]);
  Math3D::Vector3 s_pt_;
  int tri_idx=world_obj_mesh_->ClosestPoint(pt,s_pt_);
  //cerr<<tri_idx<<endl;
  s_pt.resize(3);
  s_pt[0]=s_pt_[0];
  s_pt[1]=s_pt_[1];
  s_pt[2]=s_pt_[2];

  // get surface normal

  Math3D::Vector3 s_n = world_obj_mesh_->TriangleNormal(tri_idx);
  s_normal.resize(3);
  s_normal[0]=s_n[0];
  s_normal[1]=s_n[1];
  s_normal[2]=s_n[2];
  
}

void manipulationEnv::get_obj_pose(Eigen::Vector3d &position,Eigen::Vector3d &rpy)
{
  Math3D::RigidTransform d;
  Math3D::Matrix4 v;
  Math3D::Vector3 pos;
  grasp_obj_->GetTransform(d);
  d.getTranslation(pos);
  Math3D::Matrix3 R;
  d.getRotation(R);
  Math3D::EulerAngleRotation e_rpy;
  e_rpy.setMatrixXYZ(R);
  rpy=Eigen::Vector3d(e_rpy.x,e_rpy.y,e_rpy.z);
  position=Eigen::Vector3d(pos[0],pos[1],pos[2]);
  
}
TriMesh manipulationEnv::get_obj_mesh()
{
  //obj_mesh_->verts=grasp_obj_->verts;
  //obj_mesh_->tris=grasp_obj_->tris;

  return *obj_mesh_;
}


}
