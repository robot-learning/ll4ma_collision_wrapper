#include "ll4ma_collision_wrapper/libs/fcl/collision_checker.h"
namespace fcl_ccheck
{
collisionChecker::collisionChecker()
{
  
}

void collisionChecker::loadModelPLY(const string &file_name, Model* obj)
{
  // read ply file:
  vector<float> vert;
  vector<uint32_t> faces;
  vector<float> norm;
  _pcl_eigen.loadPLY(file_name,vert,norm,faces);

  // transform to fcl vertices:
  vector<Vector3<double>> vertices;

  vertices.resize(vert.size()/3);

  for( int i=0;i<vertices.size();++i)
  {
    vertices[i][0]=vert[3*i];
    vertices[i][1]=vert[3*i+1];
    vertices[i][2]=vert[3*i+2];
  }

  vector<Triangle> triangles;
  triangles.resize(faces.size()/3);
  
  for(int i=0;i<triangles.size();++i)
  {
    Triangle T(faces[3*i],faces[3*i+1],faces[3*i+2]);
    triangles[i]=T;
  }
  obj->beginModel();
  obj->addSubModel(vertices,triangles);
  obj->endModel();
}

CollisionObjectd* collisionChecker::createCollObj(Model* obj, Matrix4d &T)
{
  // create pose instance:
  // pose is [x,y,z, tx,ty,tz]
  Transform3d pose(T);
  
  // fcl requires a shared ptr
  std::shared_ptr<Model> model(obj);

  CollisionObjectd* c_1= new CollisionObjectd(model,pose);
  return c_1;
}

void collisionChecker::checkCollision(CollisionObjectd* obj_1, CollisionObjectd* obj_2,CollisionResultd &result)
{
  CollisionRequest<double> request;
  // result will be returned via the collision result structure
  // perform collision test
  collide(obj_1, obj_2, request, result);
  
}

void collisionChecker::checkCollision(CollisionObjectd* obj_1, CollisionObjectd* obj_2,coll_data &result)
{
  CollisionResultd res;
  checkCollision(obj_1,obj_2,res);
  result.collision=res.isCollision();
}


void collisionChecker::checkCollision(CollisionObjectd* obj_1, vector<CollisionObjectd*> obj_2, CollisionResultd &result)
{
  // return the data at first instance of a collisiond
  for(int i=0;i<obj_2.size();++i)
  {
    //auto* cdata = static_cast<DistanceData<S>*>(cdata_);

    //CollisionData<S> cdata;
    CollisionRequest<double> request(1000,true);
    CollisionResult<double> res;
    
    
    collide(obj_1, obj_2[i], request, res);
    
    //cerr<<result.isCollision()<<endl;
    if(result.isCollision())
    {
      result=res;
      break;
    }
    result=res;
  }
   
}

void collisionChecker::checkCollision(CollisionObjectd* obj_1,vector<CollisionObjectd*> env_arr, coll_data &result)
{
  CollisionResultd res;
  checkCollision(obj_1,env_arr,res);
  result.collision=res.isCollision();
}

void collisionChecker::signedDistance(CollisionObjectd* obj_1, CollisionObjectd* obj_2, DistanceResultd &result)
{
  DistanceRequest<double> request;
  request.enable_signed_distance=true;
  request.enable_nearest_points=true;
  // If you are using meshes for your objects, then gjk_solver_type=GST_INDEP
  request.gjk_solver_type=GST_LIBCCD;// GST_LIBCCD
  distance(obj_1, obj_2, request, result);

  // The below operation is slow!!
  // Nearest points are in the object frames
  // transforming nearest points to world frame
  //result.nearest_points[0]=obj_1->getTransform()*result.nearest_points[0];
  //result.nearest_points[1]=obj_2->getTransform()*result.nearest_points[1];

}

void collisionChecker::signedDistance(CollisionObjectd* obj_1, CollisionObjectd* obj_2, coll_data &result)
{
  DistanceResultd res;
  signedDistance(obj_1,obj_2,res);
  
  result.distance=res.min_distance;
  if(result.distance<0.0)
  {
    result.collision=1;
  }
  else
  {
    result.collision=0;
  }
  result.p1=res.nearest_points[0];
  result.p2=res.nearest_points[1];
}

void collisionChecker::signedDistance(CollisionObjectd* obj_1, vector<CollisionObjectd*> obj_2, DistanceResultd &result)
{
  double mdist=1000.0;
  int idx=0;
  for(int i=0;i<obj_2.size();++i)
  {
    DistanceResult<double> res;
    signedDistance(obj_1,obj_2[i],res);
    if(res.min_distance<mdist)
    {
      result=res;
      mdist=res.min_distance;
      idx=i;
    }
  }
  result.nearest_points[0]=obj_1->getTransform()*result.nearest_points[0];
  result.nearest_points[1]=obj_2[idx]->getTransform()*result.nearest_points[1];

}
void collisionChecker::signedDistance(CollisionObjectd* obj_1, vector<CollisionObjectd*> obj_2, vector<DistanceResultd> &result)
{
  double mdist=1000.0;
  result.resize(obj_2.size());
  for(int i=0;i<obj_2.size();++i)
  {
    signedDistance(obj_1, obj_2[i],result[i]);
  }
}

void collisionChecker::signedDistance(CollisionObjectd* obj_1,vector<CollisionObjectd*> env_arr, coll_data &result)
{
  DistanceResultd res;
  signedDistance(obj_1,env_arr,res);
  
  result.distance=res.min_distance;
  if(result.distance<0.0)
  {
    result.collision=1;
  }
  else
  {
    result.collision=0;
  }
  result.p1=res.nearest_points[0];
  result.p2=res.nearest_points[1];
}
void collisionChecker::signedDistance(CollisionObjectd* obj_1,vector<CollisionObjectd*> env_arr, vector<coll_data> &result)
{
  vector<DistanceResultd> res;
  signedDistance(obj_1,env_arr,res);

  result.resize(res.size());
  for(int i=0;i<res.size();++i)
  {
    result[i].distance=res[i].min_distance;
    if(result[i].distance<0.0)
    {
      result[i].collision=1;
    }
    else
    {
      result[i].collision=0;
    }
    result[i].p1=res[i].nearest_points[0];
    result[i].p2=res[i].nearest_points[1];
  }
}
void collisionChecker::generateBoxesFromOctomap(std::vector<CollisionObject<double>*>& boxes, OcTree<double> tree)
{
  typedef double S;

  std::vector<std::array<S, 6> > boxes_ = tree.toBoxes();

  for(std::size_t i = 0; i < boxes_.size(); ++i)
  {
    S x = boxes_[i][0];
    S y = boxes_[i][1];
    S z = boxes_[i][2];
    S size = boxes_[i][3];
    S cost = boxes_[i][4];
    S threshold = boxes_[i][5];

    Box<S>* box = new Box<S>(size, size, size);
    //cerr<<cost<<" "<<threshold<<endl;
    box->cost_density = cost;
    box->threshold_occupied = threshold;//threshold;
    CollisionObject<S>* obj = new CollisionObject<S>(std::shared_ptr<CollisionGeometry<S>>(box), Transform3<S>(Translation3<S>(Vector3<S>(x, y, z))));
    //cerr<<obj->isFree()<<" "<<obj->isOccupied()<<endl;
    boxes.push_back(obj);
  }


}

CollisionObjectd* collisionChecker::loadBoxShape(double lx, double ly, double lz, Matrix4d &T)
{
  std::shared_ptr<fcl::CollisionGeometry<double>> boxGeometry (new fcl::Box<double> (lx,ly,lz));
  Transform3d pose(T);
  fcl::CollisionObject<double>* box = new CollisionObject<double>(boxGeometry, pose);
  return box;
}

CollisionObjectd* collisionChecker::loadCylinderShape(double radius, double lz,Matrix4d &T)
{
  std::shared_ptr<fcl::CollisionGeometry<double>> cylGeometry (new fcl::Cylinder<double> (radius,lz));
  Transform3d pose(T);
  fcl::CollisionObject<double>* cyl = new CollisionObject<double>(cylGeometry, pose);
  return cyl;
}

CollisionObjectd* collisionChecker::loadSphereShape(double radius, Matrix4d &T)
{
  // we create a collision geometry(parent) from a shapebase(derived), object is sliced!
  std::shared_ptr<fcl::CollisionGeometry<double>> sphGeometry (new fcl::Sphere<double> (radius));
  Transform3d pose(T);
  fcl::CollisionObject<double>* sph = new CollisionObject<double>(sphGeometry, pose);
  return sph;
}

vector<CollisionObjectd*> collisionChecker::loadOctreeCollObj(octomap::OcTree* tree)
{
  //octomap::OcTree temp_tree=*tree;
  typedef double S;
  OcTree<S>* oc_tree = new OcTree<S>(std::shared_ptr<const octomap::OcTree>(tree));
  Eigen::Matrix4d T;
  T.setIdentity();
  
  std::vector<CollisionObject<S>*> boxes;

  generateBoxesFromOctomap(boxes, *oc_tree);
  return boxes;
}

void collisionChecker::readLinkShapes(string &urdf_txt,vector<string> &link_names, vector<CollisionObjectd*> &c_objs, vector<Transform3d> &link_offsets)
{
  urdf::Model model;
  if (!model.initString(urdf_txt))
  {
    ROS_ERROR("Failed to parse urdf file");
  }
  c_objs.resize(link_names.size());
  link_offsets.resize(link_names.size());
  
  // TODO assert when link name is non existent in the urdf 
  for(int i=0; i<link_names.size();++i)
  {
    // get geometry type: 0=sphere, 1=box, 2=cylinder, 3=mesh
    // std::shared_ptr<const urdf::Link>
    auto link = model.getLink(link_names[i]);

    // store link collision offset:
    urdf::Pose p=link->collision->origin;

    link_offsets[i].setIdentity();
    Eigen::Quaterniond q(p.rotation.w,p.rotation.x,p.rotation.y,p.rotation.z);
    link_offsets[i].rotate(q);

    Eigen::Vector3d pos(p.position.x,p.position.y,p.position.z);
    link_offsets[i].translate(pos);
    // build tranform3d from pose:


    
    int type=link->collision->geometry->type;
    Matrix4d T;
    T.setIdentity();

    if(type==0)
    {
      std::shared_ptr<urdf::Sphere> sph = std::dynamic_pointer_cast<urdf::Sphere>(link->collision->geometry);
      double r=sph->radius;
      c_objs[i]=loadSphereShape(r,T);
    }

    else if(type==1)
    {
      std::shared_ptr<urdf::Box> box = std::dynamic_pointer_cast<urdf::Box>(link->collision->geometry);
      double x=box->dim.x;
      double y=box->dim.y;
      double z=box->dim.z;

      c_objs[i]=loadBoxShape(x,y,z,T);

    }
    else if(type==2)
    {
      std::shared_ptr<urdf::Cylinder> cylinder = std::dynamic_pointer_cast<urdf::Cylinder>(link->collision->geometry);
      double r=cylinder->radius;
      double lz=cylinder->length;
      c_objs[i]=loadCylinderShape(r,lz,T);
    }
    else if(type==3)
    {
      cerr<<"Link : "<<link_names[i]<<" has a mesh, only primitive shapes are supported!!Exiting!!"<<endl;
      assert(type<3);
    }
    //cerr<<type<<endl;
  }
  //cerr<<c_objs[0]->getTranslation()<<endl;
}

void collisionChecker::update_pose(CollisionObjectd* obj, vector<double> &pose)
{
  Quaterniond q(pose[6],pose[3],pose[4],pose[5]);// fcl quaternion(w,x,y,z)
  Vector3d p(pose[0],pose[1],pose[2]);
  obj->setTransform(q,p);
}
void collisionChecker::update_pose(CollisionObjectd* obj, Transform3d &pose)
{
  obj->setTransform(pose);
}

void collisionChecker::update_pose(vector<CollisionObjectd*> &obj, vector<double> &pose)
{
  Quaterniond q(pose[6],pose[3],pose[4],pose[5]);// fcl quaternion(w,x,y,z)
  Vector3d p(pose[0],pose[1],pose[2]);
  for(int i=0;i<obj.size();++i)
  {
    obj[i]->setTransform(q,p);
  }
}
void collisionChecker::update_pose(vector<CollisionObjectd*> &obj, vector<vector<double>> &pose)
{
  // TODO: optimize initialization
  assert(obj.size()==pose.size());
  for(int i=0;i<obj.size();++i)
  {
    Quaterniond q(pose[i][6],pose[i][3],pose[i][4],pose[i][5]);// fcl quaternion(w,x,y,z)
    Vector3d p(pose[i][0],pose[i][1],pose[i][2]);
    obj[i]->setTransform(q,p);
  }
}

void collisionChecker::update_pose(vector<CollisionObjectd*> &obj, vector<Transform3d> &pose)
{
  // TODO: optimize initialization
  assert(obj.size()==pose.size());
  for(int i=0;i<obj.size();++i)
  {
    obj[i]->setTransform(pose[i]);
  }
}

void collisionChecker::store_offsets(vector<CollisionObjectd*> &obj, vector<Transform3d> &offsets)
{
  offsets.resize(obj.size());
  for(int i=0;i<obj.size();++i)
  {
    offsets[i]=obj[i]->getTransform();
  }
}
  
void collisionChecker::add_offsets(vector<Transform3d> &offsets, vector<vector<double>> &poses, vector<Transform3d> &out_poses)
{
  
  out_poses.resize(poses.size());
  // TODO: this is making the computations slower, find a better way!!!
  //Quaterniond q;
  //Vector3d p;
  for(int i=0;i<out_poses.size();++i)
  {
    Quaterniond q(poses[i][6],poses[i][3],poses[i][4],poses[i][5]);// fcl quaternion(w,x,y,z)
    Vector3d p(poses[i][0],poses[i][1],poses[i][2]);
    out_poses[i].setIdentity();
    out_poses[i].translate(p);
    out_poses[i].rotate(q);

    out_poses[i]=out_poses[i]*offsets[i];
  }
  
}
void collisionChecker::add_offsets(vector<Transform3d> &offsets, vector<double> &pose, vector<Transform3d> &out_poses)
{
  
  out_poses.resize(offsets.size());
  // TODO: this is making the computations slower, find a better way!!!
  //Quaterniond q;
  //Vector3d p;
  Quaterniond q(pose[6],pose[3],pose[4],pose[5]);// fcl quaternion(w,x,y,z)
  Vector3d p(pose[0],pose[1],pose[2]);

  for(int i=0;i<out_poses.size();++i)
  {
    out_poses[i].setIdentity();
    out_poses[i].translate(p);
    out_poses[i].rotate(q);

    out_poses[i]=out_poses[i]*offsets[i];
  }
  
}

void collisionChecker::add_offsets(Transform3d &offset, vector<double> &pose, Transform3d &out_pose)
{
  Quaterniond q(pose[6],pose[3],pose[4],pose[5]);// fcl quaternion(w,x,y,z)
  Vector3d p(pose[0],pose[1],pose[2]);
  out_pose.setIdentity();
  out_pose.translate(p);
  out_pose.rotate(q);

  out_pose=out_pose*offset;

}

}
