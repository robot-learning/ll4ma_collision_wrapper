#include <ll4ma_collision_wrapper/libs/geodesic/Vertex.h>
#include <ll4ma_collision_wrapper/libs/geodesic/HalfEdge.h>
#include <ll4ma_collision_wrapper/libs/geodesic/Face.h>

std::vector<HalfEdge> isolated;

bool Vertex::isIsolated() const
{
    return he == isolated.begin();
}

double Vertex::dualArea() const
{    
    double area = 0.0;
    
    HalfEdgeCIter h = he;
    do {
        area += h->face->area();
        h = h->flip->next;
        
    } while (h != he);
    
    return area / 3.0;
}

bool Vertex::onBoundary() const
{
    HalfEdgeIter h = he;
    do {
        if (h->onBoundary) return true;
        
        h = h->flip->next;
    } while (h != he);
    
    return false;
}
