#include <ll4ma_collision_wrapper/libs/geodesic/HeatGeodesic.h>

using namespace Meshing;
namespace geodesic
{
HeatGeodesic::HeatGeodesic(const TriMesh &mesh)
{
  mesh_=mesh;
  keep_scale=true;
  // load as internal mesh type
  read_trimesh(mesh);
  
  // store values of out mesh:
  
  
}

Eigen::VectorXd HeatGeodesic::computeEuclidean(const int vIdx)
{
  Eigen::Vector3d v_source=vertices[vIdx].position;
  Eigen::VectorXd phis;
  phis.resize(vertices.size());
  phis.setZero();
  for(int i=0;i<vertices.size();++i)
  {
    phis[i]=(vertices[i].position-v_source).norm();
  }
  
  // compute max and min phis
  double minPhi = INFINITY;
  double maxPhi = -INFINITY;
  for (VertexIter v = vertices.begin(); v != vertices.end(); v++) {
    if (minPhi > phis(v->index)) minPhi = phis(v->index);
    if (maxPhi < phis(v->index)) maxPhi = phis(v->index);
  }
  double range = maxPhi - minPhi;
  
  // set phis
  for (VertexIter v = vertices.begin(); v != vertices.end(); v++) {
    v->phi = 1 - ((phis(v->index) - minPhi) / range);
  }
  return phis;
}
bool HeatGeodesic::read_trimesh(const TriMesh &mesh)
{

  // read vertices:
  MeshData data;
  // read points

  // read
  for(int i=0;i<mesh.verts.size();++i)
  {
    data.positions.push_back(Eigen::Vector3d(mesh.verts[i][0],mesh.verts[i][1],mesh.verts[i][2]));
    
  }
  for(int i=0;i<mesh.tris.size();++i)
  {
    std::vector<Index> faceIndices;
    faceIndices.push_back(Index(mesh.tris[i][0],-1,-1));
    faceIndices.push_back(Index(mesh.tris[i][1],-1,-1));
    faceIndices.push_back(Index(mesh.tris[i][2],-1,-1));
    data.indices.push_back(faceIndices);
  }
  cerr<<"Built mesh:"<<MeshIO::buildMesh(data,*this)<<endl;;
  normalize();
  setup();

  return true;
}

double HeatGeodesic::get_distance(const int &dest_vidx)
{
  double dist;
  return dist;
}

double HeatGeodesic::get_distance(const Eigen::Vector3d pt)
{
  double dist;
  return dist;

}


}
