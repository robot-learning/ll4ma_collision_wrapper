#include <ll4ma_collision_wrapper/libs/utils.h>
namespace ll4ma_collision_wrapper
{
ColorizedMesh get_viz_mesh(Meshing::TriMesh mesh, vector<double> heatmap)
{

  // get coloro
  Eigen::Vector3d rgb_min(0.0,0.0,0.0);
  Eigen::Vector3d rgb_max(0.0,0.0,0.0);

  // only visualize vertices, face, color
  ColorizedMesh out;
  for(int i=0;i<mesh.verts.size();++i)
  {
    geometry_msgs::Point32 vertex;
    vertex.x=mesh.verts[i][0];
    vertex.y=mesh.verts[i][1];
    vertex.z=mesh.verts[i][2];
    out.vertices.push_back(vertex);
    // assume no normal info
    geometry_msgs::Vector3 normal;
    normal.x=0;
    normal.y=0;
    normal.z=1;
    out.vertex_normals.push_back(normal);
    // add vertex color:
    std_msgs::ColorRGBA color;
    Eigen::Vector3d rgb;
    color.r=rgb[0];
    color.g=rgb[1];
    color.b=rgb[2];
    color.a=1.0;
    out.vertex_colors.push_back(color);
    
        
    
  }
}
}
