#include <ll4ma_collision_wrapper/libs/kris_ccd/collision_checker.h>
//using namespace kris_ccheck;
using namespace Meshing;
using namespace Geometry;
namespace kris_ccd_ccheck
{

void collisionChecker::checkCollision(Geometry::CollisionMesh* obj_1, Geometry::CollisionMesh* obj_2, const obj_t& obj1, const obj_t& obj2,coll_data &c_data)
{
  coll_data ccd_data;
  //std::cout<< "ll4ma_collision_wrapper/src/libs/kris_ccd line 12" << std::endl;
  checkCollision(obj_1,obj_2,c_data);
  if(!c_data.success)
  {
    //std::cout<< "ll4ma_collision_wrapper/src/libs/kris_ccd line 16" << std::endl;
    checkCollision(obj1,obj2,ccd_data);
    if(ccd_data.collision)
    {
      //std::cout<< "ll4ma_collision_wrapper/src/libs/kris_ccd line 20" << std::endl;
      //c_data.distance=-c_data.distance;
      c_data.collision=true;
    }
  }
  
  
}


void collisionChecker::signedDistance(Geometry::CollisionMesh* obj_1, Geometry::CollisionMesh* obj_2, const obj_t& obj1, const obj_t& obj2,coll_data &c_data,const bool &compute_non_colliding)
{

  coll_data ccd_data,kris_data;
  checkCollision(obj_1,obj_2,kris_data);
  if(!kris_data.success)
  {

    //std::cout<< "ll4ma_collision_wrapper/src/libs/kris_ccd line 36" << std::endl;
    checkCollision(obj1,obj2,ccd_data);
    if(ccd_data.collision) 
    {
      //std::cout<< "ll4ma_collision_wrapper/src/libs/kris_ccd line 40" << std::endl;
      furthestDistance(obj_1,obj_2,c_data);
      c_data.distance=-c_data.distance;
      c_data.collision=true;
    }
    else
    {
      //std::cout<< "ll4ma_collision_wrapper/src/libs/kris_ccd line 47" << std::endl;
      signedDistance(obj_1,obj_2,c_data,true);
      c_data.collision=false; 
    }

  }
  else
  {
    signedDistance(obj_1,obj_2,c_data,compute_non_colliding);
  }

}

void collisionChecker::signedDistance(Geometry::CollisionMesh* obj_1, Geometry::CollisionMesh* obj_2, const obj_t& obj1, const vector<obj_t>& obj2,coll_data &c_data,const bool &compute_non_colliding)
{
  //std::cout<< "ll4ma_collision_wrapper/src/libs/kris_ccd line 58" << std::endl;
  coll_data ccd_data,kris_data;
  checkCollision(obj_1,obj_2,kris_data);

  if(!kris_data.success)
  {
     checkCollision(obj1,obj2,ccd_data);
    if(ccd_data.collision)
    {
      furthestDistance(obj_1,obj_2,c_data);
      c_data.distance=-c_data.distance;
      c_data.collision=true;
    }
    else
    {
      signedDistance(obj_1,obj_2,c_data,true);
      c_data.collision=false;
    }
  }
  else
  {
    signedDistance(obj_1,obj_2,c_data,compute_non_colliding);
  }
  
}

void collisionChecker::signedDistance(Geometry::CollisionMesh* obj_1, Geometry::CollisionMesh* obj_2, const vector<obj_t>& obj1, const vector<obj_t> & obj2,coll_data &c_data,const bool &compute_non_colliding)
{
  //std::cout<< "ll4ma_collision_wrapper/src/libs/kris_ccd line 86" << std::endl;
  coll_data ccd_data,kris_data;
  checkCollision(obj_1,obj_2,kris_data);
  
  if(!kris_data.success)
  {
    checkCollision(obj1,obj2,ccd_data);
    if(ccd_data.collision)
    {
      furthestDistance(obj_1,obj_2,c_data);
      c_data.distance=-c_data.distance;
      c_data.collision=true;
    }
    else
    {
      signedDistance(obj_1,obj_2,c_data,true);
      c_data.collision=false;
    }

  }
  else
  {
    signedDistance(obj_1,obj_2,c_data,compute_non_colliding);
  }

}

void collisionChecker::signedDistance(Geometry::CollisionMesh* obj_1, vector<Geometry::CollisionMesh*> obj_2, const obj_t& obj1, const vector<vector<obj_t>>& obj2,coll_data &c_data,const bool &compute_non_colliding)
{
  //std::cout<< "ll4ma_collision_wrapper/src/libs/kris_ccd line 115" << std::endl;
  double mdist=1000;
  for(int i=0;i<obj_2.size();++i)
  {
    coll_data i_data;
    signedDistance(obj_1,obj_2[i],obj1,obj2[i],i_data,compute_non_colliding);
    if(i_data.distance<mdist)
    {
      c_data=i_data;
      mdist=i_data.distance;
    }
  }

}

void collisionChecker::signedDistance(Geometry::CollisionMesh* obj_1, vector<Geometry::CollisionMesh*> obj_2, const vector<obj_t>& obj1, const vector<vector<obj_t>>& obj2,coll_data &c_data,const bool &compute_non_colliding)
{
  //std::cout<< "ll4ma_collision_wrapper/src/libs/kris_ccd line 132" << std::endl;
  double mdist=1000;
  for(int i=0;i<obj_2.size();++i)
  {
    coll_data i_data;
    signedDistance(obj_1,obj_2[i],obj1,obj2[i],i_data,compute_non_colliding);
    if(i_data.distance<mdist)
    {
      c_data=i_data;
      mdist=i_data.distance;
    }
  }

} 

/* TODO: Need this combination?
void collisionChecker::signedDistance(Geometry::CollisionMesh* obj_1, vector<Geometry::CollisionMesh*> obj_2, const vector<obj_t>& obj1, const vector<obj_t>& obj2,coll_data &c_data,const bool &compute_non_colliding)
{
  double mdist=1000;
  for(int i=0;i<obj_2.size();++i)
  {
    coll_data i_data;
    signedDistance(obj_1,obj_2[i],obj1,obj2[i],i_data,compute_non_colliding);
    if(i_data.distance<mdist)
    {
      c_data=i_data;
      mdist=i_data.distance;
    }
  }

}
*/
void collisionChecker::signedDistance(Geometry::CollisionMesh* obj_1, vector<Geometry::CollisionMesh*> obj_2, const obj_t& obj1, const vector<obj_t>& obj2,coll_data &c_data,const bool &compute_non_colliding)
{
  //std::cout<< "ll4ma_collision_wrapper/src/libs/kris_ccd line 166" << std::endl;
  double mdist=1000;
  for(int i=0;i<obj_2.size();++i)
  {
    coll_data i_data;
    signedDistance(obj_1,obj_2[i],obj1,obj2[i],i_data,compute_non_colliding);
    if(i_data.distance<mdist)
    {
      c_data=i_data;
      mdist=i_data.distance;
    }
  }

}


void collisionChecker::readLinkShapes(string &urdf_txt,vector<string> &link_names, vector<CollisionMesh*> &c_objs,vector<obj_t> &ccd_objs)
{
  urdf::Model model;
  if (!model.initString(urdf_txt))
  {
    ROS_ERROR("Failed to parse urdf file");
  }
  c_objs.resize(link_names.size());
  ccd_objs.resize(link_names.size());
    
  vector<Transform3d> link_offsets;
  link_offsets.resize(link_names.size());
  
  // TODO assert when link name is non existent in the urdf 
  for(int i=0; i<link_names.size();++i)
  {
    c_objs[i]=new CollisionMesh();
    // get geometry type: 0=sphere, 1=box, 2=cylinder, 3=mesh
    std::shared_ptr<const urdf::Link> link = model.getLink(link_names[i]);

    if(link->collision->geometry->type==3)
    {
      // store link visual offset:
      urdf::Pose p=link->collision->origin;
    
      link_offsets[i].setIdentity();
    
      Eigen::Quaterniond q(p.rotation.w,p.rotation.x,p.rotation.y,p.rotation.z);

      Eigen::Vector3d pos(p.position.x,p.position.y,p.position.z);
      link_offsets[i].translate(pos);
      link_offsets[i].rotate(q);

      // build tranform3d from pose:


    
      int type=link->collision->geometry->type;
      
      Eigen::Matrix4d T;
      T.setIdentity();
      T=link_offsets[i].matrix();
      
      link_offsets[i].setIdentity();
      //assert(type==3);// currently only supports meshes (STL)
      if(type==3)
      {
        std::shared_ptr<urdf::Mesh> link_mesh =std::dynamic_pointer_cast<urdf::Mesh>(link->collision->geometry);
        string file_name=ros_uri::absolute_path(link_mesh->filename);
        c_objs[i]=loadMeshShape(file_name,T);
        loadCvxMeshShape(file_name,T,ccd_objs[i]);
        
      }
    }
    else if(link->visual->geometry->type==3)
    {
      cerr<<"#####WARNING: Collision mesh of"<<link_names[i]<<" is a primitive, loading visual mesh instead"<<endl;
      
      // store link visual offset:
      urdf::Pose p=link->visual->origin;
    
      link_offsets[i].setIdentity();
    
      Eigen::Quaterniond q(p.rotation.w,p.rotation.x,p.rotation.y,p.rotation.z);

      Eigen::Vector3d pos(p.position.x,p.position.y,p.position.z);
      link_offsets[i].translate(pos);
      link_offsets[i].rotate(q);

      // build tranform3d from pose:


    
      int type=link->visual->geometry->type;
      
      Eigen::Matrix4d T;
      T.setIdentity();
      T=link_offsets[i].matrix();
      
      link_offsets[i].setIdentity();
      //assert(type==3);// currently only supports meshes (STL)
      if(type==3)
      {
        std::shared_ptr<urdf::Mesh> link_mesh =std::dynamic_pointer_cast<urdf::Mesh>(link->visual->geometry);
        string file_name=ros_uri::absolute_path(link_mesh->filename);
        c_objs[i]=loadMeshShape(file_name,T);
        loadCvxMeshShape(file_name,T,ccd_objs[i]);
        
      }
    }
    else
    {
      cerr<<"ERROR: Only mesh is supported for collision checking!"<<endl;
      exit(0);
    }
  
  }

}
}
