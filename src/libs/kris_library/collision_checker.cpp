#include "ll4ma_collision_wrapper/libs/kris_library/collision_checker.h"
namespace kris_ccheck
{
using namespace Meshing;
using namespace Geometry;

collisionChecker::collisionChecker()
{
  
}

void collisionChecker::loadModel(const string &file_name, TriMesh* obj)
{
  // read file using assimp:
  LoadAssimp(file_name.c_str(),*obj);
  
}

CollisionMesh* collisionChecker::createCollObj(TriMesh* obj, const Eigen::Matrix4d &T_offset, const Eigen::Matrix4d &T_pose)
{
  Math3D::Matrix4 t_offset(Math3D::Vector4(T_offset.col(0)[0],T_offset.col(0)[1],T_offset.col(0)[2],T_offset.col(0)[3]),
                    Math3D::Vector4(T_offset.col(1)[0],T_offset.col(1)[1],T_offset.col(1)[2],T_offset.col(1)[3]),
                    Math3D::Vector4(T_offset.col(2)[0],T_offset.col(2)[1],T_offset.col(2)[2],T_offset.col(2)[3]),
                    Math3D::Vector4(T_offset.col(3)[0],T_offset.col(3)[1],T_offset.col(3)[2],T_offset.col(3)[3]));

  // apply transform to trimesh:
  obj->Transform(t_offset);

   Math3D::Matrix4 t_pose(Math3D::Vector4(T_pose.col(0)[0],T_pose.col(0)[1],T_pose.col(0)[2],T_pose.col(0)[3]),
                    Math3D::Vector4(T_pose.col(1)[0],T_pose.col(1)[1],T_pose.col(1)[2],T_pose.col(1)[3]),
                    Math3D::Vector4(T_pose.col(2)[0],T_pose.col(2)[1],T_pose.col(2)[2],T_pose.col(2)[3]),
                    Math3D::Vector4(T_pose.col(3)[0],T_pose.col(3)[1],T_pose.col(3)[2],T_pose.col(3)[3]));
 
  Math3D::RigidTransform T_mat(t_pose);
  
  // get AABB:
  Math3D::Vector3 bmin,bmax;
  obj->GetAABB(bmin,bmax);
  cerr<<"bmin: "<<bmin<<" bmax: "<<bmax<<endl;
  CollisionMesh* c_1=new CollisionMesh(*obj);
  c_1->UpdateTransform(T_mat);
  // create pose instance:

  return c_1;
}
CollisionMesh* collisionChecker::loadMeshShape(const string &file_name, Eigen::Matrix4d &T)
{
  TriMesh obj;
  loadModel(file_name,&obj);
  
  CollisionMesh* coll_m=new CollisionMesh();
  coll_m=createCollObj(&obj,T);
  return coll_m;
}
void collisionChecker::loadMeshShape(const string &file_name, Eigen::Matrix4d &T,TriMesh &obj)
{
  loadModel(file_name,&obj);
  Math3D::Matrix4 t(Math3D::Vector4(T.col(0)[0],T.col(0)[1],T.col(0)[2],T.col(0)[3]),
                    Math3D::Vector4(T.col(1)[0],T.col(1)[1],T.col(1)[2],T.col(1)[3]),
                    Math3D::Vector4(T.col(2)[0],T.col(2)[1],T.col(2)[2],T.col(2)[3]),
                    Math3D::Vector4(T.col(3)[0],T.col(3)[1],T.col(3)[2],T.col(3)[3]));
  Math3D::RigidTransform T_mat;
  T_mat.setIdentity();

  // apply transform to trimesh:
  obj.Transform(t);
  
}


void collisionChecker::checkCollision(CollisionMesh* obj_1, CollisionMesh* obj_2,coll_data &result)
{
  int result_c=Collide(*obj_1,*obj_2);
  if(result_c==-6)
  {
    result.success=false;
    result.collision=false;
  }
  else
  {
    result.collision=bool(result_c);

    result.success=true;
  }

}


void collisionChecker::checkCollision(CollisionMesh* obj_1,vector<CollisionMesh*> env_arr, coll_data &result)
{
  for(int i=0;i<env_arr.size();++i)
  {
    int result_c=Collide(*obj_1,*env_arr[i]);
    if(result_c==-6)
    {
      result.collision=false;
      result.success=false;
      break;
    }
    result.collision=bool(result_c);
  }


}

void collisionChecker::signedDistance(CollisionMesh* obj_1, CollisionMesh* obj_2, coll_data &result,bool compute_non_colliding)
{
  int coll_res=Collide(*obj_1,*obj_2);

  bool colliding=false;
  if(coll_res>0)
  {
    colliding=true;
  }
  if(!colliding) // if not colliding
  {
    if(!compute_non_colliding) // if signeddistance is only needed when in collision
    {
      result.collision=false;
      result.p1=Eigen::Vector3d(0,0,0);
      result.p2=Eigen::Vector3d(0,0,0);
      result.relp1=Eigen::Vector3d(0,0,0);
      result.relp2=Eigen::Vector3d(0,0,0);

      result.distance=10.0;
      return;
    }
  }


  
  CollisionMeshQuery coll_q(*obj_1,*obj_2);
  Math3D::Vector3 p1,p2,d1;// nearest points:
  
  double d=coll_q.Distance(0.000001,0.000001);
  coll_q.ClosestPoints(p1,p2);    
  if(!colliding) // not in collision
  {
    result.collision=false;
  }
  else
  {
    d=copysign(coll_q.PenetrationDepth(),-1.0);
    coll_q.PenetrationPoints(p1,p2,d1);
    result.collision=true;

  }
  
  result.distance=d;

  //
  result.relp1=Eigen::Vector3d(p1[0],p1[1],p1[2]);
  result.relp2=Eigen::Vector3d(p2[0],p2[1],p2[2]);
  
  Math3D::RigidTransform t;
  Math3D::Matrix4 t_kris;
  obj_1->GetTransform(t);
  t.get(t_kris);
  (t_kris*Math3D::Vector4(p1)).get(p1);

  Math3D::RigidTransform t2;
  Math3D::Matrix4 t2_kris;
  
  obj_2->GetTransform(t2);
  t2.get(t2_kris);

  (t2_kris*Math3D::Vector4(p2)).get(p2);
  result.p1=Eigen::Vector3d(p1[0],p1[1],p1[2]);
  
  result.p2=Eigen::Vector3d(p2[0],p2[1],p2[2]);
  
  
}

void collisionChecker::signedDistance(CollisionMesh* obj_1,vector<CollisionMesh*> env_arr, coll_data &result,bool compute_non_colliding)
{
  //std::cout << "line 178" << std::endl;
  double mdist=1000.0;
  int idx=0;
  for(int i=0;i<env_arr.size();++i)
  {
    coll_data c_data;
    signedDistance(obj_1,env_arr[i],c_data,compute_non_colliding);
    if(c_data.distance<mdist)
    {
      result=c_data;
      mdist=c_data.distance;
      idx=i;
    }
  }

  /* Moving this to the single signeddistance function:

  Math3D::RigidTransform t;
  Math3D::Matrix4 t_kris;
  obj_1->GetTransform(t);
  t.get(t_kris);
  double data[16];
  t_kris.get(data);
  Eigen::Matrix4d T_mat(data);
  Transform3d t_mat(T_mat);
  result.p1=t_mat*result.p1;

  env_arr[idx]->GetTransform(t);
  t.get(t_kris);
  t_kris.get(data);
  Eigen::Matrix4d T_mat2(data);
  Transform3d t_mat2(T_mat2);
  result.p2=t_mat2*result.p2;
  */
}
void collisionChecker::furthestDistance(CollisionMesh* obj_1, CollisionMesh* obj_2, coll_data &result)
{
  
  CollisionMeshQuery coll_q(*obj_1,*obj_2);

  Math3D::Vector3 p1,p2,d1;// nearest points:
  
  double d=coll_q.FurthestDistance(0.000001,0.000001);
  coll_q.ClosestPoints(p1,p2);    
  
  result.distance=d;

  //
  result.relp1=Eigen::Vector3d(p1[0],p1[1],p1[2]);
  result.relp2=Eigen::Vector3d(p2[0],p2[1],p2[2]);
  
  Math3D::RigidTransform t;
  Math3D::Matrix4 t_kris;
  obj_1->GetTransform(t);
  t.get(t_kris);
  (t_kris*Math3D::Vector4(p1)).get(p1);

  Math3D::RigidTransform t2;
  Math3D::Matrix4 t2_kris;
  
  obj_2->GetTransform(t2);
  t2.get(t2_kris);

  (t2_kris*Math3D::Vector4(p2)).get(p2);
  result.p1=Eigen::Vector3d(p1[0],p1[1],p1[2]);
  
  result.p2=Eigen::Vector3d(p2[0],p2[1],p2[2]);
  
  
}

 
void collisionChecker::signedDistance(CollisionMesh* obj_1,vector<CollisionMesh*> env_arr, vector<coll_data> &result,bool compute_non_colliding)
{
  
  //std::cout << "line 252" << std::endl;
  result.resize(env_arr.size());
  for(int i=0;i<env_arr.size();++i)
  {
    signedDistance(obj_1,env_arr[i],result[i],compute_non_colliding);
  }
}

void collisionChecker::readLinkShapes(string &urdf_txt,vector<string> &link_names, vector<TriMesh*> &c_objs, vector<Transform3d> &link_offsets)
{
  urdf::Model model;
  if (!model.initString(urdf_txt))
  {
    ROS_ERROR("Failed to parse urdf file");
  }
  c_objs.resize(link_names.size());
  link_offsets.resize(link_names.size());
  
  // TODO assert when link name is non existent in the urdf 
  for(int i=0; i<link_names.size();++i)
  {
    c_objs[i]=new TriMesh();
    // get geometry type: 0=sphere, 1=box, 2=cylinder, 3=mesh
    //std::shared_ptr<const urdf::Link>
    auto link = model.getLink(link_names[i]);
    
    // store link visual offset:
    urdf::Pose p=link->collision->origin;
    
    link_offsets[i].setIdentity();
    
    Eigen::Quaterniond q(p.rotation.w,p.rotation.x,p.rotation.y,p.rotation.z);

    Eigen::Vector3d pos(p.position.x,p.position.y,p.position.z);
    link_offsets[i].translate(pos);
    link_offsets[i].rotate(q);

    // build tranform3d from pose:


    
    int type=link->collision->geometry->type;
    Eigen::Matrix4d T;
    T.setIdentity();
    T=link_offsets[i].matrix();
    
    link_offsets[i].setIdentity();
    //assert(type==3);
    if(type==0)
    {
      std::shared_ptr<urdf::Sphere> sph =
          std::dynamic_pointer_cast<urdf::Sphere>(link->collision->geometry);
      double r=sph->radius;
      loadSphereShape(r,T,*c_objs[i]);
    }

    else if(type==1)
    {
      std::shared_ptr<urdf::Box> box = std::dynamic_pointer_cast<urdf::Box>(link->collision->geometry);
      double x=box->dim.x;
      double y=box->dim.y;
      double z=box->dim.z;

      loadBoxShape(x,y,z,T,*c_objs[i]);

    }
    else if(type==2)
    {
      std::shared_ptr<urdf::Cylinder> cylinder = std::dynamic_pointer_cast<urdf::Cylinder>(link->collision->geometry);
      double r=cylinder->radius;
      double lz=cylinder->length;
      
      loadCylinderShape(r,lz,T,*c_objs[i]);
    }
    else if(type==3)
    {
      std::shared_ptr<urdf::Mesh> link_mesh = std::dynamic_pointer_cast<urdf::Mesh>(link->collision->geometry);
      string file_name=ros_uri::absolute_path(link_mesh->filename);
      loadMeshShape(file_name,T,*c_objs[i]);
    }
  }
}

void collisionChecker::readLinkShapes(string &urdf_txt,vector<string> &link_names, vector<CollisionMesh*> &c_objs, vector<Transform3d> &link_offsets)
{
  urdf::Model model;
  if (!model.initString(urdf_txt))
  {
    ROS_ERROR("Failed to parse urdf file");
  }
  c_objs.resize(link_names.size());
  link_offsets.resize(link_names.size());
  
  // TODO assert when link name is non existent in the urdf 
  for(int i=0; i<link_names.size();++i)
  {
    // get geometry type: 0=sphere, 1=box, 2=cylinder, 3=mesh
    std::shared_ptr<const urdf::Link> link = model.getLink(link_names[i]);

    // store link collision offset:
    urdf::Pose p=link->collision->origin;
    
    link_offsets[i].setIdentity();
    
    Eigen::Quaterniond q(p.rotation.w,p.rotation.x,p.rotation.y,p.rotation.z);

    Eigen::Vector3d pos(p.position.x,p.position.y,p.position.z);
    link_offsets[i].translate(pos);
    link_offsets[i].rotate(q);

    // build tranform3d from pose:


    
    int type=link->collision->geometry->type;
    Eigen::Matrix4d T;
    T.setIdentity();
    //T=link_offsets[i].matrix();
    //link_offsets[i].setIdentity();
    //assert(type==3);
    if(type==0)
    {
      std::shared_ptr<urdf::Sphere> sph =std::dynamic_pointer_cast<urdf::Sphere>(link->collision->geometry);
      double r=sph->radius;
      c_objs[i]=loadSphereShape(r,T);
    }

    else if(type==1)
    {
      std::shared_ptr<urdf::Box> box =std::dynamic_pointer_cast<urdf::Box>(link->collision->geometry);
      double x=box->dim.x;
      double y=box->dim.y;
      double z=box->dim.z;

      c_objs[i]=loadBoxShape(x,y,z,T);

    }
    else if(type==2)
    {
      std::shared_ptr<urdf::Cylinder> cylinder = std::dynamic_pointer_cast<urdf::Cylinder>(link->collision->geometry);
      double r=cylinder->radius;
      double lz=cylinder->length;
      
      c_objs[i]=loadCylinderShape(r,lz,T);
    }
    else if(type==3)
    {
      std::shared_ptr<urdf::Mesh> link_mesh =std::dynamic_pointer_cast<urdf::Mesh>(link->collision->geometry);
      string file_name=ros_uri::absolute_path(link_mesh->filename);
      c_objs[i]=loadMeshShape(file_name,T);
    }
  }
}

CollisionMesh* collisionChecker::loadBoxShape(double lx, double ly, double lz, Eigen::Matrix4d &T)
{
  // load primitive shape
  Math3D::Box3D box;
  box.xbasis=Math3D::Vector3(1,0,0);
  box.ybasis=Math3D::Vector3(0,1,0);
  box.zbasis=Math3D::Vector3(0,0,1);
  box.dims.x=lx;
  box.dims.y=ly;
  box.dims.z=lz;
  box.setCenter(Math3D::Vector3(0,0,0));
  TriMesh shape_mesh;
  PrimitiveToMesh(box,shape_mesh,10);
  //cerr<<"box verts:"<<shape_mesh.verts.size()<<endl;
  CollisionMesh* shape_coll=new CollisionMesh();
  shape_coll=createCollObj(&shape_mesh,T);
  //cerr<<box<<endl;
  return shape_coll;
}

void collisionChecker::loadBoxShape(double lx, double ly, double lz, Eigen::Matrix4d &T,TriMesh &shape_mesh)
{
  // load primitive shape
  Math3D::Box3D box;
  box.xbasis=Math3D::Vector3(1,0,0);
  box.ybasis=Math3D::Vector3(0,1,0);
  box.zbasis=Math3D::Vector3(0,0,1);
  box.dims.x=lx;
  box.dims.y=ly;
  box.dims.z=lz;
  box.setCenter(Math3D::Vector3(0,0,0));
  PrimitiveToMesh(box,shape_mesh,10);
}

CollisionMesh* collisionChecker::loadCylinderShape(double radius, double lz,Eigen::Matrix4d &T)
{
  Math3D::Cylinder3D cyl;
  cyl.radius=radius;
  cyl.height=lz;
  cyl.center=Math3D::Vector3(0,0,0);

  TriMesh shape_mesh;
  PrimitiveToMesh(cyl,shape_mesh,10);
  CollisionMesh* shape_coll=new CollisionMesh();
  shape_coll=createCollObj(&shape_mesh,T);
  return shape_coll;
}
void collisionChecker::loadCylinderShape(double radius, double lz,Eigen::Matrix4d &T,TriMesh &out)
{
  Math3D::Cylinder3D cyl;
  cyl.radius=radius;
  cyl.height=lz;
  cyl.center=Math3D::Vector3(0,0,0);

  PrimitiveToMesh(cyl,out,10);
}

CollisionMesh* collisionChecker::loadSphereShape(double radius, Eigen::Matrix4d &T)
{
  Math3D::Sphere3D sph;
  sph.radius=radius;
  sph.center=Math3D::Vector3(0,0,0);

  TriMesh shape_mesh;
  PrimitiveToMesh(sph,shape_mesh,10);
  CollisionMesh* shape_coll=new CollisionMesh();
  shape_coll=createCollObj(&shape_mesh,T);
  return shape_coll;
 
}
void collisionChecker::loadSphereShape(double radius, Eigen::Matrix4d &T,TriMesh &shape_mesh)
{
  Math3D::Sphere3D sph;
  sph.radius=radius;
  sph.center=Math3D::Vector3(0,0,0);

  PrimitiveToMesh(sph,shape_mesh,10);

}
void collisionChecker::updatePose(CollisionMesh* obj, vector<double> &pose)
{
  // setup transform:
  Math3D::RigidTransform T;
  // compute R matrix from quaternion:
  T.setTranslation(Math3D::Vector3(pose[0],pose[1],pose[2]));
  Math3D::QuaternionRotation r_q(pose[6],pose[3],pose[4],pose[5]);
  Math3D::Matrix3 r_mat;
  r_q.getMatrix(r_mat);
  T.setRotation(r_mat);
  //Math3D::SetMatrixRotationQuaternion (T.R, Math3D::Quaternion(pose[6],pose[3],pose[4],pose[5]));
  //cerr<<"**************"<<T<<endl;
  obj->UpdateTransform(T);
}

void collisionChecker::updatePose(CollisionMesh* obj, Transform3d &pose)
{
  Eigen::Matrix4d T=pose.matrix();
  // setup transform:
  
 Math3D::RigidTransform T_(Math3D::Matrix4(Math3D::Vector4(T.col(0)[0],T.col(0)[1],T.col(0)[2],T.col(0)[3]),
                                           Math3D::Vector4(T.col(1)[0],T.col(1)[1],T.col(1)[2],T.col(1)[3]),
                                           Math3D::Vector4(T.col(2)[0],T.col(2)[1],T.col(2)[2],T.col(2)[3]),
                                           Math3D::Vector4(T.col(3)[0],T.col(3)[1],T.col(3)[2],T.col(3)[3])));
  // compute R matrix from quaternion:
  obj->UpdateTransform(T_);
}


void collisionChecker::updatePose(TriMesh* obj, Transform3d &pose)
{
  Eigen::Matrix4d T=pose.matrix();
  // setup transform:
  
 Math3D::RigidTransform T_(Math3D::Matrix4(Math3D::Vector4(T.col(0)[0],T.col(0)[1],T.col(0)[2],T.col(0)[3]),
                                           Math3D::Vector4(T.col(1)[0],T.col(1)[1],T.col(1)[2],T.col(1)[3]),
                                           Math3D::Vector4(T.col(2)[0],T.col(2)[1],T.col(2)[2],T.col(2)[3]),
                                           Math3D::Vector4(T.col(3)[0],T.col(3)[1],T.col(3)[2],T.col(3)[3])));
  // compute R matrix from quaternion:
  obj->Transform(T_);
}

void collisionChecker::updatePose(TriMesh* &obj, vector<double> &pose)
{
  Math3D::RigidTransform T;
  // compute R matrix from quaternion:
  T.setTranslation(Math3D::Vector3(pose[0],pose[1],pose[2]));
  Math3D::SetMatrixRotationQuaternion (T.R, Math3D::Quaternion(pose[6],pose[3],pose[4],pose[5]));
  obj->Transform(T);
}

void collisionChecker::updatePose(vector<CollisionMesh*> &obj, vector<Transform3d> &pose)
{
  // TODO: optimize initialization
  assert(obj.size()==pose.size());
  for(int i=0;i<obj.size();++i)
  {
    updatePose(obj[i],pose[i]);
  }
}

void collisionChecker::updatePose(vector<CollisionMesh*> &obj, vector<double> &pose)
{
  Math3D::RigidTransform T;
  // compute R matrix from quaternion:
  T.setTranslation(Math3D::Vector3(pose[0],pose[1],pose[2]));
  Math3D::SetMatrixRotationQuaternion (T.R, Math3D::Quaternion(pose[6],pose[3],pose[4],pose[5]));
  for(int i=0;i<obj.size();++i)
  {
    obj[i]->UpdateTransform(T);
  }
}
  
void collisionChecker::updatePose(vector<CollisionMesh*> &obj, vector<vector<double>> &pose)
{
  // TODO: optimize initialization
  assert(obj.size()==pose.size());
  for(int i=0;i<obj.size();++i)
  {
    Math3D::RigidTransform T;
    // compute R matrix from quaternion:
    T.setTranslation(Math3D::Vector3(pose[i][0],pose[i][1],pose[i][2]));
    Math3D::SetMatrixRotationQuaternion (T.R, Math3D::Quaternion(pose[i][6],pose[i][3],pose[i][4],pose[i][5]));
    obj[i]->UpdateTransform(T);
  }
}

// Pointcloud to collision object:
CollisionMesh* collisionChecker::loadPCDFile(const string &file_name,Eigen::Matrix4d &T)
{
  PointCloud3D pcd_data;
  pcd_data.LoadPCL(file_name.c_str());
  TriMesh pcd_mesh;
  PointCloudToMesh(pcd_data,pcd_mesh,0.02);// TODO: verify 0.02 value for sensors
  //cout<<"Pointcloud Mesh size: vertices "<<pcd_mesh.verts.size()<<" Triangles: "<<pcd_mesh.tris.size()<<endl;

  CollisionMesh* pcd_coll=new CollisionMesh();
  pcd_coll=createCollObj(&pcd_mesh,T);
  return pcd_coll;
}
/*
void collisionChecker::store_offsets(vector<CollisionMesh*> &obj, vector<Transform3d> &offsets)
{
  offsets.resize(obj.size());
  for(int i=0;i<obj.size();++i)
  {
    offsets[i]=obj[i]->getTransform();
  }
}
*/

void collisionChecker::addOffsets(vector<Transform3d> &offsets, vector<vector<double>> &poses, vector<Transform3d> &out_poses)
{
  
  out_poses.resize(poses.size());
  // TODO: this is making the computations slower, find a better way!!!
  //Quaterniond q;
  //Vector3d p;
  for(int i=0;i<out_poses.size();++i)
  {
    Eigen::Quaterniond q(poses[i][6],poses[i][3],poses[i][4],poses[i][5]);// fcl quaternion(w,x,y,z)
    Eigen::Vector3d p(poses[i][0],poses[i][1],poses[i][2]);
    out_poses[i].setIdentity();
    out_poses[i].translate(p);
    out_poses[i].rotate(q);
    //std::cerr<<offsets[i].matrix()<<std::endl;
    out_poses[i]=out_poses[i]*offsets[i];
  }
  
}
void collisionChecker::addOffsets(vector<Transform3d> &offsets, vector<double> &pose, vector<Transform3d> &out_poses)
{
  
  out_poses.resize(offsets.size());
  // TODO: this is making the computations slower, find a better way!!!
  //Quaterniond q;
  //Vector3d p;
  Eigen::Quaterniond q(pose[6],pose[3],pose[4],pose[5]);// fcl quaternion(w,x,y,z)
  Eigen::Vector3d p(pose[0],pose[1],pose[2]);

  for(int i=0;i<out_poses.size();++i)
  {
    out_poses[i].setIdentity();
    out_poses[i].translate(p);
    out_poses[i].rotate(q);

    out_poses[i]=out_poses[i]*offsets[i];
  }
  
}
void collisionChecker::addOffsets(Transform3d &offset, vector<double> &pose, Transform3d &out_pose)
{
  Eigen::Quaterniond q(pose[6],pose[3],pose[4],pose[5]);// fcl quaternion(w,x,y,z)
  Eigen::Vector3d p(pose[0],pose[1],pose[2]);
  out_pose.setIdentity();
  out_pose.translate(p);
  out_pose.rotate(q);

  out_pose=out_pose*offset;

}
void collisionChecker::getTransform3d(vector<double> &pose, Transform3d &out_pose)
{
  Eigen::Quaterniond q(pose[6],pose[3],pose[4],pose[5]);// fcl quaternion(w,x,y,z)
  Eigen::Vector3d p(pose[0],pose[1],pose[2]);
  out_pose.setIdentity();
  out_pose.translate(p);
  out_pose.rotate(q);

}
void collisionChecker::getTransform3d(vector<vector<double>> &poses, vector<Transform3d> &out_poses)
{
  out_poses.resize(poses.size());
  for(int i=0;i<poses.size();++i)
  {
    getTransform3d(poses[i],out_poses[i]);
  }
}
}
