#include "ll4ma_collision_wrapper/libs/pcl_add/pcl_eigen_conversion.h"


namespace pcl_eigen
{

void pclEigen::loadPLY(const std::string &file_name, vector<float> &verts, vector<float> &norms,    std::vector<uint32_t> &faces) const
{
  {
  using namespace tinyply;
 
  try
  {
    // Read the file and create a std::istringstream suitable
    // for the lib -- tinyply does not perform any file i/o.
    std::ifstream ss(file_name, std::ios::binary);
    
    // Parse the ASCII header fields

    PlyFile file(ss);
    /*
    for (auto e : file.get_elements())
    {
      std::cout << "element - " << e.name << " (" << e.size << ")" << std::endl;
      for (auto p : e.properties)
      {
        std::cout << "\tproperty - " << p.name << " (" << PropertyTable[p.propertyType].str << ")" << std::endl;
      }
    }
    std::cout << std::endl;
   
    for (auto c : file.comments)
    {
      std::cout << "Comment: " << c << std::endl;
    }
    */
    // Define containers to hold the extracted data. The type must match
    // the property type given in the header. Tinyply will interally allocate the
    // the appropriate amount of memory.
    //std::vector<double> verts;

    //std::vector<uint8_t> colors;
    

    //std::vector<float> uvCoords;
    
    uint32_t vertexCount, normalCount, colorCount, faceCount, faceTexcoordCount, faceColorCount;
    vertexCount = normalCount = colorCount = faceCount = faceTexcoordCount = faceColorCount = 0;
    
    // The count returns the number of instances of the property group. The vectors
    // above will be resized into a multiple of the property group size as
    // they are "flattened"... i.e. verts = {x, y, z, x, y, z, ...}
    vertexCount = file.request_properties_from_element("vertex", { "x", "y", "z" }, verts);
    normalCount = file.request_properties_from_element("vertex", { "nx", "ny", "nz" }, norms);
    //colorCount = file.request_properties_from_element("vertex", { "red", "green", "blue", "alpha" }, colors);
    
    // For properties that are list types, it is possibly to specify the expected count (ideal if a
    // consumer of this library knows the layout of their format a-priori). Otherwise, tinyply
    // defers allocation of memory until the first instance of the property has been found
    // as implemented in file.read(ss)
    faceCount = file.request_properties_from_element("face", { "vertex_indices" }, faces, 3);
    //faceTexcoordCount = file.request_properties_from_element("face", { "texcoord" }, uvCoords, 6);
    
    // Now populate the vectors...
    file.read(ss);
    /*
    // Good place to put a breakpoint!
    //std::cout << "Parsing took " << difference_micros(before, after) << "μs: " << std::endl;
    std::cout << "\tRead " << verts.size() << " total vertices (" << vertexCount << " properties)." << std::endl;
    std::cout << "\tRead " << norms.size() << " total normals (" << normalCount << " properties)." << std::endl;
    //std::cout << "\tRead " << colors.size() << " total vertex colors (" << colorCount << " properties)." << std::endl;
    std::cout << "\tRead " << faces.size() << " total faces (triangles) (" << faceCount << " properties)." << std::endl;
    //std::cout << "\tRead " << uvCoords.size() << " total texcoords (" << faceTexcoordCount << " properties)." << std::endl;

    */
        
  }

  catch (const std::exception & e)
  {
    std::cerr << "Caught exception while reading ply file: " << e.what() << std::endl;
  }
  }  
}
void pclEigen::loadPLY(const std::string &file_name,vector<Vector3d>& points) const
{
  PCLPointCloud2 _cloud_pcl2 ;//(new typename pcl::PointCloud<pcl::PointXYZ>);
  PointCloud<pcl::PointXYZ>::Ptr _cloud;// (new typename pcl::PointCloud<pcl::PointXYZ>);

  if (pcl::io::loadPLYFile (file_name, _cloud_pcl2) != 0) FILE_OPEN_ERROR(file_name);

  #ifdef DEBUG
  cerr<<"Loaded ply file into pointcloud2 with size: "<<_cloud_pcl2.data.size()<<endl;
  #endif

  // removing nans:
  removenans(_cloud_pcl2,0);

  PointCloud<PointXYZ>::Ptr temp_cloud(new PointCloud<PointXYZ>);
  _cloud=temp_cloud;
  
  // Convert to pcl pointcloud:
  pcl::fromPCLPointCloud2 (_cloud_pcl2, *_cloud);
  #ifdef DEBUG
  cerr<<"Converted pointcloud2 to pointcloud"<<endl;
  #endif

  // Load cloud into vector:
  for (int j=0; j < _cloud->size(); ++j) {
    points.emplace_back(_cloud->points[j].x,_cloud->points[j].y,_cloud->points[j].z);
  }  
  
}

void pclEigen::loadPLY(const std::string &file_name, PointCloud<pcl::PointXYZ>::Ptr cloud) const
{
  PCLPointCloud2 _cloud_pcl2 ;//(new typename pcl::PointCloud<pcl::PointXYZ>);


  if (pcl::io::loadPLYFile (file_name, _cloud_pcl2) != 0) FILE_OPEN_ERROR(file_name);
  // removing nans:
  removenans(_cloud_pcl2,0);
  //cerr<<"removed nans"<<endl;
  // Convert to pcl pointcloud:
  pcl::fromPCLPointCloud2 (_cloud_pcl2, *cloud);
  //cerr<<"loaded"<<cloud->size()<<endl;
}


inline void pclEigen::randPSurface (vtkPolyData * polydata, std::vector<double> * cumulativeAreas, double totalArea, Eigen::Vector3f& p) const
{
  float r = static_cast<float> (uniform_deviate (rand ()) * totalArea);

  std::vector<double>::iterator low = std::lower_bound (cumulativeAreas->begin (), cumulativeAreas->end (), r);
  vtkIdType el = vtkIdType (low - cumulativeAreas->begin ());

  double A[3], B[3], C[3];
  vtkIdType npts = 0;
  vtkIdType *ptIds = NULL;
  polydata->GetCellPoints (el, npts, ptIds);
  polydata->GetPoint (ptIds[0], A);
  polydata->GetPoint (ptIds[1], B);
  polydata->GetPoint (ptIds[2], C);

  float r1 = static_cast<float> (uniform_deviate (rand ()));
  float r2 = static_cast<float> (uniform_deviate (rand ()));
  randomPointTriangle (float (A[0]), float (A[1]), float (A[2]),
                       float (B[0]), float (B[1]), float (B[2]),
                       float (C[0]), float (C[1]), float (C[2]), r1, r2, p);
}
inline double pclEigen::uniform_deviate (int seed) const
{
  double ran = seed * (1.0 / (RAND_MAX + 1.0));
  return ran;
}

inline void pclEigen::randomPointTriangle (float a1, float a2, float a3, float b1, float b2, float b3, float c1, float c2, float c3,                     float r1, float r2, Eigen::Vector3f& p) const
{
  float r1sqr = std::sqrt (r1);
  float OneMinR1Sqr = (1 - r1sqr);
  float OneMinR2 = (1 - r2);
  a1 *= OneMinR1Sqr;
  a2 *= OneMinR1Sqr;
  a3 *= OneMinR1Sqr;
  b1 *= OneMinR2;
  b2 *= OneMinR2;
  b3 *= OneMinR2;
  c1 = r1sqr * (r2 * c1 + b1) + a1;
  c2 = r1sqr * (r2 * c2 + b2) + a2;
  c3 = r1sqr * (r2 * c3 + b3) + a3;
  p[0] = c1;
  p[1] = c2;
  p[2] = c3;
}
bool pclEigen::meshSampling(pcl::PolygonMesh &in_mesh, PointCloud<PointXYZ>::Ptr &cloud_out)  const
{
  size_t n_samples=1000; // TODO: find good value
  bool calc_normal=false;
  bool calc_color=false;
  
  vtkSmartPointer<vtkPolyData> polydata;

  pcl::io::mesh2vtk (in_mesh, polydata);

  polydata->BuildCells ();
  vtkSmartPointer<vtkCellArray> cells = polydata->GetPolys ();

  double p1[3], p2[3], p3[3], totalArea = 0;
  std::vector<double> cumulativeAreas (cells->GetNumberOfCells (), 0);
  size_t i = 0;
  vtkIdType npts = 0, *ptIds = NULL;
  for (cells->InitTraversal (); cells->GetNextCell (npts, ptIds); i++)
  {
    polydata->GetPoint (ptIds[0], p1);
    polydata->GetPoint (ptIds[1], p2);
    polydata->GetPoint (ptIds[2], p3);
    totalArea += vtkTriangle::TriangleArea (p1, p2, p3);
    cumulativeAreas[i] = totalArea;
  }

  cloud_out->points.resize (n_samples);
  cloud_out->width = static_cast<pcl::uint32_t> (n_samples);
  cloud_out->height = 1;

  for (i = 0; i < n_samples; i++)
  {
    Eigen::Vector3f p;
    randPSurface (polydata, &cumulativeAreas, totalArea, p);
    cloud_out->points[i].x = p[0];
    cloud_out->points[i].y = p[1];
    cloud_out->points[i].z = p[2];
  }
  VoxelGrid<PointXYZ> grid_;
  grid_.setInputCloud (cloud_out);
  grid_.setLeafSize (0.02,0.02,0.02);
  grid_.filter (*cloud_out);
  return true;
}
bool pclEigen::pointcloudFromMesh(const std::string& mesh_file, PointCloud<PointXYZ>::Ptr &out_cloud) const
{
  pcl::PolygonMesh mesh;
  
  pcl::io::loadPolygonFile(mesh_file,mesh);

  if(mesh.polygons.size()==0)// if mesh has only vertices, we assume its a dense cloud
  {
    return true;
  }
  else
  {
      // perform mesh sampling

    if(!meshSampling(mesh,out_cloud))
    {
      return false;
    }
    return true;

  }
    
  
}
bool pclEigen::loadMesh(const std::string& mesh_file, vector<Vector3d> &points) const
{
  PointCloud<PointXYZ>::Ptr cloud (new PointCloud<pcl::PointXYZ>);
  pointcloudFromMesh(mesh_file,cloud);
  // Load cloud into vector:
  for (int j=0; j < cloud->size(); ++j)
  {
    points.emplace_back(cloud->points[j].x,cloud->points[j].y,cloud->points[j].z);
  }  
  return true;
  
}

void pclEigen::convex_decomp(const std::string& mesh_file, vector<vector<Vector3d>> &out_cluster,const float &vsize) const
{
  PointCloud<pcl::PointXYZ>::Ptr out_cloud (new pcl::PointCloud<pcl::PointXYZ>);
  pointcloudFromMesh(mesh_file,out_cloud);
  convex_decomp(out_cloud,out_cluster,vsize);
}

void pclEigen::loadPCD(const std::string &file_name,PointCloud<pcl::PointXYZ>::Ptr cloud) const
{
  PCLPointCloud2 _cloud_pcl2 ;//(new typename pcl::PointCloud<pcl::PointXYZ>);

  if (pcl::io::loadPCDFile (file_name, _cloud_pcl2) != 0) FILE_OPEN_ERROR(file_name);

  // removing nans:
  removenans(_cloud_pcl2,0);
  
  // Convert to pcl pointcloud:
  pcl::fromPCLPointCloud2 (_cloud_pcl2, *cloud);
  
}

void pclEigen::convex_decomp(const vector<Vector3d> &verts, vector<vector<Vector3d>>& objects,const float &vsize) const
{
  // transform vertices to point cloud

  PointCloud<pcl::PointXYZ>::Ptr cloud(new PointCloud<pcl::PointXYZ>);
  cloud->points.resize(verts.size());
  for(int j=0;j<verts.size();++j)
  {
    cloud->points[j].x=verts[j][0];
    cloud->points[j].y=verts[j][1];
    cloud->points[j].z=verts[j][2];
    
  }
  // send to convex decomp function
  convex_decomp(cloud,objects,vsize);
}
void pclEigen::convex_decomp(const PointCloud<pcl::PointXYZ>::Ptr cloud, vector<vector<Vector3d>>& objects,const float &vsize) const
{
  #ifdef DEBUG
  cerr<<"Running Convex decomp"<<endl;
  #endif
  vector<vector<int> > label2inds;    
  MatrixXf _dirs;
  _dirs = getSpherePoints(1);

  label2inds.clear();
  #ifdef DEBUG
  cerr<<"Convx decomp..."<<endl;
  #endif

  
  // Perform convex decomposition on pointcloud
  ConvexDecomp(cloud, _dirs, vsize, &label2inds, NULL);
  #ifdef DEBUG
  cerr<<"Convex Decomp done"<<endl;
  #endif


  // Store objects as list of objects:
  
  typedef pair<int,int> IntPair;
  vector< IntPair > size_label;
  for (int i=0; i < label2inds.size(); ++i) {
    size_label.push_back(IntPair(-label2inds[i].size(), i));
  }

  std::sort(size_label.begin(), size_label.end());

  vector<Vector3d> vertices;
  objects.clear();

  for(int i=0;i<size_label.size();++i)
    {
      vertices.clear();
      int label = size_label[i].second;
  
  
      for (int j=0; j < label2inds[label].size(); ++j)
	{
	  PointXYZ  oldpt = cloud->points[label2inds[label][j]];
	  vertices.emplace_back(oldpt.x,oldpt.y,oldpt.z);
	}
  
      objects.emplace_back(vertices);
    }
    
}
void pclEigen::init() const
{

}

void pclEigen::removenans(pcl::PCLPointCloud2& cloud,const float &fillval=0) const
{
  int npts = cloud.width * cloud.height;
  for (int i=0; i < npts; ++i) {
    float* ptdata = (float*)(cloud.data.data() + cloud.point_step * i);
    for (int j=0; j < 3; ++j) {
      if (!isfinite(ptdata[j])) ptdata[j] = fillval;
    }
  }
}
void pclEigen::removenans(PointCloud<PointXYZ>::Ptr cloud) const
{
  vector<int> ind;
  pcl::removeNaNFromPointCloud(*cloud,*cloud,ind); 
}

void pclEigen::downsample_cloud(PointCloud<PointXYZ>::Ptr cloud,const float &vsize) const
{
  //cerr<<cloud->isOrganized()<<endl;
  /*
  pcl::VoxelGrid<PointXYZ > sor;
  sor.setInputCloud (cloud);
  //sor.setKeepOrganized(true);

  sor.setLeafSize (vsize, vsize, vsize);
  sor.filter (*cloud);
  cerr<<cloud->isOrganized()<<endl;
  */
  // remove outliers:
  pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor_1;
  sor_1.setInputCloud (cloud);
  sor_1.setKeepOrganized(true);

  sor_1.setMeanK (50);
  sor_1.setStddevMulThresh (0.05);
  sor_1.filter (*cloud);
}
void pclEigen::bound_cloud(PointCloud<PointXYZ>::Ptr cloud,const vector<float> &min_,const vector<float> &max_) const
{
  // Remove points that are out of the bounding box:

 
  // Use conditional filtering
  pcl::ConditionAnd<pcl::PointXYZ>::Ptr range_cond (new   pcl::ConditionAnd<pcl::PointXYZ> ());
  
  range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
      pcl::FieldComparison<pcl::PointXYZ> ("x", pcl::ComparisonOps::GT, min_[0])));
  
  range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
      pcl::FieldComparison<pcl::PointXYZ> ("x", pcl::ComparisonOps::LT,max_[0])));

  range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
      pcl::FieldComparison<pcl::PointXYZ> ("y", pcl::ComparisonOps::GT, min_[1])));
  
  range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
      pcl::FieldComparison<pcl::PointXYZ> ("y", pcl::ComparisonOps::LT,max_[1])));
  range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
      pcl::FieldComparison<pcl::PointXYZ> ("z", pcl::ComparisonOps::GT, min_[2])));
  
  range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
      pcl::FieldComparison<pcl::PointXYZ> ("z", pcl::ComparisonOps::LT,max_[2])));


  // build the filter
  pcl::ConditionalRemoval<pcl::PointXYZ> condrem;
  condrem.setCondition (range_cond);
  condrem.setInputCloud (cloud);
  condrem.setKeepOrganized(true);
  // apply filter
  condrem.filter (*cloud);
  
 
}
void pclEigen::transform_verts(vector<Vector3d> &pts, const Eigen::Matrix4d &T, vector<Vector3d> &transformed_pts) const
{
  // transform points:
  for(int i=0;i<pts.size();++i)
  {
    
    transformed_pts.emplace_back(T.block(0,0,3,3)*pts[i]+T.col(3).head(3));
  }
}

void pclEigen::transform_pointcloud(PointCloud<PointXYZ>::Ptr cloud, const string &target_frame, const tf::TransformListener &tf_listener) const
{

  pcl_ros::transformPointCloud(target_frame,*cloud,*cloud,tf_listener);

}
void pclEigen::transform_pointcloud(PointCloud<PointXYZ>::Ptr cloud, const geometry_msgs::Transform &transform_mat) const
{
  tf::Transform tf_l;
  tf::transformMsgToTF(transform_mat,tf_l);
  pcl_ros::transformPointCloud(*cloud,*cloud,tf_l);

}
void pclEigen::find_table(PointCloud<PointXYZ>::Ptr in_cloud, PointCloud<PointXYZ>::Ptr table_cloud, PointCloud<PointXYZ>::Ptr outliers) const
{
  
  pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
  // Create the segmentation object
  pcl::SACSegmentation<pcl::PointXYZ> seg;
  // Optional
  seg.setOptimizeCoefficients (true);
  // Mandatory
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setDistanceThreshold (0.01);

  seg.setInputCloud (in_cloud);
  seg.segment (*inliers, *coefficients);

  if (inliers->indices.size () == 0)
  {
    PCL_ERROR ("Could not estimate a planar model for the given dataset.");
    
  }
  // copy only the inliers to the table_cloud:
  pcl::ExtractIndices<pcl::PointXYZ> extract;
  // Extract the inliers
  extract.setInputCloud (in_cloud);
  extract.setIndices (inliers);
  extract.setNegative (false);
  extract.filter (*table_cloud);
  extract.setNegative(true);
  extract.filter(*outliers);

}
void pclEigen::find_cluster(PointCloud<PointXYZ>::Ptr in_cloud, PointCloud<PointXYZ>::Ptr obj_cloud, PointCloud<PointXYZ>::Ptr outliers) const
{
  
  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
  tree->setInputCloud (in_cloud);

  std::vector<pcl::PointIndices> cluster_indices;
  pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
  ec.setClusterTolerance (0.02); // 2cm
  ec.setMinClusterSize (100);
  ec.setMaxClusterSize (25000);
  ec.setSearchMethod (tree);
  ec.setInputCloud (in_cloud);
  ec.extract (cluster_indices);

  // extract the largest cluster:
  // TODO return all clusters:
  int j = 0;
  int largest=0;
  int l_idx=0;
  for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
  {
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);
    for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
    {
      cloud_cluster->points.push_back (in_cloud->points[*pit]); //*
    }
    cloud_cluster->width = cloud_cluster->points.size ();
    cloud_cluster->height = 1;
    cloud_cluster->is_dense = true;
    
    // this is a bad way to find the largest cluster, fix it later!!
    if(cloud_cluster->points.size()>largest)
    {
      *obj_cloud=*cloud_cluster;
      largest=cloud_cluster->points.size();
    }
    cerr<<largest<<endl;

  }

  
  //TODO: return outliers
  
}

void pclEigen::generate_octree(PointCloud<PointXYZ>::Ptr in_cloud, pcl::octree::OctreePointCloud<PointXYZ> &out_tree) 
{
  out_tree.setInputCloud(in_cloud);
  out_tree.addPointsFromInputCloud();
}

void pclEigen::loadPCLOctree(PointCloud<PointXYZ>::Ptr env_cloud, octomap::OcTree* tree)
{
  float resolution = 0.1f;

  pcl::octree::OctreePointCloud<pcl::PointXYZ> octree (resolution);

  generate_octree(env_cloud,octree);
  
  // convert to normal octree:

  //octomap::OcTree* tree = new octomap::OcTree(resolution);
  octomap::point3d camera_pose(0.0,0.0,0.0);

  octomap::point3d octomap_3d_point;
  octomap::Pointcloud octomap_pointcloud;
  //Reading from pcl point cloud and saving it into octomap point cloud
  for(unsigned int i =0; i < env_cloud->points.size(); i++)
  {
    octomap_3d_point(0) = env_cloud->points[i].x;
    octomap_3d_point(1) = env_cloud->points[i].y;
    octomap_3d_point(2) = env_cloud->points[i].z;
    octomap_pointcloud.push_back(octomap_3d_point);
  }
  
  tree->insertPointCloud(octomap_pointcloud,camera_pose);
  tree->updateInnerOccupancy();
  
}

void pclEigen::loadBinvoxOctree(const string &bvox_file, octomap::OcTree* tree) const
{
  // This function is reproduced from binbox2bt from octomap source code
  /*!
   * Header from binvox2bt.cpp
   * Read files generated by Patrick Min's 3D mesh voxelizer
   * ("binvox", available at: http://www.cs.princeton.edu/~min/binvox/)
   * and convert the voxel meshes to a single bonsai tree file.
   * This file is based on code from http://www.cs.princeton.edu/~min/binvox/read_binvox.cc
   *
   * @author S. Osswald, University of Freiburg, Copyright (C) 2010.
   * License: New BSD License
   */
  using namespace octomap;
  int version;               // binvox file format version (should be 1)
  int depth, height, width;  // dimensions of the voxel grid
  int size;                  // number of grid cells (height * width * depth)
  float tx, ty, tz;          // Translation
  float scale;               // Scaling factor
  bool mark_free = false;    // Mark free cells (false = cells remain "unknown")    
  bool rotate = false;       // Fix orientation of webots-exported files
  bool show_help = false;
  string output_filename;
  double minX = 0.0;
  double minY = 0.0;
  double minZ = 0.0;
  double maxX = 0.0;
  double maxY = 0.0;
  double maxZ = 0.0;
  bool applyBBX = false;
  bool applyOffset = false;
  octomap::point3d offset(0.0, 0.0, 0.0);

    
    


  // Open input file
  ifstream *input = new ifstream(bvox_file, ios::in | ios::binary);    
  if(!input->good())
  {
    cerr << "Error: Could not open input file " << bvox_file << "!" << endl;
    exit(1);
  }
  else
  {
    cout << "Reading binvox file " << bvox_file << "." << endl;
  }

  // read header
  string line;
  *input >> line;    // #binvox
  if (line.compare("#binvox") != 0) {
    cout << "Error: first line reads [" << line << "] instead of [#binvox]" << endl;
    delete input;
    exit(1);
  }
  *input >> version;
  cout << "reading binvox version " << version << endl;
  
  depth = -1;
  int done = 0;
  while(input->good() && !done) {
    *input >> line;
    if (line.compare("data") == 0) done = 1;
    else if (line.compare("dim") == 0) {
      *input >> depth >> height >> width;
    }
    else if (line.compare("translate") == 0) {
      *input >> tx >> ty >> tz;
    }
    else if (line.compare("scale") == 0) {
      *input >> scale;
    }
    else {
      cout << "    unrecognized keyword [" << line << "], skipping" << endl;
      char c;
      do {    // skip until end of line
        c = input->get();
      } while(input->good() && (c != '\n'));
      
    }
  }
  if (!done) {
    cout << "    error reading header" << endl;
    exit(1);
  }
  if (depth == -1) {
    cout << "    missing dimensions in header" << endl;
    exit(1);
  }
  
  size = width * height * depth;
  int maxSide = std::max(std::max(width, height), depth);
  double res = double(scale)/double(maxSide);
  
  if(!tree) {
    cout << "Generating octree with leaf size " << res << endl << endl;
    tree = new octomap::OcTree(res);
  }
  
  if (applyBBX){
    cout << "Bounding box for Octree: [" << minX << ","<< minY << "," << minZ << " - "
         << maxX << ","<< maxY << "," << maxZ << "]\n";
    
  }
  if (applyOffset){
    std::cout << "Offset on final map: "<< offset << std::endl;
    
  }
                
  cout << "Read data: ";
  cout.flush();
  typedef unsigned char byte;
         
  // read voxel data
  byte value;
  byte count;
  int index = 0;
  int end_index = 0;
  unsigned nr_voxels = 0;
  unsigned nr_voxels_out = 0;
  
  input->unsetf(ios::skipws);    // need to read every byte now (!)
  *input >> value;    // read the linefeed char
  
  while((end_index < size) && input->good())
  {
    *input >> value >> count;
    
    if (input->good())
    {
      end_index = index + count;
      if (end_index > size) break;
      for(int i=index; i < end_index; i++) {
        // Output progress dots
        if(i % (size / 20) == 0) {
          cout << ".";            
          cout.flush();
        }
        // voxel index --> voxel coordinates 
        int y = i % width;
        int z = (i / width) % height;
        int x = i / (width * height);
                    
        // voxel coordinates --> world coordinates
        point3d endpoint((float) ((double) x*res + tx + 0.000001), 
                         (float) ((double) y*res + ty + 0.000001),
                         (float) ((double) z*res + tz + 0.000001));
        
        if(rotate) {
          endpoint.rotate_IP(M_PI_2, 0.0, 0.0);
        }
        if (applyOffset)
          endpoint += offset;
        
        if (!applyBBX  || (endpoint(0) <= maxX && endpoint(0) >= minX
                           && endpoint(1) <= maxY && endpoint(1) >= minY
                           && endpoint(2) <= maxZ && endpoint(2) >= minZ)){
          
          // mark cell in octree as free or occupied
          if(mark_free || value == 1) {
            tree->updateNode(endpoint, value == 1, true);
          }
        } else{
          nr_voxels_out ++;
        }
      }
      
      if (value) nr_voxels += count;
      index = end_index;
    }    // if file still ok
    
  }    // while
  
  cout << endl << endl;
  
  input->close();
  cout << "    read " << nr_voxels << " voxels, skipped "<<nr_voxels_out << " (out of bounding box)\n\n";

    
  // prune octree
  cout << "Pruning octree" << endl << endl;
  tree->updateInnerOccupancy();
  tree->prune();
  
}


void pclEigen::get_marker_octree(octomap::OcTree* m_octree, visualization_msgs::MarkerArray &occupiedNodesVis,string frame_id)
{
  double m_treeDepth = m_octree->getTreeDepth();
  double m_maxTreeDepth = m_treeDepth;
  double m_occupancyMinZ(-std::numeric_limits<double>::max());
  double m_occupancyMaxZ(std::numeric_limits<double>::max());
  double m_colorFactor=0.8;
  occupiedNodesVis.markers.resize(m_treeDepth+1);
  
  std_msgs::ColorRGBA m_color;

  m_color.r = 0.0;
  m_color.g = 0.0;
  m_color.b = 1.0;
  m_color.a = 0.8;
  vector<double> size_arr(m_treeDepth+1,0.0);
  // now, traverse all leafs in the tree:
  for (octomap::OcTree::iterator it = m_octree->begin(m_maxTreeDepth),
         end = m_octree->end(); it != end; ++it)
  {

    if (m_octree->isNodeOccupied(*it))
    {
      double z = it.getZ();
      if (z > m_occupancyMinZ && z < m_occupancyMaxZ)
      {
        double size = it.getSize();
        double x = it.getX();
        double y = it.getY();
        //create marker:
        unsigned idx = it.getDepth();
        assert(idx < occupiedNodesVis.markers.size());

        geometry_msgs::Point cubeCenter;
        cubeCenter.x = x;
        cubeCenter.y = y;
        cubeCenter.z = z;
        
        occupiedNodesVis.markers[idx].points.push_back(cubeCenter);
        size_arr[idx]=size;
        double minX, minY, minZ, maxX, maxY, maxZ;
        m_octree->getMetricMin(minX, minY, minZ);
        m_octree->getMetricMax(maxX, maxY, maxZ);

        double h = (1.0 - std::min(std::max((cubeCenter.z-minZ)/ (maxZ - minZ), 0.0), 1.0)) *m_colorFactor;
        //occupiedNodesVis.markers[idx].colors.push_back(heightMapColor(h));
        
        //std_msgs::ColorRGBA _color; _color.r = (r / 255.); _color.g = (g / 255.); _color.b = (b / 255.); _color.a = 1.0; // TODO/EVALUATE: potentially use occupancy as measure for alpha channel?
        //occupiedNodesVis.markers[idx].colors.push_back(_color);
      }
    }
  }

  for (unsigned i= 0; i < occupiedNodesVis.markers.size(); ++i)
  {
    
    double size = m_octree->getNodeSize(i);

    occupiedNodesVis.markers[i].header.frame_id = frame_id;
    occupiedNodesVis.markers[i].header.stamp = ros::Time();
    occupiedNodesVis.markers[i].ns = frame_id;
    occupiedNodesVis.markers[i].id = i;
    occupiedNodesVis.markers[i].type = visualization_msgs::Marker::CUBE_LIST;
    occupiedNodesVis.markers[i].scale.x = size_arr[i];
    occupiedNodesVis.markers[i].scale.y = size_arr[i];
    occupiedNodesVis.markers[i].scale.z = size_arr[i];
    occupiedNodesVis.markers[i].color = m_color;
    
    if (occupiedNodesVis.markers[i].points.size() > 0)
      occupiedNodesVis.markers[i].action = visualization_msgs::Marker::ADD;
    else
      occupiedNodesVis.markers[i].action = visualization_msgs::Marker::DELETE;
  }

}
}
