#include "collision_checker/convex_collision_checker.h"

collisionChecker::collisionChecker()
{
  _pcl_eigen.init();
}

void collisionChecker::create_publisher(ros::NodeHandle nh)
{
  pub=nh.advertise<pcl::PointCloud<PointXYZRGB>>("/collision_checker/robot_links",1);
}
void collisionChecker::load_robot_meshes(vector<string> file_names)
{ 

  for(int i=0;i<file_names.size();++i)
    {
#ifdef DEBUG
      cerr<<"robot link: "<<i<<" file: "<<file_names[i]<<endl;
#endif
      vector<Vector3d> mesh;
      _pcl_eigen.loadPLY(file_names[i],mesh);
      _robot_links.push_back(mesh);
    }
  
}

void collisionChecker::load_world_from_file(string file_name)
{
  PointCloud<PointXYZ>::Ptr temp_cloud(new PointCloud<PointXYZ>);
  _env_cloud=temp_cloud;
  _pcl_eigen.loadPCD(file_name,_env_cloud);
}

void collisionChecker::compute_convex_decomp(float vsize)
{
  _objects.clear();
  _pcl_eigen.convex_decomp(_env_cloud,_objects,vsize);
}

obj_t collisionChecker::loadMeshModel(vector<Vector3d> points,vector<double> pose)
{
  vector<Vector3d> vertices;

  Vector3d centre;
  for(int i=0;i<points.size();++i)
    {
      centre+=points[i];
    }
  
  centre=centre/double(points.size());

  //if(pose[0]==0.0 && pose[1]==0.0 && pose[2]==0.0)
    {
      centre[0]=0.0;
      centre[1]=0.0;
      centre[2]=0.0;
    }
  for(int i=0;i<points.size();++i)
    {
      vertices.push_back(points[i]-centre);
    }
  // We make the pose as the centre if its an environment object
  // Robot meshes have centre to be zero and so only the pose from tf will be used.
  ccd_vec3_t pos={.v={float(pose[0]+centre[0]),float(pose[1]+centre[1]),float(pose[2]+centre[2])}};
  // Assuming normalized quaternion:
  ccd_quat_t quat={.q={float(pose[3]),float(pose[4]),float(pose[5]),float(pose[6])}};

  obj_t obj={.pos=pos,.quat=quat,.vertices=vertices};
  
  return obj;
}
// TODO : wrap inside a class
void support_convex(const void *_obj, const ccd_vec3_t *dir_, ccd_vec3_t *vec)
{
  obj_t *obj=(obj_t *)_obj;
  
  ccd_vec3_t dir, p;
  ccd_real_t maxdot, dot;
  int i;

  //const auto& center = c->convex->center;
  ccd_quat_t qinv;
  
  ccdVec3Copy(&dir, dir_);
  ccdQuatInvert2(&qinv, &obj->quat);
  ccdQuatRotVec(&dir, &qinv);

  maxdot = -CCD_REAL_MAX;
 

  for(i = 0; i < obj->vertices.size(); ++i)
  {
    ccdVec3Set(&p, obj->vertices[i][0], obj->vertices[i][1] ,obj->vertices[i][2]);
    dot = ccdVec3Dot(&dir, &p);

    if(dot > maxdot)
    {
      ccdVec3Set(vec, obj->vertices[i][0], obj->vertices[i][1] ,obj->vertices[i][2]);
      maxdot = dot;
    }

  }

  // transform support vertex
  ccdQuatRotVec(vec, &obj->quat);
  ccdVec3Add(vec, &obj->pos);
}

collision_data collisionChecker::signed_distance(obj_t* obj1, obj_t* obj2)
{
  CCD_INIT(&ccd); // initialize ccd_t struct
  ccd.support1       = support_convex; // support function for first object
  ccd.support2       = support_convex; // support function for second object
  ccd.max_iterations = 100;     // maximal number of iterations
  ccd.epa_tolerance  = 0.001;  // maximal tolerance for EPA part

  ccd_real_t depth;
  ccd_vec3_t dir, point_pos;
  #ifdef DEBUG_ALL
  std::cout << "line 112 libs/ccd" << std::endl; 
  cerr<<"Performing GJK, if segfaulting next, uninstall libccd* using apt-get and compile from source"<<endl;
  #endif

  int intersect = ccdGJKPenetration(obj1, obj2, &ccd, &depth, &dir, &point_pos);
  //int intersect = ccdGJKIntersect(&obj1, &obj2, &ccd);
  #ifdef DEBUG_ALL
  cerr<<"Performed GJK"<<endl;
  #endif

  
  bool collision=false;
  collision_data c_dat;
  if(intersect==0)
    {
       #ifdef DEBUG_ALL
      cerr<<"GJK: In collision"<<endl;
       #endif

      collision=true;
      c_dat.collision=collision;
      c_dat.point=Vector3d(point_pos.v[0],point_pos.v[1],point_pos.v[2]);
      c_dat.dir=Vector3d(dir.v[0],dir.v[1],dir.v[2]);
      c_dat.distance=depth;
       #ifdef DEBUG_ALL
      cerr<<"GJK: In collision, storing data"<<endl;
       #endif
      
    }
  else// if not in collision:
    {
      #ifdef DEBUG_ALL
      cerr<<"GJK: Not in collision"<<endl;
       #endif

      ccd_simplex_t simplex;
      #ifdef DEBUG_ALL
      cerr<<"GJK: computing penetration depth"<<endl;
       #endif

      __ccdGJK(obj1, obj2, &ccd, &simplex);
      #ifdef DEBUG_ALL
      cerr<<"GJK: called __ccdGJK()"<<endl;
       #endif
      
      depth=_ccdDist(obj1,obj2,&ccd,&simplex,&dir,&point_pos);
      #ifdef DEBUG_ALL
      cerr<<"GJK: computed penetration depth"<<endl;
       #endif

      collision=false;
      c_dat.collision=collision;
      c_dat.point=Vector3d(point_pos.v[0],point_pos.v[1],point_pos.v[2]);
      #ifdef DEBUG_ALL 
      cerr<<"GJK: storing point"<<endl;
       #endif

      c_dat.dir=Vector3d(dir.v[0],dir.v[1],dir.v[2]);
      c_dat.distance=-depth;
      
    }
  #ifdef DEBUG_ALL
   cerr<<"Got c_dat from GJK"<<endl;
  #endif
  
  return c_dat;
}
vector<collision_data> collisionChecker::compute_signed_distance(obj_t* robot_link, vector<obj_t>& env_objects)
{
  vector<collision_data> collision_arr;
  #ifdef DEBUG_ALL
  cerr<<"compute_signed_distance(): "<<env_objects.size()<<endl;
  #endif
  for(int i=0;i<env_objects.size();++i)
    {
      obj_t* temp_env=&env_objects[i];
      collision_data c_dat;
      c_dat=signed_distance(robot_link,temp_env);
      //if(c_dat.collision)
      collision_arr.push_back(c_dat);
      
      
    }
  return collision_arr;
}
void collisionChecker::create_robot_coll_objects()
{
  _robot_coll_objects.resize(_robot_links.size());
  
  for(int i=0;i<_robot_links.size();++i)
    {
      #ifdef DEBUG
      cerr<<"robot link: "<<i<<endl;
      #endif

      obj_t r_mesh=loadMeshModel(_robot_links[i],_link_poses[i]);
      #ifdef DEBUG
      cerr<<"storing robot link mesh model: "<<i<<endl;
      #endif

      _robot_coll_objects[i]=r_mesh;
      #ifdef DEBUG
      cerr<<"stored robot link mesh model: "<<i<<endl;
      #endif
    }
}

void collisionChecker::update_env_coll_objects(vector<double> pose)
{

  #ifdef DEBUG
  cerr<<"Total objects in environment: "<<_objects.size()<<endl;
  #endif
  //cerr<<"Total objects in environment: "<<_objects.size()<<endl;
  _env_coll_objects.clear();  
  for(int i=0;i<_objects.size();++i)
    {
      if(_objects[i].size()>min_env_size)
	{
	  obj_t env_mesh=loadMeshModel(_objects[i],pose);
	  _env_coll_objects.push_back(env_mesh);
	}
    }
  cerr<<"Total collision objects: "<<_env_coll_objects.size()<<endl;
  publish_env_objects(-1,pub_);
  
}
void collisionChecker::update_env_pose(vector<double> pose)
{

  #ifdef DEBUG
  cerr<<"Total objects in environment: "<<_objects.size()<<endl;
  #endif
  //cerr<<"Total objects in environment: "<<_objects.size()<<endl;
  for(int i=0;i<_env_coll_objects.size();++i)
    {
      _env_coll_objects[i].pos.v[0]=pose[0];
      _env_coll_objects[i].pos.v[1]=pose[1];
      _env_coll_objects[i].pos.v[2]=pose[2];
      _env_coll_objects[i].quat.q[0]=pose[3];
      _env_coll_objects[i].quat.q[1]=pose[4];
      _env_coll_objects[i].quat.q[2]=pose[5];
      _env_coll_objects[i].quat.q[3]=pose[6];
    }
  //cerr<<"Total collision objects: "<<_env_coll_objects.size()<<endl;
  //publish_env_objects(-1,pub_);
  
}

void collisionChecker::create_env_coll_objects()
{

  #ifdef DEBUG
  cerr<<"Total objects in environment: "<<_objects.size()<<endl;
  #endif
  cerr<<"Total objects in environment: "<<_objects.size()<<endl;
  
  // initialize pose to zero since we obtain the object pose from HACD centroid computation:
  vector<double> pose={0.0,0.0,0.0,0.0,0.0,0.0,1.0};
  
  _env_coll_objects.clear();  
  for(int i=0;i<_objects.size();++i)
    {
      if(_objects[i].size()>min_env_size)
	{
	  obj_t env_mesh=loadMeshModel(_objects[i],pose);
	  _env_coll_objects.push_back(env_mesh);
	}
    }
    cerr<<"Total collision objects: "<<_env_coll_objects.size()<<endl;

}

void collisionChecker::update_robot_state(vector<vector<double>> poses) 
{
  // Clearing previous data:
  _link_poses.clear();
  
  for(int i=0;i<poses.size();++i)
    {
      _link_poses.push_back(poses[i]);

      // Update pose in robot collision objects
      _robot_coll_objects[i].pos.v[0]=poses[i][0];
      _robot_coll_objects[i].pos.v[1]=poses[i][1];
      _robot_coll_objects[i].pos.v[2]=poses[i][2];
      _robot_coll_objects[i].quat.q[0]=poses[i][3];
      _robot_coll_objects[i].quat.q[1]=poses[i][4];
      _robot_coll_objects[i].quat.q[2]=poses[i][5];
      _robot_coll_objects[i].quat.q[3]=poses[i][6];
    }
  
  
}
void collisionChecker::update_robot_state(vector<vector<double>> poses,vector<int> links_idx) 
{
  // Clearing previous data:
  //_link_poses.clear();
  int idx;
  for(int i=0;i<poses.size();++i)
  {
    idx=links_idx[i];
    _link_poses[idx]=poses[i];
                
    // Update pose in robot collision objects
    _robot_coll_objects[idx].pos.v[0]=poses[i][0];
    _robot_coll_objects[idx].pos.v[1]=poses[i][1];
    _robot_coll_objects[idx].pos.v[2]=poses[i][2];
    _robot_coll_objects[idx].quat.q[0]=poses[i][3];
    _robot_coll_objects[idx].quat.q[1]=poses[i][4];
    _robot_coll_objects[idx].quat.q[2]=poses[i][5];
    _robot_coll_objects[idx].quat.q[3]=poses[i][6];
  }
  
  
}

/*
vector<obj_t>  collisionChecker::update_robot_state(vector<vector<double>> poses)
{
  // Clearing previous data:
  // _link_poses.clear();
  vector<obj_t> robot_coll_objects;
  
  for(int i=0;i<poses.size();++i)
    {
      //_link_poses.emplace_back(poses[i]);

      // Update pose in robot collision objects
      robot_coll_objects[i].pos.v[0]=poses[i][0];
      robot_coll_objects[i].pos.v[1]=poses[i][1];
      robot_coll_objects[i].pos.v[2]=poses[i][2];
      robot_coll_objects[i].quat.q[0]=poses[i][3];
      robot_coll_objects[i].quat.q[1]=poses[i][4];
      robot_coll_objects[i].quat.q[2]=poses[i][5];
      robot_coll_objects[i].quat.q[3]=poses[i][6];
    }
  return robot_coll_objects;
}
*/

void collisionChecker::initialize_robot_variables()
{
  // link poses are initialized to robot home position:
  
  vector<double> pose={0.0,0.0,0.0,0.0,0.0,0.0,1.0};
  for(int i=0;i<_DOF;++i)
    {
      _link_poses.push_back(pose);
    }
  
}


void collisionChecker::initialize_robot_state(int DOF,vector<string>& robot_file_names)
{
  _pcl_eigen.init();
  // Initialize variables:
  _DOF=DOF;
  
  // Load robot meshes and create collision objects of robot:
  #ifdef DEBUG
  cerr<<"Initializing collision checker"<<endl;
  #endif
  #ifdef DEBUG
  cerr<<"***Loading robot link meshes***"<<endl;
  #endif
  load_robot_meshes(robot_file_names);

  // Initialize robot variables to default values
  initialize_robot_variables();

  #ifdef DEBUG
  cerr<<"***Creating robot link collision objects***"<<endl;
  #endif
  create_robot_coll_objects();
  
}

// Pointcloud subscriber:
void collisionChecker::pcl_callback(sensor_msgs::PointCloud2 input_pcl)
{

  PointCloud<PointXYZ>::Ptr t_cloud(new PointCloud<PointXYZ>);
  // Update env_cloud:
  pcl::fromROSMsg(input_pcl,*t_cloud);
  _t_cloud=t_cloud;
}
void collisionChecker::initialize_robot_env_state(int DOF,vector<string>& robot_file_names,string pcd_file_name)
{
  _pcl_eigen.init();
  // Initialize variables:
  _DOF=DOF;
  
  // Load robot meshes and create collision objects of robot:
  #ifdef DEBUG
  cerr<<"Initializing collision checker"<<endl;
  #endif
  #ifdef DEBUG
  cerr<<"***Loading robot link meshes***"<<endl;
  #endif
  load_robot_meshes(robot_file_names);

  // Initialize robot variables to default values
  initialize_robot_variables();

  #ifdef DEBUG
  cerr<<"***Creating robot link collision objects***"<<endl;
  #endif
  create_robot_coll_objects();
  

  // Load environment and perform convex decomposition
  #ifdef DEBUG
  cerr<<"***Loading environment from pcd file***" <<endl;
  #endif

  load_world_from_file(pcd_file_name);


  // Downsample environment cloud:
  float vsize=0.015;
  #ifdef DEBUG
  cerr<<"Downsampling cloud using voxel grid filter with leaf size: "<<vsize<<endl;
  #endif
  _pcl_eigen.downsample_cloud(_env_cloud,vsize);
  
  #ifdef DEBUG
  cerr<<"***Performing HACD***"<<endl;
  #endif
  compute_convex_decomp(0.01);
  #ifdef DEBUG
  cerr<<"HACD computation took: "<<endl;
  #endif

  #ifdef DEBUG
  cerr<<"***Creating environment collision objects***"<<endl;
  #endif
  create_env_coll_objects();
  
  #ifdef DEBUG
  cerr<<"**** Phew! Successfully initialized collision checker****"<<endl;
  #endif
  cerr<<"**** Phew! Successfully initialized collision checker****"<<endl;

  
}

void collisionChecker::initialize_robot_env_state(int DOF,vector<string>& robot_file_names, PointCloud<PointXYZ>::Ptr t_cloud)
{
  // Initialize variables:
  _DOF=DOF;
  
  // Load robot meshes and create collision objects of robot:
  #ifdef DEBUG
  cerr<<"Initializing collision checker"<<endl;
  #endif
  #ifdef DEBUG
  cerr<<"***Loading robot link meshes***"<<endl;
  #endif
  load_robot_meshes(robot_file_names);

  // Initialize robot variables to default values
  initialize_robot_variables();

  #ifdef DEBUG
  cerr<<"***Creating robot link collision objects***"<<endl;
  #endif
  create_robot_coll_objects();
  

  // Load environment and perform convex decomposition
  #ifdef DEBUG
  cerr<<"***Loading environment***" <<endl;
  #endif
  _env_cloud=t_cloud;
  


  // Downsample environment cloud:
  float vsize=0.015;
  #ifdef DEBUG
  cerr<<"Downsampling cloud using voxel grid filter with leaf size: "<<vsize<<endl;
  #endif
  _pcl_eigen.downsample_cloud(_env_cloud,vsize);
  
  #ifdef DEBUG
  cerr<<"***Performing HACD***"<<endl;
  #endif
  compute_convex_decomp(0.01);

  #ifdef DEBUG
  cerr<<"HACD computation took: "<<endl;
  #endif

  #ifdef DEBUG
  cerr<<"***Creating environment collision objects***"<<endl;
  #endif
  create_env_coll_objects();
  
  #ifdef DEBUG
  cerr<<"**** Phew! Successfully initialized collision checker****"<<endl;
  #endif
  cerr<<"**** Phew! Successfully initialized collision checker****"<<endl;

  
}
vector<collision_data> collisionChecker::check_pt_collisions(vector<vector<double>> points, vector<int> link_idx)
{
  // Create objects for points:
  vector<double> pose;
  pose.resize(7,0.0);
  pose[6]=1.0;
  collision_data c_dat;
  vector<collision_data> coll_dat;  
  for(int i=0;i<points.size();++i)
  {
    Vector3d pt;
    pt[0]=points[i][0];
    pt[1]=points[i][1];
    pt[2]=points[i][2];
    vector<Vector3d> pts;

    pts.push_back(pt);
    
    obj_t pt_mesh=loadMeshModel(pts,pose);
    c_dat=signed_distance(&pt_mesh,&_robot_coll_objects[link_idx[i]]);
    coll_dat.push_back(c_dat);
  }
  return coll_dat;
   
}
vector<vector<collision_data>> collisionChecker::check_collisions(vector<vector<double>> r_poses) 
{
  
  #ifdef DEBUG
  cerr<<"****Running Collision Checker****"<<endl;
  #endif
  
  // Update robot link poses:
  #ifdef DEBUG
  cerr<<"***Updating robot link poses***"<<endl;
  #endif
  update_robot_state(r_poses);
  //publish_robot_objects();
  // initializing collision data for all links
  vector<vector<collision_data>> _robot_c_data;

  #ifdef DEBUG_COLL
  cerr<<"***Computing signed distances:***"<<endl;
  cerr<<"Env size: "<<_env_coll_objects.size()<<endl;
  #endif
  vector<collision_data> c_dat;  
  // check collisions:
  for(int i=0;i<r_poses.size();++i)
    {
      c_dat.clear();
      #ifdef DEBUG
      cerr<<"robot link: "<<i<<endl;
      #endif
      c_dat=compute_signed_distance(&_robot_coll_objects[i],_env_coll_objects);
      #ifdef DEBUG
      cerr<<"Collisions found: "<<c_dat.size()<<endl;
      #endif
      
      _robot_c_data.push_back(c_dat);
    }

  return _robot_c_data;
}

vector<vector<collision_data>> collisionChecker::check_collisions(vector<vector<double>> r_poses,vector<int> link_idx) 
{
  
  #ifdef DEBUG
  cerr<<"****Running Collision Checker****"<<endl;
  #endif
  
  // Update robot link poses:
  #ifdef DEBUG
  cerr<<"***Updating robot link poses***"<<endl;
  #endif
  update_robot_state(r_poses,link_idx);
  //publish_robot_objects();
  // initializing collision data for all links
  vector<vector<collision_data>> _robot_c_data;

  #ifdef DEBUG_COLL
  cerr<<"***Computing signed distances:***"<<endl;
  cerr<<"Env size: "<<_env_coll_objects.size()<<endl;
  #endif
  vector<collision_data> c_dat;  
  // check collisions:
  for(int i=0;i<link_idx.size();++i)
    {
      c_dat.clear();
      #ifdef DEBUG
      cerr<<"robot link: "<<i<<endl;
      #endif
      c_dat=compute_signed_distance(&_robot_coll_objects[link_idx[i]],_env_coll_objects);
      #ifdef DEBUG
      cerr<<"Collisions found: "<<c_dat.size()<<endl;
      #endif
      
      _robot_c_data.push_back(c_dat);
    }

  return _robot_c_data;
}

vector<vector<collision_data>> collisionChecker::check_self_collisions(vector<vector<double>> r_poses,vector<int> link_idx,vector<int> coll_idx) 
{
  
  #ifdef DEBUG
  cerr<<"****Running Collision Checker****"<<endl;
  #endif
  
  // Update robot link poses:
  #ifdef DEBUG
  cerr<<"***Updating robot link poses***"<<endl;
  #endif
  update_robot_state(r_poses,link_idx);
  //publish_robot_objects();
  // initializing collision data for all links
  vector<vector<collision_data>> _robot_c_data;

  #ifdef DEBUG_COLL
  cerr<<"***Computing signed distances:***"<<endl;
  cerr<<"Env size: "<<_env_coll_objects.size()<<endl;
  #endif
  vector<collision_data> c_dat_arr;
  collision_data c_dat;
  // check collisions:
  for(int i=0;i<link_idx.size();++i)
    {
      c_dat_arr.clear();
      #ifdef DEBUG
      cerr<<"robot link: "<<i<<endl;
      #endif
      for (int j=0;j<coll_idx.size();++j)
      {
        c_dat=signed_distance(&_robot_coll_objects[link_idx[i]],&_robot_coll_objects[coll_idx[j]]);
        c_dat_arr.push_back(c_dat);
      }
      //c_dat=compute_signed_distance(&_robot_coll_objects[link_idx[i]],_env_coll_objects);
      #ifdef DEBUG
      cerr<<"Collisions found: "<<c_dat_arr.size()<<endl;
      #endif
      
      _robot_c_data.push_back(c_dat_arr);
    }

  return _robot_c_data;
}


void collisionChecker::view_env_objects(int obj_number)
{
  // Start pcl viewer:
  visualization::PCLVisualizer  viewer ("3d Viewer");
  viewer.setBackgroundColor (0,0,0);


  // add objects:
  pcl::PointCloud<PointXYZRGB>::Ptr cloud_obj1(new PointCloud<PointXYZRGB>);

  float r=rand()%255, g = rand()%255, b = rand()%255;

  vector<Vector3d> obj=_objects[obj_number];

  for (int j=0; j <  obj.size() ; ++j) {

    PointXYZRGB newpt;
    newpt.x = obj[j][0];
    newpt.y = obj[j][1];
    newpt.z = obj[j][2];
    newpt.r = r;
    newpt.g = g;
    newpt.b = b;
    cloud_obj1->push_back(newpt);
  }  
  viewer.addPointCloud(cloud_obj1, (boost::format("cloud_1")).str());


  // view pcl:
  while (!viewer.wasStopped ())
  {
    viewer.spinOnce (100);
    boost::this_thread::sleep (boost::posix_time::microseconds (100000));
  }

}

void collisionChecker::publish_env_objects(int obj_number, int argc, char** argv)
{
  
  // Start pcl publisher:
  ros::init(argc,argv,"hacd_publisher");
  ros::NodeHandle nh;
  ros::Publisher pub=nh.advertise<pcl::PointCloud<PointXYZRGB>>("/hacd/objects",1);
  
  // add objects:
  pcl::PointCloud<PointXYZRGB>::Ptr cloud_obj1(new PointCloud<PointXYZRGB>);

  if(obj_number=-1)
    {
      for(int i=0;i<_objects.size();++i)
	{
	  float r=rand()%255, g = rand()%255, b = rand()%255;
	  
	  vector<Vector3d> obj=_objects[i];
          if(obj.size()>min_env_size)
          {
            for (int j=0; j <  obj.size() ; ++j)
            {
              
              PointXYZRGB newpt;
              newpt.x = obj[j][0];
              newpt.y = obj[j][1];
              newpt.z = obj[j][2];
              newpt.r = r;
              newpt.g = g;
              newpt.b = b;
              cloud_obj1->push_back(newpt);

            }
          }
	}
    }
  else
    {
      
      float r=rand()%255, g = rand()%255, b = rand()%255;

      vector<Vector3d> obj=_objects[obj_number];
      if(obj.size()>min_env_size)
      {
      for (int j=0; j <  obj.size() ; ++j) {

	PointXYZRGB newpt;
	newpt.x = obj[j][0];
	newpt.y = obj[j][1];
	newpt.z = obj[j][2];
	newpt.r = r;
	newpt.g = g;
	newpt.b = b;
	cloud_obj1->push_back(newpt);
      }
      }
    }
  

  // publish pcl:
  cloud_obj1->header.frame_id="palm_link";
  ros::Rate loop_rate(4);
  int i=0;
  while (nh.ok() && i<5)
  {
    cloud_obj1->header.stamp=ros::Time::now().toNSec();
    pub.publish(cloud_obj1);
    ros::spinOnce();
    loop_rate.sleep();
    i++;
  }

}
void collisionChecker::publish_env_objects(int obj_number, ros::Publisher &pub)
{
  
   // add objects:
  pcl::PointCloud<PointXYZRGB>::Ptr cloud_obj1(new PointCloud<PointXYZRGB>);
  if(obj_number=-1)
    {

      for(int i=0;i<_env_coll_objects.size();++i)
	{
	  float r=rand()%200, g = rand()%200, b = rand()%200;
	  
	  vector<Vector3d> obj=_env_coll_objects[i].vertices;
          Vector4f trans={_env_coll_objects[i].pos.v[0],
                          _env_coll_objects[i].pos.v[1],
                          _env_coll_objects[i].pos.v[2],1.0};
          Matrix3f m; m=Quaternionf(_env_coll_objects[i].quat.q[3],
                                    _env_coll_objects[i].quat.q[0], _env_coll_objects[i].quat.q[1],
                                    _env_coll_objects[i].quat.q[2]);
          Matrix4f T;
          T.setIdentity();
          T.block<3,3>(0,0)=m;
          T.rightCols<1>()=trans;
  
          for (int j=0; j <  obj.size() ; ++j) {
            Vector4f point={obj[j][0],obj[j][1],obj[j][2],1.0};
            point=T*point;
            PointXYZRGB newpt;
            newpt.x = point[0];
            newpt.y = point[1];
            newpt.z = point[2];
            newpt.r = r;
            newpt.g = g;
            newpt.b = b;
            cloud_obj1->push_back(newpt);
          }
    
        }
    }
  else
    {
      
      float r=rand()%255, g = rand()%255, b = rand()%255;

      vector<Vector3d> obj=_objects[obj_number];
      if(obj.size()>min_env_size)
      {
        for (int j=0; j <  obj.size() ; ++j)
        {

          PointXYZRGB newpt;
          newpt.x = obj[j][0];
          newpt.y = obj[j][1];
          newpt.z = obj[j][2];
          newpt.r = r;
          newpt.g = g;
          newpt.b = b;
          cloud_obj1->push_back(newpt);
        }
      }
    }
  

  // publish pcl:
  cloud_obj1->header.frame_id="palm_link";
  ros::Rate loop_rate(100);
  int i=0;
  while(ros::ok() && i<1)
  {
    //cloud_obj1->header.stamp=ros::Time::now().toNSec();
    pub.publish(cloud_obj1);
    loop_rate.sleep();
    //cerr<<"Publishing cloud! "<<cloud_obj1->size()<<endl;
    i++;
  }
}

void collisionChecker::publish_robot_objects()
{
  
  // add objects:
  pcl::PointCloud<PointXYZRGB>::Ptr cloud_obj1(new PointCloud<PointXYZRGB>);

  for(int i=0;i<_robot_coll_objects.size();++i)
  {
  
  float r=rand()%200, g = rand()%200, b = rand()%200;

  vector<Vector3d> obj=_robot_coll_objects[i].vertices;
  Vector4f trans={_robot_coll_objects[i].pos.v[0],_robot_coll_objects[i].pos.v[1],_robot_coll_objects[i].pos.v[2],1.0};
  Matrix3f m; m=Quaternionf(_robot_coll_objects[i].quat.q[3],_robot_coll_objects[i].quat.q[0],_robot_coll_objects[i].quat.q[1],_robot_coll_objects[i].quat.q[2]);
  Matrix4f T;
  T.setIdentity();
  T.block<3,3>(0,0)=m;
  T.rightCols<1>()=trans;
  
  for (int j=0; j <  obj.size() ; ++j) {
    Vector4f point={obj[j][0],obj[j][1],obj[j][2],1.0};
    point=T*point;
    PointXYZRGB newpt;
    newpt.x = point[0];
    newpt.y = point[1];
    newpt.z = point[2];
    newpt.r = r;
    newpt.g = g;
    newpt.b = b;
    cloud_obj1->push_back(newpt);
  }  
  
  }
  // publish pcl:
  cloud_obj1->header.frame_id="palm_link";
  //ros::Rate loop_rate(4);
  //bool pub_cloud=false;
  int i=0;

  while (ros::ok() && i<1)
  {
    //cloud_obj1->header.stamp=ros::Time::now().toNSec();
    pub_.publish(cloud_obj1);
    //ros::spinOnce();
    //loop_rate.sleep();
    i++;
    //pub_cloud=true;
  }

}



// Collision ros service:
bool collisionChecker::check_collisions(ll4ma_collision_wrapper::collision_checker_msg::Request &req,ll4ma_collision_wrapper::collision_checker_msg::Response &res)
{

  //Get robot_poses as vector from geometry_msgs:
  vector<double> link_pose;
  vector<vector<double>> robot_link_poses;
  for (int i=0;i<req.link_pose.size();++i)
    {
      link_pose.clear();
      link_pose.push_back(req.link_pose[i].position.x);
      link_pose.push_back(req.link_pose[i].position.y);
      link_pose.push_back(req.link_pose[i].position.z);

      link_pose.push_back(req.link_pose[i].orientation.x);
      link_pose.push_back(req.link_pose[i].orientation.y);
      link_pose.push_back(req.link_pose[i].orientation.z);
      link_pose.push_back(req.link_pose[i].orientation.w);
      
      robot_link_poses.push_back(link_pose);
    }
  //publish_robot_objects();
  vector<vector<collision_data>> c_data;
  c_data=check_collisions(robot_link_poses);



  for (int i=0; i<c_data.size();++i)
    {
      ll4ma_collision_wrapper::link_collision_data ros_link_data;
      ros_link_data.link_num=i;

      for(int j=0;j<c_data[i].size();++j)
	{
	  ll4ma_collision_wrapper::collision_data ros_col_data;
	  ros_col_data.collision=c_data[i][j].collision;

	  ros_col_data.point.push_back(c_data[i][j].point[0]);
	  ros_col_data.point.push_back(c_data[i][j].point[1]);
	  ros_col_data.point.push_back(c_data[i][j].point[2]);

	  ros_col_data.dir.push_back(c_data[i][j].dir[0]);
	  ros_col_data.dir.push_back(c_data[i][j].dir[1]);
	  ros_col_data.dir.push_back(c_data[i][j].dir[2]);
	  
	  ros_col_data.distance=c_data[i][j].distance;
	  
	  ros_link_data.c_data.push_back(ros_col_data);
	}
      res.link_data.push_back(ros_link_data);
    }
  


  return true;
}
// Collision ros service:
bool collisionChecker::debug_robot_viz(ll4ma_collision_wrapper::collision_checker_msg::Request &req,ll4ma_collision_wrapper::collision_checker_msg::Response &res)
{
  cerr<<"received request"<<endl;
  //Get robot_poses as vector from geometry_msgs:
  vector<double> link_pose;
  vector<vector<double>> robot_link_poses;
  for (int i=0;i<req.link_pose.size();++i)
    {
      link_pose.clear();
      link_pose.push_back(req.link_pose[i].position.x);
      link_pose.push_back(req.link_pose[i].position.y);
      link_pose.push_back(req.link_pose[i].position.z);

      link_pose.push_back(req.link_pose[i].orientation.x);
      link_pose.push_back(req.link_pose[i].orientation.y);
      link_pose.push_back(req.link_pose[i].orientation.z);
      link_pose.push_back(req.link_pose[i].orientation.w);
      
      robot_link_poses.push_back(link_pose);
    }


  update_robot_state(robot_link_poses);
  publish_robot_objects();


  return true;
}

// Update environment cloud 
// Call this everytime you want a new plan if the environment has changed
// TODO: Split robot initialization from env. initialization
bool collisionChecker::update_env_data(sensor_msgs::PointCloud2 &in_cloud, geometry_msgs::Transform &tf_l)
{
  PointCloud<PointXYZ>::Ptr t_cloud(new PointCloud<PointXYZ>);
  // Update env_cloud:
  pcl::fromROSMsg(in_cloud,*t_cloud);
  _t_cloud=t_cloud;

  while(_t_cloud->size()==0)
    {
      // wait for data
    }

  // get cloud from service tabletop segmenter:
  #ifdef DEBUG
  cerr<<"Received cloud from sensor "<<endl;
  #endif
  
  // remove nans:
  vector<int> ind;
  pcl::removeNaNFromPointCloud(*_t_cloud,*_t_cloud,ind);
  #ifdef DEBUG
  cerr<<"Removed Nans "<<endl;
  #endif

  // Transform pointcloud to robot base frame:
  string base_frame="base_link";
  _pcl_eigen.transform_pointcloud(_t_cloud,tf_l);
  #ifdef DEBUG
  cerr<<"Transformed PCL "<<endl;
  #endif

  // Filter pointcloud:
  // Remove points not in robot workspace:
  vector<float> min_={-1.2,-1.2,0.1};
  vector<float> max_={1.2,1.2,1.2};  
  _pcl_eigen.bound_cloud(_t_cloud,min_,max_);
  // Get new data from pointcloud topic:
  #ifdef DEBUG
  cerr<<"Copying cloud from pcl callback "<<endl;
  #endif
  PointCloud<PointXYZ>::Ptr temp_cloud(new PointCloud<PointXYZ>);
  copyPointCloud(*_t_cloud,*temp_cloud);
  #ifdef DEBUG
  cerr<<"t size: "<<temp_cloud->size()<<endl;
  #endif
  
  _env_cloud=temp_cloud;

  // Remove nans:

  cerr<<"Env size: "<<_env_cloud->size()<<endl;
  #ifdef DEBUG
  cerr<<"Env size: "<<_env_cloud->size()<<endl;
  #endif
  // Downsample environment cloud:
  float vsize=0.015;
  #ifdef DEBUG
  cerr<<"Downsampling cloud using voxel grid filter with leaf size: "<<vsize<<endl;
  #endif
  _pcl_eigen.downsample_cloud(_env_cloud,vsize);
  
  #ifdef DEBUG
  cerr<<"***Performing HACD***"<<endl;
  #endif

  compute_convex_decomp(0.01);

  // Visualize pointcloud:
  //publish_env_objects(-1,pub_);
  #ifdef DEBUG
  cerr<<"HACD computation took: "<<endl;
  #endif

  #ifdef DEBUG
  cerr<<"***Creating environment collision objects***"<<endl;
  #endif
  create_env_coll_objects();
  return true;
}
bool collisionChecker::update_env_cloud(ll4ma_collision_wrapper::update_env::Request &req,ll4ma_collision_wrapper::update_env::Response &res)
{
  // Return true
  //res.init_env=update_env_data();
  return true;
}

 bool collisionChecker::add_obj_cloud(ros::ServiceClient &client)
{
  // load cloud from file:
  _objects.clear();
  // get object cloud from segmentation service:
  //ll4ma_collision_wrapper::SegmentGraspObject seg_srv;

  PointCloud<PointXYZ>::Ptr t_cloud(new PointCloud<PointXYZ>);
  // Update env_cloud:
  //client.call(seg_srv);
  //pcl::fromROSMsg(seg_srv.response.obj.cloud,*t_cloud);
  string base_frame="base_link";

  _pcl_eigen.transform_pointcloud(t_cloud,base_frame,*tf_listen);
  
  _env_cloud=t_cloud;

    cerr<<"Env size: "<<_env_cloud->size()<<endl;
  #ifdef DEBUG
  cerr<<"Env size: "<<_env_cloud->size()<<endl;
  #endif
  // Downsample environment cloud:
  float vsize=0.001;
  #ifdef DEBUG
  cerr<<"Downsampling cloud using voxel grid filter with leaf size: "<<vsize<<endl;
  #endif
  _pcl_eigen.downsample_cloud(_env_cloud,vsize);
  
  #ifdef DEBUG
  cerr<<"***Performing HACD***"<<endl;
  #endif

  compute_convex_decomp(0.01);

  // Visualize pointcloud:
  //publish_env_objects(-1,pub_);
  
  #ifdef DEBUG
  cerr<<"HACD computation took: "<<endl;
  #endif

  #ifdef DEBUG
  cerr<<"***Creating environment collision objects***"<<endl;
  #endif
  create_env_coll_objects();
  
  // Return true

  return true;

}
bool collisionChecker::add_obj_cloud(string file_name)
{
  // load cloud from file:
  _objects.clear();
  
  //Load cloud from ply file
  PointCloud<PointXYZ>::Ptr temp_cloud(new PointCloud<PointXYZ>);
  _pcl_eigen.loadPLY(file_name,temp_cloud);
  _env_cloud=temp_cloud;

  cerr<<"Env size: "<<_env_cloud->size()<<endl;
  #ifdef DEBUG
  cerr<<"Env size: "<<_env_cloud->size()<<endl;
  #endif
  // Downsample environment cloud:
  float vsize=0.0005;
  #ifdef DEBUG
  cerr<<"Downsampling cloud using voxel grid filter with leaf size: "<<vsize<<endl;
  #endif
  _pcl_eigen.downsample_cloud(_env_cloud,vsize);
  
  #ifdef DEBUG
  cerr<<"***Performing HACD***"<<endl;
  #endif

  compute_convex_decomp(0.001);

  // Visualize pointcloud:
  //publish_env_objects(-1,pub_);
  
  #ifdef DEBUG
  cerr<<"HACD computation took: "<<endl;
  #endif

  #ifdef DEBUG
  cerr<<"***Creating environment collision objects***"<<endl;
  #endif
  create_env_coll_objects();
  
  // Return true

  return true;
}

bool collisionChecker::add_obj_cloud(string file_name,vector<double> pose)
{
  // load cloud from file:
  _objects.clear();
  
  //Load cloud from ply file
  PointCloud<PointXYZ>::Ptr temp_cloud(new PointCloud<PointXYZ>);
  _pcl_eigen.loadPLY(file_name,temp_cloud);
  _env_cloud=temp_cloud;
  //Transform point cloud to pose:
  
  
  cerr<<"Env size: "<<_env_cloud->size()<<endl;
  #ifdef DEBUG
  cerr<<"Env size: "<<_env_cloud->size()<<endl;
  #endif
  // Downsample environment cloud:
  float vsize=0.005;
  #ifdef DEBUG
  cerr<<"Downsampling cloud using voxel grid filter with leaf size: "<<vsize<<endl;
  #endif
  _pcl_eigen.downsample_cloud(_env_cloud,vsize);
  
  #ifdef DEBUG
  cerr<<"***Performing HACD***"<<endl;
  #endif

  compute_convex_decomp(0.01);

  
  #ifdef DEBUG
  cerr<<"HACD computation took: "<<endl;
  #endif

  #ifdef DEBUG
  cerr<<"***Creating environment collision objects***"<<endl;
  #endif
  create_env_coll_objects();
  // Visualize pointcloud:
  publish_env_objects(-1,pub_);
  
  // Return true

  return true;
}


// signed distance computation when objects are not in collision:
// Obtained from fcl source files: https://github.com/flexible-collision-library/fcl/blob/master/include/fcl/narrowphase/detail/convexity_based_algorithm/gjk_libccd-inl.h

ccd_real_t  collisionChecker::_ccdDist(const void *obj1, const void *obj2,const ccd_t *ccd_,ccd_simplex_t* simplex,   ccd_vec3_t* dir,ccd_vec3_t* p1)
{
  ccd_vec3_t p3;
  ccd_vec3_t *p2=&p3;
   unsigned long iterations;
  ccd_support_t last; // last support point
 // direction vector
  ccd_real_t dist, last_dist = CCD_REAL_MAX;

  for (iterations = 0UL; iterations < ccd_->max_iterations; ++iterations)
  {
    // get a next direction vector
    // we are trying to find out a point on the minkowski difference
    // that is nearest to the origin, so we obtain a point on the
    // simplex that is nearest and try to exapand the simplex towards
    // the origin
    //cerr<<"Initialized: "<<ccdSimplexSize(simplex)<<endl;
    if (ccdSimplexSize(simplex) == 1)
    {
      ccdVec3Copy(dir, &ccdSimplexPoint(simplex, 0)->v);
      dist = ccdVec3Len2(&ccdSimplexPoint(simplex, 0)->v);
      dist = CCD_SQRT(dist);
    }
    else if (ccdSimplexSize(simplex) == 2)
    {
      dist = ccdVec3PointSegmentDist2(ccd_vec3_origin,
                                      &ccdSimplexPoint(simplex, 0)->v,
                                      &ccdSimplexPoint(simplex, 1)->v,
                                      dir);
      dist = CCD_SQRT(dist);
    }
    else if(ccdSimplexSize(simplex) == 3)
    {
      dist = ccdVec3PointTriDist2(ccd_vec3_origin,
                                  &ccdSimplexPoint(simplex, 0)->v,
                                  &ccdSimplexPoint(simplex, 1)->v,
                                  &ccdSimplexPoint(simplex, 2)->v,
                                  dir);
      dist = CCD_SQRT(dist);
    }
    else
    {// ccdSimplexSize(&simplex) == 4
      dist = simplexReduceToTriangle(simplex, last_dist, dir);
    }

    // check whether we improved for at least a minimum tolerance
    if ((last_dist - dist) < ccd_->dist_tolerance)
    {
      if(p1) *p1 = last.v1;
      if(p2) *p2 = last.v2;
      return dist;
    }

    // point direction towards the origin
    ccdVec3Scale(dir, -CCD_ONE);
    ccdVec3Normalize(dir);

    // find out support point
    __ccdSupport(obj1, obj2, dir, ccd_, &last);

    // record last distance
    last_dist = dist;

    // check whether we improved for at least a minimum tolerance
    // this is here probably only for a degenerate cases when we got a
    // point that is already in the simplex
    dist = ccdVec3Len2(&last.v);
    dist = CCD_SQRT(dist);
    if (CCD_FABS(last_dist - dist) < ccd_->dist_tolerance)
    {
      if(p1) *p1 = last.v1;
      if(p2) *p2 = last.v2;
      return last_dist;
    }

    // add a point to simplex
    ccdSimplexAdd(simplex, &last);
  }

  return -CCD_REAL(1.);
}


int collisionChecker::__ccdGJK(const void *obj1, const void *obj2,
                    const ccd_t *ccd_1, ccd_simplex_t *simplex)
{
    unsigned long iterations;
    ccd_vec3_t dir; // direction vector
    ccd_support_t last; // last support point
    int do_simplex_res;

    // initialize simplex struct
    ccdSimplexInit(simplex);

    // get first direction
    ccd_1->first_dir(obj1, obj2, &dir);
    // get first support point
    __ccdSupport(obj1, obj2, &dir, ccd_1, &last);
    // and add this point to simplex as last one
    ccdSimplexAdd(simplex, &last);

    // set up direction vector to as (O - last) which is exactly -last
    ccdVec3Copy(&dir, &last.v);
    ccdVec3Scale(&dir, -CCD_ONE);
    #ifdef DEBUG_ALL
    cerr<<"__ccdGJK(): starting iterations"<<endl;
    #endif

    // start iterations
    for (iterations = 0UL; iterations < ccd_1->max_iterations; ++iterations) {
        // obtain support point
         __ccdSupport(obj1, obj2, &dir, ccd_1, &last);
         #ifdef DEBUG_ALL
         cerr<<"__ccdGJK(): called __ccdSupport()"<<endl;
         #endif

        // check if farthest point in Minkowski difference in direction dir
        // isn't somewhere before origin (the test on negative dot product)
        // - because if it is, objects are not intersecting at all.
        //cerr<<"dot"<<last.v.v[0]<<endl;
        if (ccdVec3Dot(&last.v, &dir) < CCD_ZERO){
            return -1; // intersection not found
        }
         #ifdef DEBUG_ALL
         cerr<<"__ccdGJK(): calling __ccdSimplexAdd()"<<endl;
         #endif

        // add last support vector to simplex
        ccdSimplexAdd(simplex, &last);
         #ifdef DEBUG_ALL
         cerr<<"__ccdGJK(): called __ccdSimplexAdd()"<<endl;
         #endif

        // if doSimplex returns 1 if objects intersect, -1 if objects don't
        // intersect and 0 if algorithm should continue
        do_simplex_res = doSimplex(simplex, &dir);
        if (do_simplex_res == 1){
            return 0; // intersection found
        }else if (do_simplex_res == -1){
            return -1; // intersection not found
        }

        if (ccdIsZero(ccdVec3Len2(&dir))){
            return -1; // intersection not found
        }
    }

    // intersection wasn't found
    return -1;
}
/*
int collisionChecker::__ccdGJK(const void *obj1, const void *obj2,
                    const ccd_t *ccd_1, ccd_simplex_t *simplex_) const
{
    unsigned long iterations;
    //ccd_vec3_t dir_=dir; // direction vector
    ccd_support_t last; // last support point
    int do_simplex_res;

    // initialize simplex struct
    ccdSimplexInit(simplex_);

    // get first direction
    ccd_1->first_dir(obj1, obj2, &dir_);
    // get first support point
    __ccdSupport(obj1, obj2, &dir_, ccd_1, &last);
    // and add this point to simplex as last one
    ccdSimplexAdd(simplex_, &last);

    // set up direction vector to as (O - last) which is exactly -last
    ccdVec3Copy(&dir_, &last.v);
    ccdVec3Scale(&dir_, -CCD_ONE);

    // start iterations
    for (iterations = 0UL; iterations < ccd_1->max_iterations; ++iterations) {
        // obtain support point
        __ccdSupport(obj1, obj2, &dir_, ccd_1, &last);

        // check if farthest point in Minkowski difference in direction dir
        // isn't somewhere before origin (the test on negative dot product)
        // - because if it is, objects are not intersecting at all.
        if (ccdVec3Dot(&last.v, &dir_) < CCD_ZERO){
            return -1; // intersection not found
        }

        // add last support vector to simplex
        ccdSimplexAdd(simplex_, &last);

        // if doSimplex returns 1 if objects intersect, -1 if objects don't
        // intersect and 0 if algorithm should continue
        do_simplex_res = doSimplex(&simplex, &dir);
        if (do_simplex_res == 1){
            return 0; // intersection found
        }else if (do_simplex_res == -1){
            return -1; // intersection not found
        }

        if (ccdIsZero(ccdVec3Len2(&dir))){
            return -1; // intersection not found
        }
    }
    simplex=*simplex_;
    // intersection wasn't found
    return -1;
}
*/

void collisionChecker::tripleCross(const ccd_vec3_t *a, const ccd_vec3_t *b,
                             const ccd_vec3_t *c, ccd_vec3_t *d)
{
    ccd_vec3_t e;
    ccdVec3Cross(&e, a, b);
    ccdVec3Cross(d, &e, c);
}


int collisionChecker::doSimplex2(ccd_simplex_t *simplex, ccd_vec3_t *dir)
{
    const ccd_support_t *A, *B;
    ccd_vec3_t AB, AO, tmp;
    ccd_real_t dot;

    // get last added as A
    A = ccdSimplexLast(simplex);
    // get the other point
    B = ccdSimplexPoint(simplex, 0);
    // compute AB oriented segment
    ccdVec3Sub2(&AB, &B->v, &A->v);
    // compute AO vector
    ccdVec3Copy(&AO, &A->v);
    ccdVec3Scale(&AO, -CCD_ONE);

    // dot product AB . AO
    dot = ccdVec3Dot(&AB, &AO);

    // check if origin doesn't lie on AB segment
    ccdVec3Cross(&tmp, &AB, &AO);
    if (ccdIsZero(ccdVec3Len2(&tmp)) && dot > CCD_ZERO){
        return 1;
    }

    // check if origin is in area where AB segment is
    if (ccdIsZero(dot) || dot < CCD_ZERO){
        // origin is in outside are of A
        ccdSimplexSet(simplex, 0, A);
        ccdSimplexSetSize(simplex, 1);
        ccdVec3Copy(dir, &AO);
    }else{
        // origin is in area where AB segment is

        // keep simplex untouched and set direction to
        // AB x AO x AB
        tripleCross(&AB, &AO, &AB, dir);
    }

    return 0;
}
/*
int collisionChecker::doSimplex2(ccd_simplex_t *simplex, ccd_vec3_t *dir) cons
{
    const ccd_support_t *A, *B;
    ccd_vec3_t AB, AO, tmp;
    ccd_real_t dot;

    // get last added as A
    A = ccdSimplexLast(simplex);
    // get the other point
    B = ccdSimplexPoint(simplex, 0);
    // compute AB oriented segment
    ccdVec3Sub2(&AB, &B->v, &A->v);
    // compute AO vector
    ccdVec3Copy(&AO, &A->v);
    ccdVec3Scale(&AO, -CCD_ONE);

    // dot product AB . AO
    dot = ccdVec3Dot(&AB, &AO);

    // check if origin doesn't lie on AB segment
    ccdVec3Cross(&tmp, &AB, &AO);
    if (ccdIsZero(ccdVec3Len2(&tmp)) && dot > CCD_ZERO){
        return 1;
    }

    // check if origin is in area where AB segment is
    if (ccdIsZero(dot) || dot < CCD_ZERO){
        // origin is in outside are of A
        ccdSimplexSet(simplex, 0, A);
        ccdSimplexSetSize(simplex, 1);
        ccdVec3Copy(dir, &AO);
    }else{
        // origin is in area where AB segment is

        // keep simplex untouched and set direction to
        // AB x AO x AB
        tripleCross(&AB, &AO, &AB, dir);
    }

    return 0;
}
*/
int collisionChecker::doSimplex3(ccd_simplex_t *simplex, ccd_vec3_t *dir)
{
    const ccd_support_t *A, *B, *C;
    ccd_vec3_t AO, AB, AC, ABC, tmp;
    ccd_real_t dot, dist;

    // get last added as A
    A = ccdSimplexLast(simplex);
    // get the other points
    B = ccdSimplexPoint(simplex, 1);
    C = ccdSimplexPoint(simplex, 0);

    // check touching contact
    dist = ccdVec3PointTriDist2(ccd_vec3_origin, &A->v, &B->v, &C->v, NULL);
    if (ccdIsZero(dist)){
        return 1;
    }

    // check if triangle is really triangle (has area > 0)
    // if not simplex can't be expanded and thus no itersection is found
    if (ccdVec3Eq(&A->v, &B->v) || ccdVec3Eq(&A->v, &C->v)){
        return -1;
    }

    // compute AO vector
    ccdVec3Copy(&AO, &A->v);
    ccdVec3Scale(&AO, -CCD_ONE);

    // compute AB and AC segments and ABC vector (perpendircular to triangle)
    ccdVec3Sub2(&AB, &B->v, &A->v);
    ccdVec3Sub2(&AC, &C->v, &A->v);
    ccdVec3Cross(&ABC, &AB, &AC);

    ccdVec3Cross(&tmp, &ABC, &AC);
    dot = ccdVec3Dot(&tmp, &AO);
    if (ccdIsZero(dot) || dot > CCD_ZERO){
        dot = ccdVec3Dot(&AC, &AO);
        if (ccdIsZero(dot) || dot > CCD_ZERO){
            // C is already in place
            ccdSimplexSet(simplex, 1, A);
            ccdSimplexSetSize(simplex, 2);
            tripleCross(&AC, &AO, &AC, dir);
        }else{
ccd_do_simplex3_45:
            dot = ccdVec3Dot(&AB, &AO);
            if (ccdIsZero(dot) || dot > CCD_ZERO){
                ccdSimplexSet(simplex, 0, B);
                ccdSimplexSet(simplex, 1, A);
                ccdSimplexSetSize(simplex, 2);
                tripleCross(&AB, &AO, &AB, dir);
            }else{
                ccdSimplexSet(simplex, 0, A);
                ccdSimplexSetSize(simplex, 1);
                ccdVec3Copy(dir, &AO);
            }
        }
    }else{
        ccdVec3Cross(&tmp, &AB, &ABC);
        dot = ccdVec3Dot(&tmp, &AO);
        if (ccdIsZero(dot) || dot > CCD_ZERO){
            goto ccd_do_simplex3_45;
        }else{
            dot = ccdVec3Dot(&ABC, &AO);
            if (ccdIsZero(dot) || dot > CCD_ZERO){
                ccdVec3Copy(dir, &ABC);
            }else{
                ccd_support_t Ctmp;
                ccdSupportCopy(&Ctmp, C);
                ccdSimplexSet(simplex, 0, B);
                ccdSimplexSet(simplex, 1, &Ctmp);

                ccdVec3Copy(dir, &ABC);
                ccdVec3Scale(dir, -CCD_ONE);
            }
        }
    }

    return 0;
}

int collisionChecker::doSimplex4(ccd_simplex_t *simplex, ccd_vec3_t *dir)
{
    const ccd_support_t *A, *B, *C, *D;
    ccd_vec3_t AO, AB, AC, AD, ABC, ACD, ADB;
    int B_on_ACD, C_on_ADB, D_on_ABC;
    int AB_O, AC_O, AD_O;
    ccd_real_t dist;

    // get last added as A
    A = ccdSimplexLast(simplex);
    // get the other points
    B = ccdSimplexPoint(simplex, 2);
    C = ccdSimplexPoint(simplex, 1);
    D = ccdSimplexPoint(simplex, 0);

    // check if tetrahedron is really tetrahedron (has volume > 0)
    // if it is not simplex can't be expanded and thus no intersection is
    // found
    dist = ccdVec3PointTriDist2(&A->v, &B->v, &C->v, &D->v, NULL);
    if (ccdIsZero(dist)){
        return -1;
    }

    // check if origin lies on some of tetrahedron's face - if so objects
    // intersect
    dist = ccdVec3PointTriDist2(ccd_vec3_origin, &A->v, &B->v, &C->v, NULL);
    if (ccdIsZero(dist))
        return 1;
    dist = ccdVec3PointTriDist2(ccd_vec3_origin, &A->v, &C->v, &D->v, NULL);
    if (ccdIsZero(dist))
        return 1;
    dist = ccdVec3PointTriDist2(ccd_vec3_origin, &A->v, &B->v, &D->v, NULL);
    if (ccdIsZero(dist))
        return 1;
    dist = ccdVec3PointTriDist2(ccd_vec3_origin, &B->v, &C->v, &D->v, NULL);
    if (ccdIsZero(dist))
        return 1;

    // compute AO, AB, AC, AD segments and ABC, ACD, ADB normal vectors
    ccdVec3Copy(&AO, &A->v);
    ccdVec3Scale(&AO, -CCD_ONE);
    ccdVec3Sub2(&AB, &B->v, &A->v);
    ccdVec3Sub2(&AC, &C->v, &A->v);
    ccdVec3Sub2(&AD, &D->v, &A->v);
    ccdVec3Cross(&ABC, &AB, &AC);
    ccdVec3Cross(&ACD, &AC, &AD);
    ccdVec3Cross(&ADB, &AD, &AB);

    // side (positive or negative) of B, C, D relative to planes ACD, ADB
    // and ABC respectively
    B_on_ACD = ccdSign(ccdVec3Dot(&ACD, &AB));
    C_on_ADB = ccdSign(ccdVec3Dot(&ADB, &AC));
    D_on_ABC = ccdSign(ccdVec3Dot(&ABC, &AD));

    // whether origin is on same side of ACD, ADB, ABC as B, C, D
    // respectively
    AB_O = ccdSign(ccdVec3Dot(&ACD, &AO)) == B_on_ACD;
    AC_O = ccdSign(ccdVec3Dot(&ADB, &AO)) == C_on_ADB;
    AD_O = ccdSign(ccdVec3Dot(&ABC, &AO)) == D_on_ABC;

    if (AB_O && AC_O && AD_O){
        // origin is in tetrahedron
        return 1;

    // rearrange simplex to triangle and call doSimplex3()
    }else if (!AB_O){
        // B is farthest from the origin among all of the tetrahedron's
        // points, so remove it from the list and go on with the triangle
        // case

        // D and C are in place
        ccdSimplexSet(simplex, 2, A);
        ccdSimplexSetSize(simplex, 3);
    }else if (!AC_O){
        // C is farthest
        ccdSimplexSet(simplex, 1, D);
        ccdSimplexSet(simplex, 0, B);
        ccdSimplexSet(simplex, 2, A);
        ccdSimplexSetSize(simplex, 3);
    }else{ // (!AD_O)
        ccdSimplexSet(simplex, 0, C);
        ccdSimplexSet(simplex, 1, B);
        ccdSimplexSet(simplex, 2, A);
        ccdSimplexSetSize(simplex, 3);
    }

    return doSimplex3(simplex, dir);
}

int collisionChecker::doSimplex(ccd_simplex_t *simplex, ccd_vec3_t *dir)
{
    if (ccdSimplexSize(simplex) == 2){
        // simplex contains segment only one segment
        return doSimplex2(simplex, dir);
    }else if (ccdSimplexSize(simplex) == 3){
        // simplex contains triangle
      return doSimplex3(simplex, dir);
    }else{ // ccdSimplexSize(simplex) == 4
        // tetrahedron - this is the only shape which can encapsule origin
        // so doSimplex4() also contains test on it
        return doSimplex4(simplex, dir);
    }
}
/*
int collisionChecker::doSimplex(ccd_simplex_t *simplex_, ccd_vec3_t *dir_) const
{
    
    if (ccdSimplexSize(&simplex_c) == 2){
        // simplex contains segment only one segment
        return doSimplex2(&simplex_c, &dir_c);
    }else if (ccdSimplexSize(&simplex_c) == 3){
        // simplex contains triangle
        return doSimplex3(&simplex_c, &dir_c);
    }else{ // ccdSimplexSize(simplex) == 4
        // tetrahedron - this is the only shape which can encapsule origin
        // so doSimplex4() also contains test on it
        return doSimplex4(&simplex_c, &dir_c);
    }
}
*/
ccd_real_t collisionChecker::simplexReduceToTriangle(ccd_simplex_t *simplex,
                                          ccd_real_t dist,
                                          ccd_vec3_t *best_witness)
{
  ccd_real_t newdist;
  ccd_vec3_t witness;
  int best = -1;
  int i;

  // try the fourth point in all three positions
  for (i = 0; i < 3; i++){
    newdist = ccdVec3PointTriDist2(ccd_vec3_origin,
                                   &ccdSimplexPoint(simplex, (i == 0 ? 3 : 0))->v,
                                   &ccdSimplexPoint(simplex, (i == 1 ? 3 : 1))->v,
                                   &ccdSimplexPoint(simplex, (i == 2 ? 3 : 2))->v,
                                   &witness);
    newdist = CCD_SQRT(newdist);

    // record the best triangle
    if (newdist < dist){
      dist = newdist;
      best = i;
      ccdVec3Copy(best_witness, &witness);
    }
  }

  if (best >= 0){
    ccdSimplexSet(simplex, best, ccdSimplexPoint(simplex, 3));
  }
  ccdSimplexSetSize(simplex, 3);

  return dist;
}
