#include "libs/ccd/collision_checker.h"

namespace libccd
{
// constructor to initialize pcl_eigen object:
collisionChecker::collisionChecker()
{
  _pcl_eigen.init();
  min_env_size=2;
}
void collisionChecker::load_mesh(string &file_name, vector<Vector3d> &obj)
{ 
  _pcl_eigen.loadPLY(file_name,obj);
}

void collisionChecker::load_meshes(vector<string> &file_names,   vector<vector<Vector3d>> &obj_arr)
{ 
  obj_arr.resize(file_names.size());
  for(int i=0;i<file_names.size();++i)
  {
#ifdef DEBUG
      cerr<<"loading mesh: "<<i<<" file: "<<file_names[i]<<endl;
#endif
      _pcl_eigen.loadPLY(file_names[i],obj_arr[i]);
  }
  
}
void collisionChecker::load_PCL(PointCloud<PointXYZ>::Ptr in_cloud, float &vsize, vector<vector<Vector3d>> &obj_arr)
{
  // Assuming the pointcloud is already preprocessed
  // perform convex decomp:
  _pcl_eigen.convex_decomp(in_cloud,obj_arr,vsize);
}
void collisionChecker::loadMeshModel(vector<Vector3d> &points,vector<double> &pose, obj_t &cvx_model)
{
  ccd_vec3_t pos={.v={float(pose[0]),float(pose[1]),float(pose[2])}};
  // Assuming normalized quaternion:
  ccd_quat_t quat={.q={float(pose[3]),float(pose[4]),float(pose[5]),float(pose[6])}};

  cvx_model={.pos=pos,.quat=quat,.vertices=points};
  
}

void collisionChecker::create_coll_object(vector<Vector3d> &mesh_object, vector<double> &mesh_pose, obj_t &coll_object)
{
  loadMeshModel(mesh_object, mesh_pose, coll_object);
}


void collisionChecker::create_coll_objects(vector<vector<Vector3d>> &mesh_objects, vector<vector<double>> &mesh_poses, vector<obj_t> &coll_objects)
{
  coll_objects.resize(mesh_objects.size());
  for(int i=0;i<mesh_objects.size();++i)
    {
      #ifdef DEBUG
      cerr<<"robot link: "<<i<<endl;
      #endif
      create_coll_object(mesh_objects[i],mesh_poses[i], coll_objects[i]);

      #ifdef DEBUG
      cerr<<"stored mesh model: "<<i<<endl;
      #endif
    }
}

// if you don't have poses for the objects, which is the case for the environment,
void collisionChecker::create_coll_objects(vector<vector<Vector3d>> &mesh_objects, vector<obj_t> &coll_objects)
{
  vector<double> pose={0.0,0.0,0.0,0.0,0.0,0.0,1.0};
  // count number of objects above threshold:
  int cnt=0;
  for(int i=0;i<mesh_objects.size();++i)
  {
    if(mesh_objects[i].size()>min_env_size)
    {
      cnt++;
    }
  }
  //
  coll_objects.resize(cnt);
  cnt=0;
  for(int i=0;i<mesh_objects.size();++i)
  {
    if(mesh_objects[i].size()>min_env_size)
    {
      create_coll_object(mesh_objects[i],pose, coll_objects[cnt]);
      cnt++;
    }
  }
}

void collisionChecker::update_obj_pose(obj_t &obj, vector<double> &pose)
{
  obj.pos.v[0]=pose[0];
  obj.pos.v[1]=pose[1];
  obj.pos.v[2]=pose[2];

  obj.quat.q[0]=pose[3];
  obj.quat.q[1]=pose[4];
  obj.quat.q[2]=pose[5];
  obj.quat.q[3]=pose[6];
  
}
void collisionChecker::update_poses(vector<obj_t> &obj_arr, vector<vector<double>> &poses)
{
  for (int i=0;i<obj_arr.size();++i)
  {
    update_obj_pose(obj_arr[i],poses[i]);
  }
}

void collisionChecker::update_poses(vector<obj_t> &obj_arr, vector<double> &pose)
{
  for (int i=0;i<obj_arr.size();++i)
  {
    update_obj_pose(obj_arr[i],pose);
  }
}

void collisionChecker::detect_collision(const obj_t &obj1, const obj_t &obj2, const ccd_t &ccd, bool &collision) const
{

  ccd_real_t depth;
  ccd_vec3_t dir, point_pos;
  #ifdef DEBUG_ALL
  cerr<<"Performing GJK, if segfaulting next, uninstall libccd* using apt-get and compile from source"<<endl;
  #endif

  int intersect = ccdGJKPenetration(&obj1, &obj2, &ccd, &depth, &dir, &point_pos);
  //int intersect = ccdGJKIntersect(&obj1, &obj2, &ccd);
  #ifdef DEBUG_ALL
  cerr<<"Performed GJK"<<endl;
  #endif

  if(intersect==0)
  {
#ifdef DEBUG_ALL
    cerr<<"GJK: In collision"<<endl;
#endif
    collision=true;
  }
  else
  {
    collision=false;
  }
}
void collisionChecker::signed_distance(const obj_t &obj1,const obj_t &obj2, const ccd_t &ccd,collision_data &c_dat) const
{
  ccd_real_t depth;
  ccd_vec3_t dir, point_pos;
  #ifdef DEBUG_ALL
  std::cout << "line 149 ccd signed_distance" << std::endl;
  cerr<<"Performing GJK, if segfaulting next, uninstall libccd* using apt-get and compile from source"<<endl;
  #endif
  // GJK has a bug where false positives are obtained
  //int intersect = ccdGJKPenetration(&obj1, &obj2, &ccd, &depth, &dir, &point_pos);
  int intersect = ccdMPRPenetration(&obj1, &obj2, &ccd, &depth, &dir, &point_pos);
  //int intersect = ccdGJKIntersect(&obj1, &obj2, &ccd);
  #ifdef DEBUG_ALL
  cerr<<"Performed GJK"<<endl;
  #endif

  
  bool collision=false;
  if(intersect==0)
    {
       #ifdef DEBUG_ALL
      cerr<<"GJK: In collision"<<endl;
       #endif

      collision=true;
      c_dat.collision=collision;
      c_dat.point=Vector3d(point_pos.v[0],point_pos.v[1],point_pos.v[2]);
      c_dat.dir=Vector3d(dir.v[0],dir.v[1],dir.v[2]);
      c_dat.distance=depth;
       #ifdef DEBUG_ALL
      cerr<<"GJK: In collision, storing data"<<endl;
       #endif
      
    }
  else if(intersect==-1)// if not in collision:
    {
      // How is this computed?
      //
      
      #ifdef DEBUG_ALL
      cerr<<"GJK: Not in collision"<<endl;
       #endif

      ccd_simplex_t simplex;
      #ifdef DEBUG_ALL
      cerr<<"GJK: computing penetration depth"<<endl;
       #endif

      int coll=__ccdGJK(&obj1, &obj2, &ccd, &simplex);
      #ifdef DEBUG_ALL
      cerr<<"GJK: called __ccdGJK()"<<endl;
       #endif
      
      depth=_ccdDist(&obj1,&obj2,&ccd,&simplex,&dir,&point_pos);
      #ifdef DEBUG_ALL
      cerr<<"GJK: computed penetration depth"<<endl;
       #endif

      collision=false;
      c_dat.collision=collision;
      c_dat.point=Vector3d(point_pos.v[0],point_pos.v[1],point_pos.v[2]);
      #ifdef DEBUG_ALL 
      cerr<<"GJK: storing point"<<endl;
       #endif

      c_dat.dir=Vector3d(dir.v[0],dir.v[1],dir.v[2]);
      c_dat.distance=-depth;
      if(coll==0)
      {
        cerr<<"GJK thinks its in collision, we set everything to 0: "<<endl;
        c_dat.distance=0.0;
        c_dat.dir=Vector3d(0.0,0.0,0.0);
      }

      
    }
  else
  {
    cerr<<"Memory allocation failed!!!!!!!!"<<endl;
  }
  #ifdef DEBUG_ALL
   cerr<<"Got c_dat from GJK"<<endl;
  #endif
 
}

void collisionChecker::detect_collisions(const obj_t &obj1,const vector<obj_t> &obj_env, const ccd_t &ccd,bool &collision) const
{
  collision=false;
  for(int i=0;i<obj_env.size();++i)
  {
    detect_collision(obj1,obj_env[i],ccd,collision);
    if(collision==true)
    {
      break;
    }
  }
}

void collisionChecker::signed_distance_arr(const obj_t &obj_a,const vector<obj_t>& obj_arr, const ccd_t &ccd, vector<collision_data> &c_dat_arr) const
{

  #ifdef DEBUG_ALL
  cerr<<"compute_signed_distance(): "<<obj_arr.size()<<endl;
  #endif
  c_dat_arr.resize(obj_arr.size());
  for(int i=0;i<obj_arr.size();++i)
  {
    signed_distance(obj_arr[i],obj_a,ccd, c_dat_arr[i]);
  }
}
void collisionChecker::check_pt_collision(vector<Vector3d> &points, obj_t &cvx_obj, const ccd_t &ccd,collision_data &c_data)
{
  // Create objects for points:
  vector<double> pose;
  pose.resize(7,0.0);
  pose[6]=1.0;
  obj_t t_mesh;
  loadMeshModel(points,pose,t_mesh);
  signed_distance(t_mesh, cvx_obj,ccd, c_data);
}

void collisionChecker::check_collision(obj_t &obj_a, obj_t &obj_b,const ccd_t &ccd, collision_data &c_data) const
{
  signed_distance(obj_a, obj_b,ccd, c_data);
}

// signed distance computation when objects are not in collision:
// Obtained from fcl source files: https://github.com/flexible-collision-library/fcl/blob/master/include/fcl/narrowphase/detail/convexity_based_algorithm/gjk_libccd-inl.h

ccd_real_t  collisionChecker::_ccdDist(const void *obj1, const void *obj2,const ccd_t *ccd,ccd_simplex_t* simplex,   ccd_vec3_t* dir, ccd_vec3_t* p1) const
{
  unsigned long iterations;
  //ccd_simplex_t simplex;
  ccd_support_t last; // last support point
  //ccd_vec3_t dir; // direction vector
  ccd_real_t dist, last_dist;
  ccd_vec3_t p3;
  ccd_vec3_t *p2=&p3;

  // first find an intersection
  if (__ccdGJK(obj1, obj2, ccd, simplex) == 0)
    return -CCD_ONE;

  last_dist = CCD_REAL_MAX;

  for (iterations = 0UL; iterations < ccd->max_iterations; ++iterations) {
    // get a next direction vector
    // we are trying to find out a point on the minkowski difference
    // that is nearest to the origin, so we obtain a point on the
    // simplex that is nearest and try to exapand the simplex towards
    // the origin
    if (ccdSimplexSize(simplex) == 1){
      ccdVec3Copy(dir, &ccdSimplexPoint(simplex, 0)->v);
      dist = ccdVec3Len2(&ccdSimplexPoint(simplex, 0)->v);
      dist = CCD_SQRT(dist);
    }else if (ccdSimplexSize(simplex) == 2){
      dist = ccdVec3PointSegmentDist2(ccd_vec3_origin,
                                      &ccdSimplexPoint(simplex, 0)->v,
                                      &ccdSimplexPoint(simplex, 1)->v,
                                      dir);
      dist = CCD_SQRT(dist);
    }else if(ccdSimplexSize(simplex) == 3){
      dist = ccdVec3PointTriDist2(ccd_vec3_origin,
                                  &ccdSimplexPoint(simplex, 0)->v,
                                  &ccdSimplexPoint(simplex, 1)->v,
                                  &ccdSimplexPoint(simplex, 2)->v,
                                  dir);
      dist = CCD_SQRT(dist);
    }else{ // ccdSimplexSize(simplex) == 4
      dist = simplexReduceToTriangle(simplex, last_dist, dir);
    }

    // touching contact -- do we really need this?
    // maybe __ccdGJK() solve this alredy.
    if (ccdIsZero(dist))
      return -CCD_ONE;

    // check whether we improved for at least a minimum tolerance
    if ((last_dist - dist) < ccd->dist_tolerance)
    {
      if(p1) *p1 = last.v1;
      if(p2) *p2 = last.v2;
      return dist;
    }

    // point direction towards the origin
    ccdVec3Scale(dir, -CCD_ONE);
    ccdVec3Normalize(dir);

    // find out support point
    __ccdSupport(obj1, obj2, dir, ccd, &last);

    // record last distance
    last_dist = dist;

    // check whether we improved for at least a minimum tolerance
    // this is here probably only for a degenerate cases when we got a
    // point that is already in the simplex
    dist = ccdVec3Len2(&last.v);
    dist = CCD_SQRT(dist);
    if (CCD_FABS(last_dist - dist) < ccd->dist_tolerance)
    {
      if(p1) *p1 = last.v1;
      if(p2) *p2 = last.v2;
      return last_dist;
    }

    // add a point to simplex
    ccdSimplexAdd(simplex, &last);
  }

  return -CCD_REAL(1.);
  /*
  ccd_vec3_t p3;
  ccd_vec3_t *p2=&p3;
   unsigned long iterations;
  ccd_support_t last; // last support point
 // direction vector
  ccd_real_t dist, last_dist = CCD_REAL_MAX;

  for (iterations = 0UL; iterations < ccd_->max_iterations; ++iterations)
  {
    // get a next direction vector
    // we are trying to find out a point on the minkowski difference
    // that is nearest to the origin, so we obtain a point on the
    // simplex that is nearest and try to exapand the simplex towards
    // the origin
    //cerr<<"Initialized: "<<ccdSimplexSize(simplex)<<endl;
    if (ccdSimplexSize(simplex) == 1)
    {
      ccdVec3Copy(dir, &ccdSimplexPoint(simplex, 0)->v);
      dist = ccdVec3Len2(&ccdSimplexPoint(simplex, 0)->v);
      dist = CCD_SQRT(dist);
    }
    else if (ccdSimplexSize(simplex) == 2)
    {
      dist = ccdVec3PointSegmentDist2(ccd_vec3_origin,
                                      &ccdSimplexPoint(simplex, 0)->v,
                                      &ccdSimplexPoint(simplex, 1)->v,
                                      dir);
      dist = CCD_SQRT(dist);
    }
    else if(ccdSimplexSize(simplex) == 3)
    {
      dist = ccdVec3PointTriDist2(ccd_vec3_origin,
                                  &ccdSimplexPoint(simplex, 0)->v,
                                  &ccdSimplexPoint(simplex, 1)->v,
                                  &ccdSimplexPoint(simplex, 2)->v,
                                  dir);
      dist = CCD_SQRT(dist);
    }
    else
    {// ccdSimplexSize(&simplex) == 4
      dist = simplexReduceToTriangle(simplex, last_dist, dir);
    }

    // check whether we improved for at least a minimum tolerance
    if ((last_dist - dist) < ccd_->dist_tolerance)
    {
      if(p1) *p1 = last.v1;
      if(p2) *p2 = last.v2;
      return dist;
    }

    // point direction towards the origin
    ccdVec3Scale(dir, -CCD_ONE);
    ccdVec3Normalize(dir);

    // find out support point
    __ccdSupport(obj1, obj2, dir, ccd_, &last);

    // record last distance
    last_dist = dist;

    // check whether we improved for at least a minimum tolerance
    // this is here probably only for a degenerate cases when we got a
    // point that is already in the simplex
    dist = ccdVec3Len2(&last.v);
    dist = CCD_SQRT(dist);
    if (CCD_FABS(last_dist - dist) < ccd_->dist_tolerance)
    {
      if(p1) *p1 = last.v1;
      if(p2) *p2 = last.v2;
      return last_dist;
    }

    // add a point to simplex
    ccdSimplexAdd(simplex, &last);
  }

  return -CCD_REAL(1.);
  */
}


int collisionChecker::__ccdGJK(const void *obj1, const void *obj2,
                    const ccd_t *ccd_1, ccd_simplex_t *simplex) const
{
    unsigned long iterations;
    ccd_vec3_t dir; // direction vector
    ccd_support_t last; // last support point
    int do_simplex_res;

    // initialize simplex struct
    ccdSimplexInit(simplex);

    // get first direction
    ccd_1->first_dir(obj1, obj2, &dir);
    // get first support point
    __ccdSupport(obj1, obj2, &dir, ccd_1, &last);
    // and add this point to simplex as last one
    ccdSimplexAdd(simplex, &last);

    // set up direction vector to as (O - last) which is exactly -last
    ccdVec3Copy(&dir, &last.v);
    ccdVec3Scale(&dir, -CCD_ONE);
    #ifdef DEBUG_ALL
    cerr<<"__ccdGJK(): starting iterations"<<endl;
    #endif

    // start iterations
    for (iterations = 0UL; iterations < ccd_1->max_iterations; ++iterations) {
        // obtain support point
         __ccdSupport(obj1, obj2, &dir, ccd_1, &last);
         #ifdef DEBUG_ALL
         cerr<<"__ccdGJK(): called __ccdSupport()"<<endl;
         #endif

        // check if farthest point in Minkowski difference in direction dir
        // isn't somewhere before origin (the test on negative dot product)
        // - because if it is, objects are not intersecting at all.
        //cerr<<"dot"<<last.v.v[0]<<endl;
        if (ccdVec3Dot(&last.v, &dir) < CCD_ZERO){
            return -1; // intersection not found
        }
         #ifdef DEBUG_ALL
         cerr<<"__ccdGJK(): calling __ccdSimplexAdd()"<<endl;
         #endif

        // add last support vector to simplex
        ccdSimplexAdd(simplex, &last);
         #ifdef DEBUG_ALL
         cerr<<"__ccdGJK(): called __ccdSimplexAdd()"<<endl;
         #endif

        // if doSimplex returns 1 if objects intersect, -1 if objects don't
        // intersect and 0 if algorithm should continue
        do_simplex_res = doSimplex(simplex, &dir);
        if (do_simplex_res == 1){
            return 0; // intersection found
        }else if (do_simplex_res == -1){
            return -1; // intersection not found
        }

        if (ccdIsZero(ccdVec3Len2(&dir))){
            return -1; // intersection not found
        }
    }

    // intersection wasn't found
    return -1;
}

void collisionChecker::tripleCross(const ccd_vec3_t *a, const ccd_vec3_t *b,
                             const ccd_vec3_t *c, ccd_vec3_t *d) const
{
    ccd_vec3_t e;
    ccdVec3Cross(&e, a, b);
    ccdVec3Cross(d, &e, c);
}


int collisionChecker::doSimplex2(ccd_simplex_t *simplex, ccd_vec3_t *dir) const
{
    const ccd_support_t *A, *B;
    ccd_vec3_t AB, AO, tmp;
    ccd_real_t dot;

    // get last added as A
    A = ccdSimplexLast(simplex);
    // get the other point
    B = ccdSimplexPoint(simplex, 0);
    // compute AB oriented segment
    ccdVec3Sub2(&AB, &B->v, &A->v);
    // compute AO vector
    ccdVec3Copy(&AO, &A->v);
    ccdVec3Scale(&AO, -CCD_ONE);

    // dot product AB . AO
    dot = ccdVec3Dot(&AB, &AO);

    // check if origin doesn't lie on AB segment
    ccdVec3Cross(&tmp, &AB, &AO);
    if (ccdIsZero(ccdVec3Len2(&tmp)) && dot > CCD_ZERO){
        return 1;
    }

    // check if origin is in area where AB segment is
    if (ccdIsZero(dot) || dot < CCD_ZERO){
        // origin is in outside are of A
        ccdSimplexSet(simplex, 0, A);
        ccdSimplexSetSize(simplex, 1);
        ccdVec3Copy(dir, &AO);
    }else{
        // origin is in area where AB segment is

        // keep simplex untouched and set direction to
        // AB x AO x AB
        tripleCross(&AB, &AO, &AB, dir);
    }

    return 0;
}
int collisionChecker::doSimplex3(ccd_simplex_t *simplex, ccd_vec3_t *dir) const
{
    const ccd_support_t *A, *B, *C;
    ccd_vec3_t AO, AB, AC, ABC, tmp;
    ccd_real_t dot, dist;

    // get last added as A
    A = ccdSimplexLast(simplex);
    // get the other points
    B = ccdSimplexPoint(simplex, 1);
    C = ccdSimplexPoint(simplex, 0);

    // check touching contact
    dist = ccdVec3PointTriDist2(ccd_vec3_origin, &A->v, &B->v, &C->v, NULL);
    if (ccdIsZero(dist)){
        return 1;
    }

    // check if triangle is really triangle (has area > 0)
    // if not simplex can't be expanded and thus no itersection is found
    if (ccdVec3Eq(&A->v, &B->v) || ccdVec3Eq(&A->v, &C->v)){
        return -1;
    }

    // compute AO vector
    ccdVec3Copy(&AO, &A->v);
    ccdVec3Scale(&AO, -CCD_ONE);

    // compute AB and AC segments and ABC vector (perpendircular to triangle)
    ccdVec3Sub2(&AB, &B->v, &A->v);
    ccdVec3Sub2(&AC, &C->v, &A->v);
    ccdVec3Cross(&ABC, &AB, &AC);

    ccdVec3Cross(&tmp, &ABC, &AC);
    dot = ccdVec3Dot(&tmp, &AO);
    if (ccdIsZero(dot) || dot > CCD_ZERO){
        dot = ccdVec3Dot(&AC, &AO);
        if (ccdIsZero(dot) || dot > CCD_ZERO){
            // C is already in place
            ccdSimplexSet(simplex, 1, A);
            ccdSimplexSetSize(simplex, 2);
            tripleCross(&AC, &AO, &AC, dir);
        }else{
ccd_do_simplex3_45:
            dot = ccdVec3Dot(&AB, &AO);
            if (ccdIsZero(dot) || dot > CCD_ZERO){
                ccdSimplexSet(simplex, 0, B);
                ccdSimplexSet(simplex, 1, A);
                ccdSimplexSetSize(simplex, 2);
                tripleCross(&AB, &AO, &AB, dir);
            }else{
                ccdSimplexSet(simplex, 0, A);
                ccdSimplexSetSize(simplex, 1);
                ccdVec3Copy(dir, &AO);
            }
        }
    }else{
        ccdVec3Cross(&tmp, &AB, &ABC);
        dot = ccdVec3Dot(&tmp, &AO);
        if (ccdIsZero(dot) || dot > CCD_ZERO){
            goto ccd_do_simplex3_45;
        }else{
            dot = ccdVec3Dot(&ABC, &AO);
            if (ccdIsZero(dot) || dot > CCD_ZERO){
                ccdVec3Copy(dir, &ABC);
            }else{
                ccd_support_t Ctmp;
                ccdSupportCopy(&Ctmp, C);
                ccdSimplexSet(simplex, 0, B);
                ccdSimplexSet(simplex, 1, &Ctmp);

                ccdVec3Copy(dir, &ABC);
                ccdVec3Scale(dir, -CCD_ONE);
            }
        }
    }

    return 0;
}

int collisionChecker::doSimplex4(ccd_simplex_t *simplex, ccd_vec3_t *dir) const
{
    const ccd_support_t *A, *B, *C, *D;
    ccd_vec3_t AO, AB, AC, AD, ABC, ACD, ADB;
    int B_on_ACD, C_on_ADB, D_on_ABC;
    int AB_O, AC_O, AD_O;
    ccd_real_t dist;

    // get last added as A
    A = ccdSimplexLast(simplex);
    // get the other points
    B = ccdSimplexPoint(simplex, 2);
    C = ccdSimplexPoint(simplex, 1);
    D = ccdSimplexPoint(simplex, 0);

    // check if tetrahedron is really tetrahedron (has volume > 0)
    // if it is not simplex can't be expanded and thus no intersection is
    // found
    dist = ccdVec3PointTriDist2(&A->v, &B->v, &C->v, &D->v, NULL);
    if (ccdIsZero(dist)){
        return -1;
    }

    // check if origin lies on some of tetrahedron's face - if so objects
    // intersect
    dist = ccdVec3PointTriDist2(ccd_vec3_origin, &A->v, &B->v, &C->v, NULL);
    if (ccdIsZero(dist))
        return 1;
    dist = ccdVec3PointTriDist2(ccd_vec3_origin, &A->v, &C->v, &D->v, NULL);
    if (ccdIsZero(dist))
        return 1;
    dist = ccdVec3PointTriDist2(ccd_vec3_origin, &A->v, &B->v, &D->v, NULL);
    if (ccdIsZero(dist))
        return 1;
    dist = ccdVec3PointTriDist2(ccd_vec3_origin, &B->v, &C->v, &D->v, NULL);
    if (ccdIsZero(dist))
        return 1;

    // compute AO, AB, AC, AD segments and ABC, ACD, ADB normal vectors
    ccdVec3Copy(&AO, &A->v);
    ccdVec3Scale(&AO, -CCD_ONE);
    ccdVec3Sub2(&AB, &B->v, &A->v);
    ccdVec3Sub2(&AC, &C->v, &A->v);
    ccdVec3Sub2(&AD, &D->v, &A->v);
    ccdVec3Cross(&ABC, &AB, &AC);
    ccdVec3Cross(&ACD, &AC, &AD);
    ccdVec3Cross(&ADB, &AD, &AB);

    // side (positive or negative) of B, C, D relative to planes ACD, ADB
    // and ABC respectively
    B_on_ACD = ccdSign(ccdVec3Dot(&ACD, &AB));
    C_on_ADB = ccdSign(ccdVec3Dot(&ADB, &AC));
    D_on_ABC = ccdSign(ccdVec3Dot(&ABC, &AD));

    // whether origin is on same side of ACD, ADB, ABC as B, C, D
    // respectively
    AB_O = ccdSign(ccdVec3Dot(&ACD, &AO)) == B_on_ACD;
    AC_O = ccdSign(ccdVec3Dot(&ADB, &AO)) == C_on_ADB;
    AD_O = ccdSign(ccdVec3Dot(&ABC, &AO)) == D_on_ABC;

    if (AB_O && AC_O && AD_O){
        // origin is in tetrahedron
        return 1;

    // rearrange simplex to triangle and call doSimplex3()
    }else if (!AB_O){
        // B is farthest from the origin among all of the tetrahedron's
        // points, so remove it from the list and go on with the triangle
        // case

        // D and C are in place
        ccdSimplexSet(simplex, 2, A);
        ccdSimplexSetSize(simplex, 3);
    }else if (!AC_O){
        // C is farthest
        ccdSimplexSet(simplex, 1, D);
        ccdSimplexSet(simplex, 0, B);
        ccdSimplexSet(simplex, 2, A);
        ccdSimplexSetSize(simplex, 3);
    }else{ // (!AD_O)
        ccdSimplexSet(simplex, 0, C);
        ccdSimplexSet(simplex, 1, B);
        ccdSimplexSet(simplex, 2, A);
        ccdSimplexSetSize(simplex, 3);
    }

    return doSimplex3(simplex, dir);
}

int collisionChecker::doSimplex(ccd_simplex_t *simplex, ccd_vec3_t *dir) const
{
    if (ccdSimplexSize(simplex) == 2){
        // simplex contains segment only one segment
        return doSimplex2(simplex, dir);
    }else if (ccdSimplexSize(simplex) == 3){
        // simplex contains triangle
      return doSimplex3(simplex, dir);
    }else{ // ccdSimplexSize(simplex) == 4
        // tetrahedron - this is the only shape which can encapsule origin
        // so doSimplex4() also contains test on it
        return doSimplex4(simplex, dir);
    }
}
ccd_real_t collisionChecker::simplexReduceToTriangle(ccd_simplex_t *simplex,
                                          ccd_real_t dist, 
                                          ccd_vec3_t *best_witness) const
{
  ccd_real_t newdist;
  ccd_vec3_t witness;
  int best = -1;
  int i;

  // try the fourth point in all three positions
  for (i = 0; i < 3; i++){
    newdist = ccdVec3PointTriDist2(ccd_vec3_origin,
                                   &ccdSimplexPoint(simplex, (i == 0 ? 3 : 0))->v,
                                   &ccdSimplexPoint(simplex, (i == 1 ? 3 : 1))->v,
                                   &ccdSimplexPoint(simplex, (i == 2 ? 3 : 2))->v,
                                   &witness);
    newdist = CCD_SQRT(newdist);

    // record the best triangle
    if (newdist < dist){
      dist = newdist;
      best = i;
      ccdVec3Copy(best_witness, &witness);
    }
  }

  if (best >= 0){
    ccdSimplexSet(simplex, best, ccdSimplexPoint(simplex, 3));
  }
  ccdSimplexSetSize(simplex, 3);

  return dist;
}
}

