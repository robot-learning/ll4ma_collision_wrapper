#include <ll4ma_collision_wrapper/libs/cvx_hull/newton_cvxhull.h>
#include "NewtonApple_hull3D.h"
using namespace Math3D;
using namespace std;
namespace cvxhull
{
/*
bool CvxHull::load_verts(const std::vector<Eigen::Vector3d> &pts, vector<Math3D::Vector3> &verts)
{
  verts.clear();
  verts.resize(pts.size());
  for(int i=0;i<pts.size();++i)
  {
    verts[i]=Vector3(pts[i][0],pts[i][1],pts[i][2]);
  }
  return true;
}
*/
bool CvxHull::compute_delaunay(const std::vector<Eigen::Vector3d> &verts,Meshing::TriMesh &cvx_mesh) const
{
  std::vector<R3> pts, pts2;
  R3 pt;
  pts.clear();
  cvx_mesh.verts.clear();

  // convert to wrapper format
  for(int i=0;i<verts.size();++i)
  {
    pt.id=i;
    pt.r=verts[i][0];
    pt.c=verts[i][1];
    pt.z=verts[i][2];
    pts.push_back(pt);
    
  }
  //
  std::vector<Tri> tris;
  
  std::vector<int> outx;
  int nx = de_duplicateR3( pts, outx, pts2);
  pts.clear();

  
  // write_R3(pts2, "pts.mat");
  
  //struct timeval tv1, tv2; // slight swizzle as pt set is now sorted.
  //gettimeofday(&tv1, NULL);

  int ts = NewtonApple_Delaunay( pts2, tris);
  if(ts==-1)
  {
    ts = NewtonApple_Delaunay( pts2, tris);
  }
  //gettimeofday(&tv2, NULL);
  //float tx =  (tv2.tv_sec + tv2.tv_usec / 1000000.0) - ( tv1.tv_sec + tv1.tv_usec / 1000000.0);

  //cerr <<  tx << " seconds for triangulation" << endl;

          
  //write_Tris(tris, "triangles.mat");
  //std::cerr << tris.size() << " triangles written to triangles.mat" << std::endl;

  for(int i=0;i<pts2.size();++i)
  {
    Math3D::Vector3 v_(pts2[i].r,pts2[i].c,pts2[i].z);
    cvx_mesh.verts.emplace_back(v_);

  }

  // build trimesh faces:
  cvx_mesh.tris.clear();
  for(int i=0;i<tris.size();++i)
  {
    IntTriple tri(tris[i].a,tris[i].b,tris[i].c);
    cvx_mesh.tris.emplace_back(tri);
  }

  return true;
  
}
bool CvxHull::compute_hull(const vector<Eigen::Vector3d> &verts,Meshing::TriMesh &cvx_mesh) const
{

  std::vector<R3> pts, pts2;
  R3 pt;
  pts.clear();
  cvx_mesh.verts.clear();

  // convert to wrapper format
  for(int i=0;i<verts.size();++i)
  {
    pt.id=i;
    pt.r=verts[i][0];
    pt.c=verts[i][1];
    pt.z=verts[i][2];
    pts.push_back(pt);
    
  }
  //
  std::vector<Tri> tris;
  
  std::vector<int> outx;
  int nx = de_duplicateR3( pts, outx, pts2);
  pts.clear();

  
  // write_R3(pts2, "pts.mat");
  
  //struct timeval tv1, tv2; // slight swizzle as pt set is now sorted.
  //gettimeofday(&tv1, NULL);

  int ts = NewtonApple_hull_3D( pts2, tris);
  if(ts==-1)
  {
    ts = NewtonApple_hull_3D( pts2, tris);
  }
  //int ts = init_hull3D_compact( pts2, tris);
          
  //gettimeofday(&tv2, NULL);
  //float tx =  (tv2.tv_sec + tv2.tv_usec / 1000000.0) - ( tv1.tv_sec + tv1.tv_usec / 1000000.0);

  //cerr <<  tx << " seconds for triangulation" << endl;

          
  //write_Tris(tris, "triangles.mat");
  //std::cerr << tris.size() << " triangles written to triangles.mat" << std::endl;

  for(int i=0;i<pts2.size();++i)
  {
    Math3D::Vector3 v_(pts2[i].r,pts2[i].c,pts2[i].z);
    cvx_mesh.verts.emplace_back(v_);

  }

  // build trimesh faces:
  cvx_mesh.tris.clear();
  for(int i=0;i<tris.size();++i)
  {
    IntTriple tri(tris[i].a,tris[i].b,tris[i].c);
    cvx_mesh.tris.emplace_back(tri);
  }

  return true;
  
}

double CvxHull::compute_volume(const Meshing::TriMesh &mesh) const
{
  double volume=0.0;
  // compute the volume of the hull by iterating over the faces:
  for(int i=0;i<mesh.tris.size();++i)
  {
    // get vertices
    Eigen::Vector3d v1(mesh.verts[mesh.tris[i].a].x,mesh.verts[mesh.tris[i].a].y,mesh.verts[mesh.tris[i].a].z);
    
    Eigen::Vector3d v2(mesh.verts[mesh.tris[i].b].x,mesh.verts[mesh.tris[i].b].y,mesh.verts[mesh.tris[i].b].z);
    
    Eigen::Vector3d v3(mesh.verts[mesh.tris[i].c].x,mesh.verts[mesh.tris[i].c].y,mesh.verts[mesh.tris[i].c].z);
    volume+=(v1.cross(v2)).dot(v3);
  }
  volume=abs(volume)/6.0;
  return volume;
}
}
