#include <ll4ma_collision_wrapper/libs/cvx_hull/cgal_cvxhull.h>
using namespace Math3D;
using namespace std;

namespace cgal_cvxhull
{
bool CvxHull::compute_hull(const vector<Eigen::Vector3d> &verts,Meshing::TriMesh &cvx_mesh) const
{
  
  typedef CGAL::Exact_predicates_inexact_constructions_kernel  K;
  typedef CGAL::Polyhedron_3<K>                     Polyhedron_3;
  typedef K::Point_3                                Point_3;
  typedef Polyhedron_3::Vertex_iterator  Vertex_iterator;
  typedef Polyhedron_3::Facet_iterator  Facet_iterator;
  typedef Polyhedron_3::Halfedge_around_facet_circulator Halfedge_facet_circulator;

  std::vector<Point_3> points;


  cvx_mesh.verts.clear();

  // convert to wrapper format
  for(int i=0;i<verts.size();++i)
  {
    points.emplace_back(Point_3(verts[i][0],verts[i][1],verts[i][2]));
    
  }

  Polyhedron_3 poly;
  CGAL::convex_hull_3(points.begin(),points.end(),poly);

  //std::cerr << "The convex hull contains " << poly.size_of_vertices() <<" "<<poly.is_pure_triangle()<< " vertices" << std::endl;

  // get vertices, faces and map to trimesh:
  //std::map<Point_3, int> index_of_vertex;
  //std::size_t i = 0;
  for(Vertex_iterator it = poly.vertices_begin(); 
      it != poly.vertices_end(); ++it)
  {
    /*
    output << it->point().x() << " " 
           << it->point().y() << " " 
           << it->point().z() << std::endl;
    */
    Math3D::Vector3 v_(it->point().x(),it->point().y(),it->point().z());
    cvx_mesh.verts.emplace_back(v_);

    //index_of_vertex[it->point()] = i;
  }

  cvx_mesh.tris.clear();
  for(Facet_iterator it = poly.facets_begin(); it != poly.facets_end(); ++it)
  {
     Halfedge_facet_circulator j = it->facet_begin();
     // Facets in polyhedral surfaces are at least triangles.
     //CGAL_assertion( CGAL::circulator_size(j) >= 3);
     //std::cout << CGAL::circulator_size(j) << ' ';
     if(CGAL::circulator_size(j)==3)
     {
       IntTriple tri;
       tri.a=std::distance(poly.vertices_begin(),j->vertex());
       ++j;
       tri.b=std::distance(poly.vertices_begin(),j->vertex());
       ++j;
       tri.c=std::distance(poly.vertices_begin(),j->vertex());
       cvx_mesh.tris.emplace_back(tri);
     }
     
  
  }

    
  return true;
  
}

double CvxHull::compute_volume(const Meshing::TriMesh &mesh) const
{
  double volume=0.0;
  // compute the volume of the hull by iterating over the faces:
  for(int i=0;i<mesh.tris.size();++i)
  {
    // get vertices
    Eigen::Vector3d v1(mesh.verts[mesh.tris[i].a].x,mesh.verts[mesh.tris[i].a].y,mesh.verts[mesh.tris[i].a].z);
    
    Eigen::Vector3d v2(mesh.verts[mesh.tris[i].b].x,mesh.verts[mesh.tris[i].b].y,mesh.verts[mesh.tris[i].b].z);
    
    Eigen::Vector3d v3(mesh.verts[mesh.tris[i].c].x,mesh.verts[mesh.tris[i].c].y,mesh.verts[mesh.tris[i].c].z);
    volume+=(v1.cross(v2)).dot(v3);
  }
  volume=abs(volume)/6.0;
  return volume;
}
}
