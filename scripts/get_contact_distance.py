import rospy
from ll4ma_collision_wrapper.srv import ComputeContactDistance
from geometry_msgs.msg import Pose

if __name__=='__main__':
    #
    rospy.init_node('contact_distance_node')

    # get data

    obj_mesh='/home/mmatak/catkin_ws/src/ll4ma_collision_wrapper/data/bih_data/block_u_mesh_scaled.obj'
    robot_finger_mesh='/home/mmatak/catkin_ws/src/ll4ma_collision_wrapper/data/bih_data/finger.stl'
    des_contact_mesh='/home/mmatak/catkin_ws/src/ll4ma_collision_wrapper/data/bih_data/block_u_des.stl'
    root_dir=''

    obj_pose=Pose()
    obj_pose.position.x=0.0
    obj_pose.position.y=0.0
    obj_pose.position.z=0.0
    obj_pose.orientation.x=0
    obj_pose.orientation.y=0
    obj_pose.orientation.z=0
    obj_pose.orientation.w=1.0
    

    robot_finger_pose=Pose()
    robot_finger_pose.position.x= -0.00
    robot_finger_pose.position.y= -0.039
    robot_finger_pose.position.z= -0.01
    robot_finger_pose.orientation.w=1.0
    
    des_contact_pose=Pose()  # -0.01 -0.05 0.01
    des_contact_pose.position.x=0#-0.01  # 0.054921 0.0728811
    des_contact_pose.position.y=0#-0.05
    des_contact_pose.position.z=0.0
    des_contact_pose.orientation.w=1.0
    
    # 0.0112502175517
    
    
    # call service
    rospy.wait_for_service('/ll4ma_collision_wrapper/compute_mesh_distance')
    print 'found service!'
    get_distance=rospy.ServiceProxy('/ll4ma_collision_wrapper/compute_mesh_distance',ComputeContactDistance)
    resp=get_distance(obj_mesh,robot_finger_mesh,des_contact_mesh,obj_pose,robot_finger_pose,
                      des_contact_pose,root_dir)
    print 'euc,geo,min'
    print resp.euclidean*100.0,resp.geodesic*100.0,resp.min_distance*100.0
    

    # 0.862144921244 0.764133798692 0.0571042866304
