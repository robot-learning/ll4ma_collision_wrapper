# LL4MA\_COLLISION\_WRAPPER

This package wraps many signed distance function libraries to give an easy interface for robot motion planning with geometric constraints.


## Contributors:

-   [Balakumar Sundaralingam](<https://balakumar-s.github.io/>)


## Install

-   Run ''install\_dependencies.sh'' script to install external libraries required to run this package.
-   compile your catkin workspace to compile functions and the demos.


### Using this library for benchmarking in-hand manipulation

-   Follow install instructions
-   ''roslaunch ll4ma\_collision\_wrapper mesh2mesh\_distance.launch''
-   Load your object and robot hand link mesh in ''ll4ma\_collision\_wrapper/scripts/get\_contact\_distance.py''
-   Run ''python get\_contact\_distance.py'' from the scripts folder to output the mesh contact errors.
-   If you are mesh has non-manifold vertices, use meshlab to remove them before running the geodesic computation.
-   To run the demo, copy [bih\_data](https://drive.google.com/open?id=1xnnPryeAx0l9rG5VfJTGeSlFcRahv237) into "ll4ma\_collision\_wrapper/data/bih\_data/"


### Dependencies

-   libccd or KrisLibrary or FCL
-   octomap-ros (sudo apt-get install ros-melodic-octomap-ros)
-   Eigen (tested with Eigen3)
-   PCL (tested with 1.7)
-   fcl (only barebone support, needs contributions)
-   asr_ros_uri <https://github.com/asr-ros/asr_ros_uri>
-   CGAL <https://github.com/CGAL/cgal>
-   Colorized Mesh Display plugin for rviz: <https://github.com/marip8/colorized_mesh_display>
-- Need to pull commit f36b0ccf24b183d2af464c21479a0e364b83f593 in order to make this build


## Supported collision libraries

-   libccd ( A convex collision checking library)
-   FCL
-   KrisLibrary


### Additional functions

-   Convex decomposition (using HACD)


## Citing

This code was developed in parts for the following publications. Cite the below publication if you use this package in your research.

*Sundaralingam B, Hermans T.Geometric In-Hand Regrasp Planning: Alternating Optimization of Finger Gaits and In-Grasp Manipulation. IEEE International Conference on Robotics and Automation (ICRA) 2018.*

```
@InProceedings{sundaralingam-2018icra-regrasp,
 author = {Balakumar Sundaralingam and Tucker Hermans},
 year = 2018,
 title = {{Geometric In-Hand Regrasp Planning: Alternating Optimization of Finger Gaits and In-Grasp Manipulation}},
 booktitle = {{IEEE International Conference on Robotics and Automation (ICRA)}}
    }
```

If you are using the geodesic functions, cite the following publication in addition to the above publication.

```
@Article{cruciani-ral2019-benchmarking-in-hand,
 author = {Silvia Cruciani* and Balakumar Sundaralingam* and Kaiyu Hang and Vikash Kumar and Tucker Hermans and Danica Kragic},
 title = {{Benchmarking In-Hand Manipulation}},
 journal = {IEEE Robotics and Automation Letters (Special Issue: Benchmarking Protocols for Robotic Manipulation)},
 year = {2019},
 url = "https://robot-learning.cs.utah.edu/project/benchmarking_in_hand_manipulation"
}
```
