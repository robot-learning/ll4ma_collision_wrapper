#!/usr/bin/env bash
set -e
# ========================================================================================== #
# Installs ll4ma_collision_wrapper dependencies to a desired filepath. 
#                                                                                            #
#                                                       
# ========================================================================================== #

INSTALL_PATH="$HOME/local"
ROS_VER="melodic"
SRC_PATH="$HOME/pkgs"

#
INSTALL_CCD=true
INSTALL_FCL=true
INSTALL_KRIS=true
# create directories if they are not present already
WORKSPACE=$WS_PATH/

mkdir -p $SRC_PATH
mkdir -p $INSTALL_PATH



# clone libccd:
if $INSTALL_CCD; then
	cd $SRC_PATH && git clone https://github.com/balakumar-s/libccd.git && cd libccd && mkdir build && cd build && cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX=$INSTALL_PATH .. && make -j8 && make install
fi

# clone and install fcl:
if $INSTALL_FCL; then
        sudo apt-get install liboctomap-dev
	cd $SRC_PATH && git clone https://github.com/flexible-collision-library/fcl.git && cd fcl && mkdir build && cd build && cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX=$INSTALL_PATH .. && make -j8 && make install
fi
# clone and install kris:
if $INSTALL_KRIS; then
	cd $SRC_PATH && git clone https://github.com/balakumar-s/KrisLibrary.git && cd KrisLibrary && cmake . && make -j8 && sudo make install
fi
