#include <ll4ma_collision_wrapper/libs/kris_library/collision_checker.h>
#include <future>
namespace kris_ccheck
{
using namespace Meshing;
using namespace Geometry;
class graspEnv : public collisionChecker
{
public:
  graspEnv();
  

  // robot link functions:
  // read from urdf:
  void load_robot_links(vector<string> &arm_link_names,vector<string> &hand_link_names,string &urdf_txt);
 
  void update_arm_state(vector<vector<double>> &link_poses);
  void update_hand_state(vector<vector<double>> &link_poses);

  void update_robot_state(vector<vector<double>> &arm_hand_link_poses);

  void update_robot_state(vector<vector<double>> &arm_link_poses,vector<vector<double>> &hand_link_poses);
  // environment functions:
  // update environment mesh:
  void add_env_meshes(vector<string> &f_names,vector<Eigen::Matrix4d> &T);
  void update_env_poses(vector<vector<double>> &poses, vector<int> &idx);
  // object functions:
  // update object mesh:
  void add_obj_mesh(const string &f_name, Eigen::Matrix4d &T);
  
  void update_obj_pose(vector<double> &pose);
  
  
  // collision checking functions:
  //checking collision between all robot links and the environment
  void arm_env_ccheck(vector<vector<double>> &l_poses, vector<coll_data> &c_data);

  void hand_env_ccheck(vector<vector<double>> &l_poses, vector<coll_data> &c_data);

  void rob_env_ccheck(vector<coll_data> &c_data);

  void arm_object_ccheck(vector<vector<double>> &l_poses, vector<coll_data> &c_data);
  void hand_object_ccheck(vector<vector<double>> &l_poses, vector<coll_data> &c_data);

  void arm_object_ccheck(vector<vector<double>> &l_poses, vector<coll_data> &c_data, vector<int> l_idx);
  void hand_object_ccheck(vector<vector<double>> &l_poses, vector<coll_data> &c_data, vector<int> l_idx);

  void rob_object_ccheck(vector<coll_data> &c_data, vector<int> l_idx,bool collision=true);
  void obj_env_ccheck(coll_data &c_data);

  /** 
   * Computes surface point and normal, given a point in the object frame
   * 
   * @param cpt point in the object frame
   * @param s_pt returned surface point that is closest to cpt
   * @param s_normal returned surface normal
   */
  void obj_surface_normal(vector<double> &cpt, vector<double> &s_pt,vector<double> &s_normal);
  // robot link self collision checking.
  
  int n_links_;
  int n_hand_links_;

  // assuming all contact meshes have the same base frame:
  // TODO: change to have seperate frames for each contact pose
  
  void add_reach_mesh(vector<string> &f_names,Eigen::Matrix4d &T);
  void update_reach_pose(vector<double> &pose);

  void reach_obj_ccheck(vector<coll_data> &c_data);
  void reach_obj_ccheck(coll_data &c_data,int idx);

private:
  
  CollisionMesh* grasp_obj_;
  TriMesh* obj_mesh_;

  
  vector<CollisionMesh*> arm_link_objs_;
  vector<CollisionMesh*> hand_link_objs_;
  vector<CollisionMesh*> env_objs_;

  // reachability meshes:
  vector<CollisionMesh*> reach_objs_;
  
  vector<Transform3d> arm_link_offsets_;
  vector<Transform3d> hand_link_offsets_;
};
}
