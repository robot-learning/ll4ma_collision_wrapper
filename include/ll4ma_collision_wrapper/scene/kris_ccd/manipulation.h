#include <ll4ma_collision_wrapper/libs/kris_ccd/collision_checker.h>
using namespace Meshing;
using namespace Geometry;
namespace kris_ccd_ccheck
{
class manipulationEnv : public collisionChecker
{
public:
  /// Constructor
  manipulationEnv() : collisionChecker() {}

  void load_robot_links(vector<string> &arm_link_names, vector<string> &hand_link_names, string &urdf_text);
  
  // ignore arm joints
  void load_robot_links(vector<string> &hand_link_names,string &urdf_txt);
 
  //void load_robot_contact_links(vector<string> &robot_contact_link_names, string &urdf_text);

  void update_arm_state(vector<vector<double>> &link_poses);
  void update_hand_state(vector<vector<double>> &link_poses);

  void update_robot_state(vector<vector<double>> &arm_hand_link_poses);

  void update_robot_state(vector<vector<double>> &arm_link_poses,vector<vector<double>> &hand_link_poses);
  // environment functions:
  // update environment mesh:
  void update_env_meshes(vector<string> &f_names,vector<Eigen::Matrix4d> &T, vector<bool> cvx_idx={});

  void add_env_meshes(vector<string> &f_names,vector<Eigen::Matrix4d> &T, vector<bool> cvx_idx={})
  {
    update_env_meshes(f_names,T,cvx_idx);
  }
  void update_env_poses(vector<vector<double>> &poses, vector<int> &idx);
  // object functions:
  // update object mesh:
  
  void update_obj_mesh(const string &f_name, Eigen::Matrix4d &T);


  // TODO: choose between update & add
  void add_obj_mesh(const string &f_name, Eigen::Matrix4d &T)
  {
    update_obj_mesh(f_name, T);
  }
  void update_obj_pose(vector<double> &pose);
  
  
  // collision checking functions:
  // self-collision for hand
  void hand_hand_ccheck(vector<coll_data> &c_data,const bool &compute_non_colliding=false);

  //checking collision between all robot links and the environment
  void arm_env_ccheck(vector<vector<double>> &l_poses, vector<coll_data> &c_data);

  void hand_env_ccheck(vector<vector<double>> &l_poses, vector<coll_data> &c_data);

  void rob_env_ccheck(vector<coll_data> &c_data,const bool &compute_non_colliding=false);

  void arm_object_ccheck(vector<vector<double>> &l_poses, vector<coll_data> &c_data);
  void hand_object_ccheck(vector<vector<double>> &l_poses, vector<coll_data> &c_data, bool compute_non_colliding=false);

  void hand_object_ccheck(vector<coll_data> &c_data, bool compute_non_colliding=false);

  void hand_object_ccheck( vector<coll_data> &c_data, const vector<int> &l_idx,bool compute_non_colliding=false);
  void hand_object_ccheck( coll_data &c_data, const int &l_idx,bool compute_non_colliding=false);

  void arm_object_ccheck(vector<vector<double>> &l_poses, vector<coll_data> &c_data, vector<int> l_idx);
  void hand_object_ccheck(vector<vector<double>> &l_poses, vector<coll_data> &c_data, vector<int> l_idx);

  void rob_object_ccheck(vector<coll_data> &c_data, vector<int> l_idx,bool compute_non_colliding=false);
  void obj_env_ccheck(coll_data &c_data);
  /** 
   * Computes surface point and normal, given a point in the object frame
   * 
   * @param cpt point in the object frame
   * @param s_pt returned surface point that is closest to cpt
   * @param s_normal returned surface normal
   */
  void obj_surface_normal(vector<double> &cpt, vector<double> &s_pt,vector<double> &s_normal);
  // robot link self collision checking.
  
  int n_links_;
  int n_hand_links_;

  // assuming all contact meshes have the same base frame:
  // TODO: change to have seperate frames for each contact pose
  
  void update_reach_mesh(vector<string> &f_names,Eigen::Matrix4d &T);
  void update_reach_pose(vector<double> &pose);

  void reach_obj_ccheck(vector<coll_data> &c_data);
  void reach_obj_ccheck(coll_data &c_data,int idx);
  
  //void rob_obj_contact(coll_data &c_data, const int &idx);

  TriMesh get_obj_mesh();
  void get_obj_pose(Eigen::Vector3d &position,Eigen::Vector3d &rpy);

protected:
  CollisionMesh* grasp_obj_;

  /// Stores object mesh
  TriMesh* obj_mesh_;

  /// Stores object mesh transformed to the world
  TriMesh* world_obj_mesh_;

  vector<obj_t> ccd_grasp_obj_;
  
  vector<CollisionMesh*> arm_link_objs_;
  vector<CollisionMesh*> hand_link_objs_;
  vector<CollisionMesh*> contact_link_objs_;

  vector<obj_t> ccd_arm_link_objs_;
  vector<obj_t> ccd_hand_link_objs_;
  vector<obj_t> ccd_contact_link_objs_;
  
  vector<vector<obj_t>> ccd_env_objs_;
  vector<CollisionMesh*> env_objs_;

  
  // reachability meshes:
  vector<CollisionMesh*> reach_objs_;
  vector<obj_t> ccd_reach_objs_;
  
  vector<Transform3d> arm_link_offsets_;
  vector<Transform3d> hand_link_offsets_;

};
}
