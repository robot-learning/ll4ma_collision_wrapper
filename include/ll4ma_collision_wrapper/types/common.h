#ifndef COLLISION_COMMON_H
#define COLLISION_COMMON_H
// Eigen library for vectors and matrices:
#include <Eigen/Dense>
#include <Eigen/Geometry>


struct coll_data
{
  bool collision;// collision if true
  Eigen::Vector3d p1;// closest point on object 1 in world frame
  Eigen::Vector3d p2;// closest point on object 2 in world frame
  Eigen::Vector3d relp1; // closest point on object 1 in object 1 frame
  Eigen::Vector3d relp2; // closest point on object 2 in object 2 frame
  double distance;// signed distance between obj1 and obj2 (-ve when in collision)
  bool success; // true if computation succeeded. false if computation failed. 
};
#endif
