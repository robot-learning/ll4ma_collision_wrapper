#include <KrisLibrary/meshing/Meshing.h>
#include <colorized_mesh_display/ColorizedMeshStamped.h>
namespace ll4ma_collision_wrapper
{
ColorizedMesh get_viz_mesh(Meshing::TriMesh mesh, vector<double> heatmap=vector<double>);
}
