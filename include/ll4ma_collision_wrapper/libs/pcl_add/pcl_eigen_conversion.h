// Contains simple functions to convert pcl data to eigen

// debug header:
#include <ll4ma_collision_wrapper/debug.h>

#include<iostream>

// Pcl library headers:
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/conditional_removal.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <boost/format.hpp>

#define FILE_OPEN_ERROR(fname) throw runtime_error( "couldn't open " + fname)
// Eigen library for vectors and matrices:
#include <Eigen/Dense>
#include <Eigen/Geometry>

using namespace std;


// Convex decomposition header files:
#include "sphere_sampling.hpp"
#include "convexdecomp.hpp"
// pcl_ros header:
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/extract_clusters.h>


#include <pcl/io/vtk_lib_io.h>
#include <pcl/common/transforms.h>
#include <vtkVersion.h>
#include <vtkPLYReader.h>
#include <vtkOBJReader.h>
#include <vtkTriangle.h>
#include <vtkTriangleFilter.h>
#include <vtkPolyDataMapper.h>
#include <pcl/filters/voxel_grid.h>
// ros tf:
#include <tf/transform_listener.h>
#include <ll4ma_collision_wrapper/libs/pcl_add/tinyply.h>

#include <sstream>
#include <fstream>
#include <iostream>

// octree:
#include <pcl/octree/octree_pointcloud.h>

#include <octomap/octomap.h>

#include <visualization_msgs/MarkerArray.h>
#include <geometry_msgs/Point.h>

#include <octomap_ros/conversions.h>
#include <octomap/octomap.h>
#include <octomap/OcTreeKey.h>

#include <octomap_msgs/Octomap.h>
#include <octomap/ColorOcTree.h>
#include <octomap_msgs/conversions.h>


namespace pcl_eigen
{
using namespace Eigen;
using namespace pcl;

// TODO: Make this class as a template to work with any pcl::point type
class pclEigen
{
 public:
  void init() const;
  /// load ply file into eigen vector of vertices and face indices:
  void loadPLY(const std::string&, vector<float> &verts, vector<float> &norms, vector<uint32_t> &faces) const;

  /// load ply file into eigen vector of vector3D:
  void loadPLY(const std::string&,vector<Vector3d>&) const;
  bool loadMesh(const std::string& mesh_file, vector<Vector3d> &points) const;

  bool meshSampling(pcl::PolygonMesh &in_mesh,  PointCloud<PointXYZ>::Ptr &out_cloud) const;
  bool pointcloudFromMesh(const std::string& mesh_file, PointCloud<PointXYZ>::Ptr &out_cloud) const;

  void convex_decomp(const std::string& mesh_file, vector<vector<Vector3d>> &out_cluster,const float &vsize=0.01) const;
  void convex_decomp(const vector<Vector3d> &verts, vector<vector<Vector3d>>& objects,const float &vsize) const;

  /// load ply into pointcloud
  void loadPLY(const std::string&, PointCloud<pcl::PointXYZ>::Ptr) const;

  void loadPCD(const std::string&, PointCloud<pcl::PointXYZ>::Ptr) const;

  /// removing Nans:
  void removenans(pcl::PCLPointCloud2&,const float&) const;
  void removenans(PointCloud<PointXYZ>::Ptr cloud) const;
  /// downsample cloud using voxelgrid filter:
  void downsample_cloud(PointCloud<pcl::PointXYZ>::Ptr, const float&) const;

  /// transform pointcloud to a different frame:
  void transform_pointcloud(PointCloud<PointXYZ>::Ptr,const string&,const tf::TransformListener&) const;
  void transform_pointcloud(PointCloud<PointXYZ>::Ptr cloud, const geometry_msgs::Transform &tf_l) const;

  void transform_verts(vector<Vector3d> &pts, const Eigen::Matrix4d &T, vector<Vector3d> &transformed_pts) const;
  /// Remove points out of the bounding box:
  void bound_cloud(PointCloud<PointXYZ>::Ptr,const vector<float>&,const vector<float>&) const;

  /// convex decomposition on pointcloud using HACD:
  /// returns an array of vec3 points
  /// TODO: 1. Add arg to return colored pointcloud
  void convex_decomp(const PointCloud<pcl::PointXYZ>::Ptr, vector<vector<Vector3d>>&,const float&) const;

  /// Ransac helper functions:
  void find_table(PointCloud<PointXYZ>::Ptr in_cloud, PointCloud<PointXYZ>::Ptr table_cloud, PointCloud<PointXYZ>::Ptr outliers=NULL) const ;
  void find_cluster(PointCloud<PointXYZ>::Ptr in_cloud, PointCloud<PointXYZ>::Ptr obj_cloud, PointCloud<PointXYZ>::Ptr outliers=NULL) const;

  /// generate octree from pointcloud:
  void generate_octree(PointCloud<PointXYZ>::Ptr in_cloud,pcl::octree::OctreePointCloud<PointXYZ> &out_tree);

  void get_marker_octree(octomap::OcTree* in_tree,visualization_msgs::MarkerArray &occupiedNodesVis,string frame="map");
  void loadPCLOctree(PointCloud<PointXYZ>::Ptr env_cloud, octomap::OcTree* tree);

  /// Load binvox file into a octree, useful for generating octree for meshes.
  void loadBinvoxOctree(const string &bvox_file, octomap::OcTree* tree) const; 

private:
  //
  inline void randPSurface (vtkPolyData * polydata, std::vector<double> * cumulativeAreas, double totalArea, Eigen::Vector3f& p) const;
  inline double uniform_deviate (int seed) const;
  inline void randomPointTriangle (float a1, float a2, float a3, float b1, float b2, float b3, float c1, float c2, float c3,                     float r1, float r2, Eigen::Vector3f& p) const;



};

}
