#ifndef COLL_OBJ_TEMP_H
#define COLL_OBJ_TEMP_H
struct obj_t
{
  ccd_vec3_t pos;
  
  ccd_quat_t quat;
  vector<Eigen::Vector3d> vertices;
};

struct collision_data
{
  bool collision;// collision if true
  Eigen::Vector3d point;// closest point on object
  Eigen::Vector3d dir;// direction of vector from point to object 2
  float distance;// distance from closest point to object 2
};
struct obj_sphere
{
  double radius;
  vector<double> centre;
};
#endif
