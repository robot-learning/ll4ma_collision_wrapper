// Computes signed distance between two convex meshes.
#ifndef CCD_COLLISION_H
#define CCD_COLLISION_H
// debug header:
#include <ll4ma_collision_wrapper/debug.h>

#include<iostream>

// Importing libccd:
#include <ccd/ccd.h>
#include <ccd/quat.h>
#include <ccd/simplex.h>

// Pcl eigen library header:
#include <ll4ma_collision_wrapper/libs/pcl_add/pcl_eigen_conversion.h>

// collision checking object template:
#include "collision_obj_template.h"

#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>

// ros tf:
#include <tf/transform_listener.h>

#include <ll4ma_collision_wrapper/types/common.h>

//#include <ll4ma_collision_wrapper/libs/ccd/support_fn.h>

// string, cout..
using namespace std;
using namespace Eigen;
using namespace pcl;
using namespace pcl_eigen;

// pcl
namespace ccd_ccheck
{
class collisionChecker
{

 public:
  /// Constructor- initializes pcl_eigen object
  collisionChecker();

  /// intializes ccd 
  void initCCD();
  /** 
   * Loads vertices of convex object from file(stl)
   * 
   * @param file_name 
   * @param obj 
   */
  
  // load single mesh:
  void loadModel(const string &file_name, vector<Vector3d> &obj) const;

  void loadMeshShape(const string &file_name,Eigen::Matrix4d T, vector<Vector3d> &obj) const;
  void loadMeshShape(const string &file_name,Eigen::Matrix4d T, vector<obj_t> &obj) const;

  void loadCvxMeshShape(const string &file_name,Eigen::Matrix4d T, obj_t &obj) const;


  /** 
   * Creates collision object from vertices
   * 
   * @param mesh  input vertices
   * @param T  pose of vertices 
   * @param cvx_model output collision model
   */
  // Create collision object:
  void createCollObj(vector<Vector3d> &mesh,const Eigen::Matrix4d &T_offset, obj_t &cvx_model,const Eigen::Matrix4d &T_pose=Eigen::Matrix4d::Identity()) const;

  
  //void loadPCDFile(const string &file_name, Eigen::Matrix4d &T,obj_t &cvx_model);

  // Create collision objects from pointcloud through convex decomposition:
  void loadPointCloud(PointCloud<PointXYZ>::Ptr in_cloud, float &vsize, vector<vector<Vector3d>> &obj_arr) const;
  
  // change the pose cvx_object:
  void updatePose(obj_t &obj, vector<double> &pose) const;
  void updatePose(vector<obj_t> &obj_arr, vector<vector<double>> &poses) const;
  void updatePose(vector<obj_t> &obj_arr, vector<double> &pose) const;


  // Signed distance computation:
  void signedDistance(const obj_t&,const obj_t&,coll_data &c_dat) const;
  // Perform narrow collision checking between object and a number of objects in an array:
  void signedDistance(const obj_t&,const vector<obj_t>&,vector<coll_data> &c_data) const;


  // Detect collision
  void checkCollision(const obj_t&,const obj_t&,coll_data &c_data) const;

  void checkCollision(const obj_t&, const vector<obj_t>&,coll_data &c_data) const;
  void checkCollision(const vector<obj_t> &obj1,const vector<obj_t> &obj2, coll_data &c_data) const;

  /// checks if  point pt is inside convex object 
  void checkCollision(const obj_t &obj1, const Eigen::Vector3d pt, coll_data &c_data) const;

  
  
  
  
  // libccd 

  int min_env_size;// for filterin

  //protected:
  // pcl helper class object:
  pclEigen _pcl_eigen;

  ccd_t ccd_;

private:

  /*
  // ccd functions:
  ccd_real_t _ccdDist(const void*, const void*,const ccd_t*,ccd_simplex_t*,ccd_vec3_t*, ccd_vec3_t*) const;
  ccd_real_t simplexReduceToTriangle(ccd_simplex_t*,ccd_real_t,ccd_vec3_t*) const;
  int __ccdGJK(const void*, const void*, const ccd_t*, ccd_simplex_t*) const;
  int doSimplex(ccd_simplex_t*, ccd_vec3_t*) const;
  int doSimplex2(ccd_simplex_t*, ccd_vec3_t*) const;
  int doSimplex3(ccd_simplex_t*, ccd_vec3_t*) const;
  int doSimplex4(ccd_simplex_t*, ccd_vec3_t*) const;
  void tripleCross(const ccd_vec3_t*, const ccd_vec3_t*,const ccd_vec3_t*, ccd_vec3_t*) const;
  */
  
};
}
#endif
