#include <ll4ma_collision_wrapper/debug.h>
#include <KrisLibrary/meshing/Meshing.h>
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <Eigen/SparseCore>
#include <Eigen/SparseCholesky>

// internal mesh types:
#include <ll4ma_collision_wrapper/libs/geodesic/Mesh.h>
#include <ll4ma_collision_wrapper/libs/geodesic/MeshIO.h>

namespace geodesic
{
class HeatGeodesic : public Mesh
{
public:
  HeatGeodesic():Mesh(){};

  HeatGeodesic(const Meshing::TriMesh &mesh);

  bool read_trimesh(const Meshing::TriMesh &mesh);
  
  double get_distance(const int &dest_vidx);

  double get_distance(const Eigen::Vector3d pt);

  Eigen::VectorXd computeEuclidean(const int vIdx);

protected:
  Meshing::TriMesh mesh_;
  std::vector<double> v_distances;

  // internal functions:
  
};
}
