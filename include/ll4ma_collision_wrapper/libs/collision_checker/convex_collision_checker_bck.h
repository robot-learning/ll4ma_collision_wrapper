// Computes signed distance between two convex meshes.
#define COLLISION_H
// debug header:
#include "debug.h"

#include<iostream>

// Importing libccd:
#include <ccd/ccd.h>
#include <ccd/quat.h>
#include <ccd/simplex.h>

// Pcl eigen library header:
#include <ll4ma_collision_wrapper/pcl_add/pcl_eigen_conversion.h>

// collision checking object template:
#include "collision_obj_template.h"

#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
// ROS service for collision checking
#include <ll4ma_collision_wrapper/collision_checker_msg.h>
// ROS service for updating env pointcloud flag
#include <ll4ma_collision_wrapper/update_env.h>

// ros tf:
#include <tf/transform_listener.h>

// Grasp object service:
//#include <ll4ma_collision_wrapper/SegmentGraspObject.h>
// vectors and matrices
using namespace Eigen;
// string, cout..
using namespace std;
// pcl
using namespace pcl;
class collisionChecker
{
 public:
  // Constructor- initialize with world frame name, number of joints,
  collisionChecker();

  // Load robot links
  void load_robot_meshes(vector<string>);

  void create_robot_coll_objects();

  void create_env_coll_objects();
  void update_env_coll_objects(vector<double> pose);
  void update_env_pose(vector<double> pose);
  // Add object cloud for grasp :
  bool add_obj_cloud(string);
  bool add_obj_cloud(string,vector<double> pose);

  bool add_obj_cloud(ros::ServiceClient&);
  //void clear_coll_objects();

  // Load robot tf
  void update_robot_state(vector<vector<double>>);
  void update_robot_state(vector<vector<double>> poses,vector<int> links_idx); 
  //vector<obj_t>  update_robot_state(vector<vector<double>>);
  // Load world pointcloud from file
  void load_world_from_file(string);

  // TODO:Load world pointcloud from rostopic
  
  // Perform HACD(convex decomposition) on pointcloud
  void compute_convex_decomp(float);

  // Create collision objects:
  obj_t loadMeshModel(vector<Vector3d>,vector<double>);

  // Signed distance computation:
  collision_data signed_distance(obj_t*,obj_t*);
  //collision_data signed_distance(obj_t,obj_t) const;

  // TODO:Perform broad collision checking to get objects close to robot

  // Perform narrow collision checking between object and a number of environment objects
  vector<collision_data> compute_signed_distance(obj_t*,vector<obj_t>&);
  //vector<collision_data> compute_signed_distance(obj_t,vector<obj_t>) const;
  
  //TODO: Perform collision checking between robot links


  void initialize_robot_variables();
  
  /** Functions for use in manipulator collision avoidance **/
  // Initializing the environment:
  void initialize_robot_env_state(int,vector<string>&, string);

  // Function overloading to allow for sending pointcloud directly:
  void initialize_robot_env_state(int,vector<string>&,PointCloud<PointXYZ>::Ptr);

  void initialize_robot_state(int,vector<string>&);

  // Check collisions for all robot links to all objects
  vector<vector<collision_data>> check_collisions(vector<vector<double>>);
  vector<vector<collision_data>> check_collisions(vector<vector<double>> r_poses,vector<int> link_idx);
  vector<vector<collision_data>> check_self_collisions(vector<vector<double>> r_poses,vector<int> link_idx,vector<int> coll_idx);
  // Checks collisions between points and robot links
  vector<collision_data> check_pt_collisions(vector<vector<double>>,vector<int> link_idx);
  

  //void support_convex(const void *_obj, const ccd_vec3_t *dir_, ccd_vec3_t *vec);

  // View environment objects:
  void view_env_objects(int);
  // publish to ros
  void publish_env_objects(int,int,char**);
  void publish_env_objects(int, ros::Publisher&);
  void publish_robot_objects();
  void create_publisher(ros::NodeHandle);
  bool debug_robot_viz(ll4ma_collision_wrapper::collision_checker_msg::Request&,ll4ma_collision_wrapper::collision_checker_msg::Response&);
  // ros service for collision checking
  bool check_collisions(ll4ma_collision_wrapper::collision_checker_msg::Request&,ll4ma_collision_wrapper::collision_checker_msg::Response&);

  // ros service for updating env with pointcloud from topic:
  bool update_env_data(sensor_msgs::PointCloud2 &in_cloud, geometry_msgs::Transform &tf_l);
  bool update_env_cloud(ll4ma_collision_wrapper::update_env::Request& ,ll4ma_collision_wrapper::update_env::Response&);
 void pcl_callback(sensor_msgs::PointCloud2 );
 
  ccd_real_t _ccdDist(const void*, const void*,const ccd_t*,ccd_simplex_t*,ccd_vec3_t*, ccd_vec3_t*);
  ccd_real_t simplexReduceToTriangle(ccd_simplex_t*,ccd_real_t,ccd_vec3_t*);

  int __ccdGJK(const void*, const void*, const ccd_t*, ccd_simplex_t*);
  //int __ccdGJK(const void*, const void*, const ccd_t*, ccd_simplex_t*) const;

  int doSimplex(ccd_simplex_t*, ccd_vec3_t*);
  //int doSimplex(ccd_simplex_t*, ccd_vec3_t*) const;
  int doSimplex2(ccd_simplex_t*, ccd_vec3_t*);
  //int doSimplex2(ccd_simplex_t*, ccd_vec3_t*) const;
  int doSimplex3(ccd_simplex_t*, ccd_vec3_t*);
  int doSimplex4(ccd_simplex_t*, ccd_vec3_t*);
  void tripleCross(const ccd_vec3_t*, const ccd_vec3_t*,const ccd_vec3_t*, ccd_vec3_t*);

  // This is stupid, find another way to send publishers and listeners
  ros::Publisher pub_;
  tf::TransformListener* tf_listen;
  
  vector<obj_t> _robot_coll_objects;
  mutable vector<vector<double>> _link_poses;// [x y z wx wy wz ww] position , quaternion
  // libccd 
  ccd_t ccd;
  int min_env_size;// for filtering
  ros::ServiceClient gd_client; 
 private:
  //mutable ccd_real_t depth;
  // mutable ccd_vec3_t dir, point_pos,dir_,dir_c;
  //mutable ccd_simplex_t simplex,simplex_c;
  pclEigen _pcl_eigen;
  // Environment Pointcloud
  pcl::PointCloud<PointXYZ>::Ptr _env_cloud;//(new PointCloud<PointXYZ>);
  pcl::PointCloud<PointXYZ>::Ptr _t_cloud;//(new PointCloud<PointXYZ>);
  // Objects are stored as array of vector arrays: 
  vector<vector<Vector3d>> _objects;
  // robot_link_meshes are made convex using meshlab and are loaded as vector array
  vector<vector<Vector3d>> _robot_links;

  // robot link poses:
 
  // environment objects' poses:
  vector<vector<double>> _env_poses; // [x y z wx wy wz ww] position , quaternion
  
  // robot objects:


  vector<obj_t> _env_coll_objects;

  int _DOF;
  
  obj_t _obj_temp;

  
  ros::Publisher pub;//=nh.advertise<pcl::PointCloud<PointXYZRGB>>("/collision_checker/robot_links",1);

  
};
