// TODO : wrap inside a class
void support_convex(const void *_obj, const ccd_vec3_t *dir_, ccd_vec3_t *vec)
{
  obj_t *obj=(obj_t *)_obj;
  
  ccd_vec3_t dir, p;
  ccd_real_t maxdot, dot;
  int i;

  //const auto& center = c->convex->center;
  ccd_quat_t qinv;
  
  ccdVec3Copy(&dir, dir_);
  ccdQuatInvert2(&qinv, &obj->quat);
  ccdQuatRotVec(&dir, &qinv);

  maxdot = -CCD_REAL_MAX;
 

  for(i = 0; i < obj->vertices.size(); ++i)
  {
    ccdVec3Set(&p, obj->vertices[i][0], obj->vertices[i][1] ,obj->vertices[i][2]);
    dot = ccdVec3Dot(&dir, &p);

    if(dot > maxdot)
    {
      ccdVec3Set(vec, obj->vertices[i][0], obj->vertices[i][1] ,obj->vertices[i][2]);
      maxdot = dot;
    }

  }

  // transform support vertex
  ccdQuatRotVec(vec, &obj->quat);
  ccdVec3Add(vec, &obj->pos);
}

void center(const void *_obj, ccd_vec3_t *center)
{
  obj_t *obj = (obj_t *)_obj;
  ccdVec3Copy(center, &obj->pos);
}
