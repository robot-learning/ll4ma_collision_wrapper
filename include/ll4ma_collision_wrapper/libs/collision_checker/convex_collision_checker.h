// Computes signed distance between two convex meshes.
#define COLLISION_H
// debug header:
#include "debug.h"

#include<iostream>

// Importing libccd:
#include <ccd/ccd.h>
#include <ccd/quat.h>
#include <ccd/simplex.h>

// Pcl eigen library header:
#include <ll4ma_collision_wrapper/libs/pcl_add/pcl_eigen_conversion.h>

// collision checking object template:
#include "collision_obj_template.h"

#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
// ROS service for collision checking
#include <ll4ma_collision_wrapper/collision_checker_msg.h>
// ROS service for updating env pointcloud flag
#include <ll4ma_collision_wrapper/update_env.h>

// ros tf:
#include <tf/transform_listener.h>

// string, cout..
using namespace std;
// pcl
namespace libccd
{
class collisionChecker
{
  using namespace Eigen;
  using namespace pcl;
  using namespace pcl_eigen;

 public:
  // Constructor- initializes pcl_eigen object
  collisionChecker();
  void init_ccd(ccd_t &ccd) const;
  // load single mesh:
  void load_mesh(string &file_name, vector<Vector3d> &obj);
  // Load meshes from file_names and stores in vector3d array
  void load_meshes(vector<string> &file_names,  vector<vector<Vector3d>> &obj_arr);
  
  // Create collision object:
  void loadMeshModel(vector<Vector3d> &mesh,vector<double> &pose, obj_t &cvx_model);

  // Create collision objects from pointcloud through convex decomposition:
  void load_PCL(PointCloud<PointXYZ>::Ptr in_cloud, float &vsize, vector<vector<Vector3d>> &obj_arr);
  // create collision objects from meshes and their poses:
  void create_coll_object(vector<Vector3d> &mesh_object, vector<double> &mesh_pose, obj_t &coll_object);

  void create_coll_objects(vector<vector<Vector3d>> &mesh_objects, vector<vector<double>> &mesh_poses, vector<obj_t> &coll_objects);

  void create_coll_objects(vector<vector<Vector3d>> &mesh_objects, vector<obj_t> &coll_objects);

  // change the pose cvx_object:
  
  void update_obj_pose(obj_t &obj, vector<double> &pose);
  void update_poses(vector<obj_t> &obj_arr, vector<vector<double>> &poses);

  void update_poses(vector<obj_t> &obj_arr, vector<double> &pose);


  // Signed distance computation:
  void signed_distance(const obj_t&,const obj_t&, const ccd_t&,collision_data &c_dat) const;


  // Perform broad collision checking 
  void detect_collision(const obj_t&,const obj_t&,const ccd_t&,bool &collision) const;

  void detect_collisions(const obj_t&, const vector<obj_t>&, const ccd_t&,bool &collision) const;

  // Perform narrow collision checking between object and a number of objects in an array:
  void signed_distance_arr(const obj_t&,const vector<obj_t>&, const ccd_t&,vector<collision_data> &c_dat_arr) const;
  

  // Check collisions between two cvx_objects:
  void check_collision(obj_t &obj_a, obj_t &obj_b, const ccd_t&,collision_data &c_data) const;
  
  // Checks collisions between points and a cvx_object
  void check_pt_collision(vector<Vector3d> &points, obj_t &cvx_obj, const ccd_t&,collision_data &c_data);
  
  
  // libccd 

  int min_env_size;// for filterin

  //protected:
  // pcl helper class object:
  pclEigen _pcl_eigen;
  
private:
  
  // ccd functions:
  ccd_real_t _ccdDist(const void*, const void*,const ccd_t*,ccd_simplex_t*,ccd_vec3_t*, ccd_vec3_t*) const;
  ccd_real_t simplexReduceToTriangle(ccd_simplex_t*,ccd_real_t,ccd_vec3_t*) const;
  int __ccdGJK(const void*, const void*, const ccd_t*, ccd_simplex_t*) const;
  int doSimplex(ccd_simplex_t*, ccd_vec3_t*) const;
  int doSimplex2(ccd_simplex_t*, ccd_vec3_t*) const;
  int doSimplex3(ccd_simplex_t*, ccd_vec3_t*) const;
  int doSimplex4(ccd_simplex_t*, ccd_vec3_t*) const;
  void tripleCross(const ccd_vec3_t*, const ccd_vec3_t*,const ccd_vec3_t*, ccd_vec3_t*) const;

  
};
}
