#include <iostream>
#include <ll4ma_collision_wrapper/debug.h>
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <KrisLibrary/meshing/Meshing.h>

using namespace std;
namespace cvxhull
{
class CvxHull
{
public:
  CvxHull(){};
  // load points
  //bool load_verts(const vector<Eigen::Vector3d> &pts);
  bool compute_delaunay(const vector<Eigen::Vector3d> &verts,Meshing::TriMesh &cvx_mesh) const ;

  bool compute_hull(const vector<Eigen::Vector3d> &verts, Meshing::TriMesh &cvx_mesh) const;
  double compute_volume(const Meshing::TriMesh &mesh) const;
  
  
};
}
