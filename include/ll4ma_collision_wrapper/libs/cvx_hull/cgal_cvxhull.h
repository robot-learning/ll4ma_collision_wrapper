#include <iostream>
#include <ll4ma_collision_wrapper/debug.h>
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <KrisLibrary/meshing/Meshing.h>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>

#include <CGAL/Polyhedron_3.h>
//#include <CGAL/Surface_mesh.h>
#include <CGAL/convex_hull_3.h>
#include <vector>
#include <fstream>


//include <OpenMesh/Core/IO/MeshIO.hh>
//#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
//#include <CGAL/boost/graph/graph_traits_TriMesh_ArrayKernelT.h>
//typedef CGAL::Exact_predicates_inexact_constructions_kernel  K;
//typedef CGAL::Polyhedron_3<K>                     Polyhedron_3;
//typedef K::Point_3                                Point_3;
//typedef CGAL::Surface_mesh<Point_3>               Surface_mesh;


using namespace std;
namespace cgal_cvxhull
{
class CvxHull
{
public:
  CvxHull(){};
  // load points
  //bool load_verts(const vector<Eigen::Vector3d> &pts);
  //bool compute_delaunay(const vector<Eigen::Vector3d> &verts,Meshing::TriMesh &cvx_mesh) const ;

  bool compute_hull(const vector<Eigen::Vector3d> &verts, Meshing::TriMesh &cvx_mesh) const;
  double compute_volume(const Meshing::TriMesh &mesh) const;
  
  
};
}
