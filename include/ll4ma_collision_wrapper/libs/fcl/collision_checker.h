// debug header:
#include <ll4ma_collision_wrapper/debug.h>

#include<iostream>

// Pcl eigen library header:
#include <ll4ma_collision_wrapper/libs/pcl_add/pcl_eigen_conversion.h>


#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>

// ROS service for collision checking
#include <ll4ma_collision_wrapper/collision_checker_msg.h>
// ROS service for updating env pointcloud flag
#include <ll4ma_collision_wrapper/update_env.h>

// ros tf:
#include <tf/transform_listener.h>

// fcl:
#include <fcl/common/types.h>
#include <fcl/math/triangle.h>
#include <fcl/geometry/bvh/BVH_model.h>
#include <fcl/geometry/collision_geometry.h>
#include <fcl/geometry/octree/octree.h>
#include <fcl/math/bv/OBBRSS.h>
#include <fcl/math/bv/OBB.h>

#include <fcl/narrowphase/collision.h>
#include <fcl/narrowphase/distance.h>
#include <fcl/narrowphase/collision_object.h>
#include <fcl/narrowphase/collision_result.h>

#include <fcl/common/unused.h>

#include <fcl/math/constants.h>
#include <fcl/geometry/geometric_shape_to_BVH_model.h>

#include <fcl/broadphase/broadphase_bruteforce.h>
#include <fcl/broadphase/broadphase_spatialhash.h>
#include <fcl/broadphase/broadphase_SaP.h>
#include <fcl/broadphase/broadphase_SSaP.h>
#include <fcl/broadphase/broadphase_interval_tree.h>
#include <fcl/broadphase/broadphase_dynamic_AABB_tree.h>
#include <fcl/broadphase/broadphase_dynamic_AABB_tree_array.h>


// urdf parser:
#include <urdf/model.h>

// collision data types:
#include <ll4ma_collision_wrapper/types/common.h>

// Grasp object service:
//#include <ll4ma_collision_wrapper/SegmentGraspObject.h>
// vectors and matrices
using namespace Eigen;
// string, cout..
using namespace std;
// pcl
using namespace pcl;
using namespace fcl;
using namespace pcl_eigen;
typedef fcl::BVHModel<fcl::OBBRSS<double>> Model;

namespace fcl_ccheck
{
class collisionChecker
{
 public:
  // Constructor- initializes pcl_eigen object
  collisionChecker();

  // laod primitive shape collision object:
  CollisionObjectd* loadCylinderShape(double radius, double lz,Matrix4d &T);
  CollisionObjectd* loadSphereShape(double radius,Matrix4d &T);
  CollisionObjectd* loadBoxShape(double lx, double ly, double lz, Matrix4d &T);

  // load poincloud as an octomap collision object:
  vector<CollisionObjectd*> loadOctreeCollObj(octomap::OcTree* in_tree);

  // check collisions between two primitive shapes:
  //void checkCollision(CollisionObjectd* obj_1, CollisionObjectd* obj_2, CollisionResultd &result);
  //void signedDistance(CollisionObjectd* obj_1, CollisionObjectd* obj_2, DistanceResultd &result);

  void loadModelPLY(const string &file_name, Model* obj);
  CollisionObjectd* createCollObj(Model* obj, Matrix4d &T);
  void checkCollision(CollisionObjectd* obj_1, CollisionObjectd* obj_2,CollisionResultd &result);
  void checkCollision(CollisionObjectd* obj_1, CollisionObjectd* obj_2,coll_data &result);
  
  void signedDistance(CollisionObjectd* obj_1, CollisionObjectd* obj_2,DistanceResultd &result);
  void signedDistance(CollisionObjectd* obj_1, CollisionObjectd* obj_2,coll_data &result);

  
  void generateBoxesFromOctomap(std::vector<CollisionObject<double>*>& boxes, OcTree<double> tree);

  // Octree collision checking:
  void checkCollision(CollisionObjectd* obj_1,vector<CollisionObjectd*> env_arr, CollisionResultd &result);
  void checkCollision(CollisionObjectd* obj_1,vector<CollisionObjectd*> env_arr, coll_data &result);

  void signedDistance(CollisionObjectd* obj_1,vector<CollisionObjectd*> env_arr, DistanceResultd &result);
  
  void signedDistance(CollisionObjectd* obj_1, vector<CollisionObjectd*> obj_2, vector<DistanceResultd> &result);

  void signedDistance(CollisionObjectd* obj_1,vector<CollisionObjectd*> env_arr, coll_data &result);
  
  void signedDistance(CollisionObjectd* obj_1,vector<CollisionObjectd*> env_arr, vector<coll_data> &result);
  

  // update a collision object pose:
  void update_pose(CollisionObjectd* obj, vector<double> &pose);
  
  void update_pose(CollisionObjectd* obj, Transform3d &pose);

  void update_pose(vector<CollisionObjectd*> &obj, vector<vector<double>> &pose);
  void update_pose(vector<CollisionObjectd*> &obj, vector<double> &pose);
  void update_pose(vector<CollisionObjectd*> &obj, vector<Transform3d> &pose);

  pclEigen _pcl_eigen;

  // Create collision object from octree
  //void loadOcTreeModel(OcTree<Model> tree, Model* obj);
  // Read primitive shapes from urdf:
  void readLinkShapes(string &urdf_txt,vector<string> &link_names, vector<CollisionObjectd*> &c_objs, vector<Transform3d> &link_offsets);

  void store_offsets(vector<CollisionObjectd*> &obj, vector<Transform3d> &offsets);

  void add_offsets(Transform3d &offset, vector<double> &pose, Transform3d &out_pose);

  void add_offsets(vector<Transform3d> &offsets, vector<double> &pose, vector<Transform3d> &out_poses);


  void add_offsets(vector<Transform3d> &offsets, vector<vector<double>> &poses, vector<Transform3d> &out_poses);
  
};
}
