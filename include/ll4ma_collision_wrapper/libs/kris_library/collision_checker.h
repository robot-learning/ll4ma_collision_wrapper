#ifndef KRIS_COLLISION_H
#define KRIS_COLLISION_H
// Eigen library for vectors and matrices:
#include <Eigen/Dense>
#include <Eigen/Geometry>


// debug header:
#include <ll4ma_collision_wrapper/debug.h>

#include<iostream>

// Pcl eigen library header:
//#include <ll4ma_collision_wrapper/libs/pcl_add/pcl_eigen_conversion.h>


#include <ros/ros.h>
//#include <pcl_ros/point_cloud.h>
//#include <pcl/point_types.h>

// ROS service for collision checking
//#include <ll4ma_collision_wrapper/collision_checker_msg.h>
// ROS service for updating env pointcloud flag
//#include <ll4ma_collision_wrapper/update_env.h>

// ros tf:
#include <tf/transform_listener.h>

// kris library 
#include<KrisLibrary/meshing/Meshing.h>
#include<KrisLibrary/meshing/IO.h>

#include<KrisLibrary/geometry/AnyGeometry.h>
#include<KrisLibrary/math3d/rotation.h>
#include<KrisLibrary/math3d/Polygon3D.h>

#include<KrisLibrary/math3d/geometry3d.h>
#include<KrisLibrary/geometry/Conversions.h>
#include<KrisLibrary/meshing/PointCloud.h>

// urdf parser:
#include <urdf/model.h>

// collision data types:
#include <ll4ma_collision_wrapper/types/common.h>

#include <ll4ma_collision_wrapper/libs/kris_library/common.h>
#include <ros_uri/ros_uri.hpp>
// Grasp object service:
//#include <ll4ma_collision_wrapper/SegmentGraspObject.h>
// vectors and matrices
// string, cout..
using namespace std;
// pcl
//using namespace pcl;

//using namespace Eigen;

namespace kris_ccheck
{
class collisionChecker
{
 public:
  /// Constructor- initializes pcl_eigen object
  collisionChecker();


  // load poincloud as an octomap collision object:
  //vector<CollisionObjectd*> loadOctreeCollObj(octomap::OcTree* in_tree);

  // check collisions between two primitive shapes:
  //void checkCollision(CollisionObjectd* obj_1, CollisionObjectd* obj_2, CollisionResultd &result);
  //void signedDistance(CollisionObjectd* obj_1, CollisionObjectd* obj_2, DistanceResultd &result);
  Geometry::CollisionMesh* loadCylinderShape(double radius, double lz,Eigen::Matrix4d &T);
  Geometry::CollisionMesh* loadSphereShape(double radius,Eigen::Matrix4d &T);
  Geometry::CollisionMesh* loadBoxShape(double lx, double ly, double lz, Eigen::Matrix4d &T);
  Geometry::CollisionMesh* loadMeshShape(const string &file_name, Eigen::Matrix4d &T);

  void loadCylinderShape(double radius, double lz,Eigen::Matrix4d &T,  Meshing::TriMesh &out);
  void loadSphereShape(double radius,Eigen::Matrix4d &T, Meshing::TriMesh &out);
  void loadBoxShape(double lx, double ly, double lz, Eigen::Matrix4d &T, Meshing::TriMesh &out);
  void loadMeshShape(const string &file_name, Eigen::Matrix4d &T, Meshing::TriMesh &out);

  
  /// Load pointcloud from file as collision object:
  Geometry::CollisionMesh* loadPCDFile(const string &file_name,Eigen::Matrix4d &T);
  //CollisionMesh* loadPCDAscii(const string &pcd_ascii,Eigen::Matrix4d &T);
  
  void loadModel(const string &file_name, Meshing::TriMesh* obj);
  
  
  void checkCollision(  Geometry::CollisionMesh* obj_1,   Geometry::CollisionMesh* obj_2,coll_data &result);
  
  void signedDistance(  Geometry::CollisionMesh* obj_1,   Geometry::CollisionMesh* obj_2,coll_data &result,bool compute_non_colliding=false);

  Geometry::CollisionMesh* createCollObj( Meshing::TriMesh* obj, const Eigen::Matrix4d &T_offset, const Eigen::Matrix4d &T_pose=Eigen::Matrix4d::Identity());
  

  void furthestDistance(Geometry::CollisionMesh* obj_1, Geometry::CollisionMesh* obj_2, coll_data &result);

  //void generateBoxesFromOctomap(std::vector<CollisionObject<double>*>& boxes, OcTree<double> tree);

  // Octree collision checking:
  void checkCollision(  Geometry::CollisionMesh* obj_1,vector<Geometry::CollisionMesh*> env_arr, coll_data &result);

  void signedDistance(  Geometry::CollisionMesh* obj_1,vector<Geometry::CollisionMesh*> env_arr, coll_data &result,bool compute_non_colliding=false);

  
  void signedDistance(  Geometry::CollisionMesh* obj_1,vector<Geometry::CollisionMesh*> env_arr, vector<coll_data> &result,bool compute_non_colliding=false);
  

  // update a collision object pose:
  void updatePose(  Geometry::CollisionMesh* obj, vector<double> &pose);
  
  void updatePose(  Geometry::CollisionMesh* obj, Transform3d &pose);

  void updatePose(vector<  Geometry::CollisionMesh*> &obj, vector<vector<double>> &pose);
  void updatePose(vector<  Geometry::CollisionMesh*> &obj, vector<double> &pose);

  void updatePose(vector<  Geometry::CollisionMesh*> &obj, vector<Transform3d> &pose);

  void updatePose(Meshing::TriMesh* obj, Transform3d &pose);
  void updatePose(Meshing::TriMesh* &obj, vector<double> &pose);

  //pclEigen _pcl_eigen;

  // Create collision object from octree
  //void loadOcTreeModel(OcTree<Model> tree, Model* obj);
  // Read primitive shapes from urdf:

  void readLinkShapes(string &urdf_txt,vector<string> &link_names, vector<  Geometry::CollisionMesh*> &c_objs, vector<Transform3d> &link_offsets);

  void readLinkShapes(string &urdf_txt,vector<string> &link_names, vector<  Meshing::TriMesh*> &c_objs, vector<Transform3d> &link_offsets);

  void addOffsets(Transform3d &offset, vector<double> &pose, Transform3d &out_pose);

  void addOffsets(vector<Transform3d> &offsets, vector<double> &pose, vector<Transform3d> &out_poses);


  void addOffsets(vector<Transform3d> &offsets, vector<vector<double>> &poses, vector<Transform3d> &out_poses);

  //pcl_eigen::pclEigen _pcl_eigen;

  /*
  void store_offsets(vector<CollisionMesh*> &obj, vector<Transform3d> &offsets);

  */
  void getTransform3d(vector<vector<double>> &poses, vector<Transform3d> &out_poses);
  void getTransform3d(vector<double> &pose, Transform3d &out_pose);

};
}
#endif
