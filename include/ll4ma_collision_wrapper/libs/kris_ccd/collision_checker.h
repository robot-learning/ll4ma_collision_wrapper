#ifndef KRIS_CCD_COLLISION_H
#define KRIS_CCD_COLLISION_H
#include<ll4ma_collision_wrapper/libs/kris_library/collision_checker.h>
#include<ll4ma_collision_wrapper/libs/ccd/collision_checker.h>

using namespace std;


namespace kris_ccd_ccheck
{
class collisionChecker : public kris_ccheck::collisionChecker, public ccd_ccheck::collisionChecker
{
public:

  collisionChecker() : kris_ccheck::collisionChecker(),ccd_ccheck::collisionChecker() {}

  /// importing functions from base classes
  using kris_ccheck::collisionChecker::checkCollision;
  using kris_ccheck::collisionChecker::signedDistance;
  using kris_ccheck::collisionChecker::loadModel;
  using kris_ccheck::collisionChecker::createCollObj;
  using kris_ccheck::collisionChecker::updatePose;
  using kris_ccheck::collisionChecker::loadMeshShape;
  using kris_ccheck::collisionChecker::furthestDistance;
  
  using ccd_ccheck::collisionChecker::checkCollision;
  using ccd_ccheck::collisionChecker::loadModel;
  using ccd_ccheck::collisionChecker::createCollObj;
  using ccd_ccheck::collisionChecker::updatePose;
  using ccd_ccheck::collisionChecker::loadMeshShape;
  using ccd_ccheck::collisionChecker::loadCvxMeshShape;
  
  void checkCollision(Geometry::CollisionMesh* obj_1, Geometry::CollisionMesh* obj_2, const obj_t& obj1, const obj_t& obj2,coll_data &c_data);
  

  void signedDistance(Geometry::CollisionMesh* obj_1, Geometry::CollisionMesh* obj_2, const obj_t& obj1, const obj_t& obj2,coll_data &c_data,const bool &compute_non_colliding=false);

  void signedDistance(Geometry::CollisionMesh* obj_1, vector<Geometry::CollisionMesh*> obj_2, const obj_t& obj1, const vector<vector<obj_t>>& obj2,coll_data &c_data,const bool &compute_non_colliding=false);

  void signedDistance(Geometry::CollisionMesh* obj_1, vector<Geometry::CollisionMesh*> obj_2, const vector<obj_t>& obj1, const vector<vector<obj_t>>& obj2,coll_data &c_data,const bool &compute_non_colliding=false);

  //void signedDistance(Geometry::CollisionMesh* obj_1, vector<Geometry::CollisionMesh*> obj_2, const vector<obj_t>& obj1, const vector<obj_t>& obj2,coll_data &c_data,const bool &compute_non_colliding=false);

  void signedDistance(Geometry::CollisionMesh* obj_1, vector<Geometry::CollisionMesh*> obj_2, const obj_t& obj1, const vector<obj_t>& obj2,coll_data &c_data,const bool &compute_non_colliding=false);

  void signedDistance(Geometry::CollisionMesh* obj_1, Geometry::CollisionMesh* obj_2, const obj_t& obj1, const vector<obj_t>& obj2,coll_data &c_data, const bool &compute_non_colliding=false);

  void signedDistance(Geometry::CollisionMesh* obj_1, Geometry::CollisionMesh* obj_2, const vector<obj_t>& obj1, const vector<obj_t> & obj2,coll_data &c_data,const bool &compute_non_colliding=false);

  void readLinkShapes(string &urdf_txt,vector<string> &link_names, vector<Geometry::CollisionMesh*> &c_objs,vector<obj_t> &ccd_objs);

};
}
#endif
