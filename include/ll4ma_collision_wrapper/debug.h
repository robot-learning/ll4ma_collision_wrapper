#ifdef DEBUG
#undef DEBUG
#endif

#ifdef DEBUG_ALL
#undef DEBUG_ALL
#endif

#ifdef DEBUG_COLL
#undef DEBUG_COLL
#endif

//#define DEBUG_COLL 1
// uncomment below lines to enable debug information
//#define DEBUG 1

#define DEBUG_ALL 1 // Prints out information inside every loop. Only for Lt. Commander Data.
